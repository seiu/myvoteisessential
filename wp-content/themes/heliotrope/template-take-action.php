<?php
/**
 * Template Name: Take Action
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<?php get_template_part( 'partials/toppers/topper', 'issues' ); ?>

<section id="content" <?php post_class( "take-action-index-template" ); ?>>
  <div class="container">
    <div class="row justify-content-center">
      <div class="module light col col-md-8">

<link href='https://actionnetwork.org/css/style-embed-whitelabel-v3.css' rel='stylesheet' type='text/css' /><script id="an-event-embed" data-src='https://actionnetwork.org/widgets/v3/event_campaign/strikeforblacklives?format=js&source=widget'></script><div id='can-event_campaign-area-strikeforblacklives' style='width: 100%'><!-- this div is the target for our HTML insertion --></div>

      </div>
    </div>
  </div>
</section>
<?php 
get_footer(); ?>
