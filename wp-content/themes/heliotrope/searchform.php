<?php
  /* 
   * This template overrides the default get_search_form result with our own
   * Used by default in overlay-search.php/top-search.php, search.php, & 404.php
   */
  $search_form_id = 'id-' . uniqid();//not guaranteed to be the same, do not use for styling
?>
<form data-searchform role="search" method="get" class="search__form row" action="<?php echo esc_url( home_url() ); ?>/">
  <div class="col search__col--input">
    <label id="label-<?php echo $search_form_id; ?>" for="<?php echo $search_form_id; ?>" class="search__label screen-reader-text"><?php esc_html_e( 'Search for:', 'heliotrope' ); ?></label>
    <input aria-describedby="label-<?php echo $search_form_id; ?>" id="<?php echo $search_form_id; ?>" value="<?php echo esc_attr( isset($_GET['s']) ?  $_GET['s'] : '' );?>" type="search" class="search__input" placeholder="<?php esc_html_e( 'Search...', 'heliotrope' ); ?>" name="s" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" required>
    <label for="<?php echo $search_form_id; ?>" class="search__info">
      <?php esc_html_e( 'Press enter to search', 'heliotrope' ); ?>
    </label><!-- .search__info -->
  </div>
  <div class="col-auto search__col--submit">
    <button type="submit" class="btn search__submit-btn">Search</button>
  </div>
</form>