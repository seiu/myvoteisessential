<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package heliostrope
 */

get_header();
if ( ! post_password_required() ) : 
?>
	<article <?php post_class(); ?>>
		<?php get_template_part( 'partials/toppers/topper', 'topper' ); ?>
		<?php //get_template_part( 'partials/submenu', 'page' ); ?>
		<section id="content" class="inner-page">
			<?php get_template_part('partials/content', 'modules'); ?>
		</section>

		<?php
			$cta_id = is_home() ? get_option('page_for_posts') : get_the_ID();
			$selected = get_field( 'disable_bottom_cta', $cta_id );
			if ( 'true' != $selected ) :
			?>

			<?php get_template_part( 'partials/home/home', 'introduction' ); ?>

			<?php get_template_part( 'partials/home/home', 'text-cta' ); ?>

			<?php get_template_part( 'partials/home/home', 'actions' ); ?>

		<?php endif; ?>
			
	</article>
	<?php
else : 
	get_template_part( 'partials/password', 'protect' );
endif;
get_footer();
