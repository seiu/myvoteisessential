<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package heliotrope
 */

?>
  <?php if ( ! is_front_page() && ! is_home() && ! is_category() ) : ?>
    <?php get_template_part( 'partials/social/shares', 'slider' ); ?>
  <?php endif; ?>
	</main><!-- .site-main -->

	<footer id="colophon" data-js-social-hide class="social-hide site-footer body-copy">
    <div class="container-fluid">
      <?php if (has_nav_menu('primary_footer_navigation')) : ?>
        <section id="primary-footer-menu-section" class="row justify-content-center footer-navigation-section header-copy">

        <?php
          $footer_hashtag = get_theme_mod( 'footer_hashtag' );
          $footer_hashtag_url = get_theme_mod( 'footer_hashtag_url' );
          if ( $footer_hashtag ) :
          ?>
          <div class="site-footer__footer-hashtag justify-content-end">
            <a href="<?php echo wp_kses_post( $footer_hashtag_url ); ?>" target="_blank" class="h2"><?php echo esc_html( $footer_hashtag ); ?></a>
          </div>
        <?php endif; ?>

          <nav class="site-footer__menu-section col primary-footer-navigation" role="navigation" aria-label="Primary Footer Navigation">
            <?php
            wp_nav_menu(
              array(
                'theme_location' => 'primary_footer_navigation',
                'depth'          => '1',
                'container'      => 'primary-navigation',
                'menu_id'        => 'primary-navigation-footer',
                'menu_class'     => 'row menu justify-content-end site-footer__item_inner',
                'add_li_class'   => 'col-auto',
                'before'         => '',
                'walker'         => new We_Accessible_Walker(),
                //'items_wrap'     => heliotrope_social_items(),
              )
            );
            ?>
          </nav><!-- .primary -->
        </section>
      <?php endif; ?>

      <?php
        $copyright_start_year = get_theme_mod( 'copyright_start_year' );
        $current_year = gmdate( 'Y' );
        if ( $copyright_start_year >= $current_year || ! $copyright_start_year ) {
          $copyright_year = $current_year;
        } else {
          $copyright_year = $copyright_start_year . '&ndash;' . $current_year;
        }
        ?>
        <div class="site-footer__copyrights row justify-content-end">
          &copy;&nbsp;<span class="copyright-holder"><?php echo esc_html( get_theme_mod( 'copyright_title' ) ); ?></span> <span class="copyright-years"><?php echo esc_html( $copyright_year ); ?></span>

        <?php if (has_nav_menu('secondary_footer_navigation')) : ?>
          <nav class="footer-secondary" role="navigation" aria-label="Secondary Footer Navigation">
            <?php
            wp_nav_menu(
              array(
                'theme_location' => 'secondary_footer_navigation',
                'depth'          => '1',
                'container'      => 'primary-navigation',
                'menu_id'        => 'footer-secondary',
                'menu_class'     => 'row menu',
                'add_li_class'   => 'col',
                // 'before'         => '<small>',
                // 'after'          => '</small>',
                'link_before'    => '',
                'link_after'     => '',
              )
            );
            ?>
          </nav><!-- .footer-secondary -->
        <?php endif; ?>
        </div><!-- .site-footer__copyrights -->

  		<?php get_template_part( 'partials/footer/meta', 'credits' ); ?>
    </div><!-- .container -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php if ( 'page_overlay' == get_theme_mod( 'search_design', 'off' ) ) : ?>
  <?php get_template_part( 'partials/overlays/site-search' ); ?>
<?php elseif ( 'pull_down' == get_theme_mod( 'search_design', 'off' ) ) : ?>
  <?php get_template_part( 'partials/site-search/top-search' ); ?>
<?php endif; ?>


<?php get_template_part( 'partials/overlays/mobile-overlay', 'nav' ); ?>
<?php
if ( 'yes' == get_theme_mod( 'enable_alert_bar', 'no' ) ) {
  get_template_part( 'partials/alerts/alert-bar' );
}
?>
<?php wp_footer(); ?>
</body>
</html>
