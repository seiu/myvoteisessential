<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package heliotrope
 */

get_header();
?>

<?php get_template_part( 'partials/toppers/topper', 'feed' ); ?>

<section <?php post_class( "post-feed" ); ?>>
  <div class="container">
    <div id="content" class="row justify-content-center">
      <div class="col-md-10 col-lg-8">
        <?php
        if ( have_posts() ) :

          /* Start the Loop */
          while ( have_posts() ) :
            the_post();

            /*
             * Include the Post-Type-specific template for the content.
             * If you want to override this in a child theme, then include a file
             * called content-___.php (where ___ is the Post Type name) and that will be used instead.
             */
            get_template_part( 'partials/content', get_post_type() );

          endwhile;

        else :
          get_template_part( 'partials/content', 'none' );
        endif;
        ?>
      </div>
    </div>
  </div>
</section>
<?php get_template_part( 'partials/pagination', 'posts' ); ?>

<?php
get_footer();
