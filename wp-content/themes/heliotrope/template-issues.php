<?php
/**
 * Template Name: Issues
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

get_header(); ?>

<?php get_template_part( 'partials/toppers/topper', 'issues' ); ?>

<section id="content" <?php post_class( "post-feed issues-index-template" ); ?>>
  <div class="container">
    <div class="row justify-content-center">
      <div class="col col-md-8">
        
        <?php
          while ( have_posts() ) :
            the_post();
          ?>

          <?php the_content(); ?>

        <?php endwhile; // End of the loop. ?>
        
        <?php
            // Custom Post Query
            $args = array(
                'post_type' => 'issues',
                'paged' => $paged,
            );

            $the_query = new WP_Query( $args );

            while ( $the_query->have_posts() ) :
              $the_query->the_post();

              get_template_part( 'partials/content', get_post_type() );

            endwhile;
          wp_reset_postdata();
          ?>

      </div>
    </div>
  </div>
</section>
<?php 
get_template_part( 'partials/pagination', 'posts' );
get_footer(); ?>
