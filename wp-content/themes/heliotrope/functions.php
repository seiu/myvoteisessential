<?php
/**
 * Heliotrope functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package heliotrope
 */

/**
 * Misc theme setup
 */
require get_template_directory() . '/includes/theme-settings.php';

/**
 * Helper functions: these are commonly used functions within the theme
 */
require get_template_directory() . '/includes/functions/imports.php';
require get_template_directory() . '/includes/functions/urls.php';
require get_template_directory() . '/includes/functions/text.php';
require get_template_directory() . '/includes/functions/page-helpers.php';
require get_template_directory() . '/includes/functions/tiny-mce.php';
require get_template_directory() . '/includes/functions/pagination.php';
require get_template_directory() . '/includes/functions/modules.php';
require get_template_directory() . '/includes/functions/crms.php';
require get_template_directory() . '/includes/functions/topper.php';

/**
 * Admin settings
 */
require get_template_directory() . '/includes/admin.php';
require get_template_directory() . '/includes/api.php';

/**
 * Theme config/features
 */
require get_template_directory() . '/includes/users.php';
require get_template_directory() . '/includes/thumbnails.php';
require get_template_directory() . '/includes/shortcodes.php';

/**
 * Menus
 */
require get_template_directory() . '/includes/menus/class-we-accessible-walker.php';
require get_template_directory() . '/includes/menus.php';

/*
  Core template functionality
*/
require get_template_directory() . '/includes/template-tags.php';
require get_template_directory() . '/includes/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer.php';

/**
 * CPTs
 */
// require get_template_directory() . '/includes/cpts/issue.php';


/**
 * ACF hooks and field groups
 */
if ( function_exists( 'acf_add_local_field_group' ) ) :
  require get_template_directory() . '/includes/acf/index.php';
endif;

/**
 * Main Styles and Scripts
 */
require get_template_directory() . '/includes/scripts.php';
