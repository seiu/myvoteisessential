<?php
  get_header();
  $fp_page_id = we_get_page_id();//separate from any posts loops
?>

<?php get_template_part( 'partials/toppers/topper', 'home' ); ?>

<section id="content" <?php post_class( 'homepage-content' ); ?>>
  <div class="container-wide">
    <div class="site-main__content row justify-content-center">
      
      <?php get_template_part( 'partials/home/home', 'introduction' ); ?>

      <?php get_template_part( 'partials/home/home', 'text-cta' ); ?>

      <?php get_template_part( 'partials/home/home', 'actions' ); ?>

      <?php get_template_part( 'partials/home/home', 'demands' ); ?>

      <?php get_template_part( 'partials/home/home', 'download-share' ); ?>

    </div>
  </div>
</section>

<?php get_footer(); ?>