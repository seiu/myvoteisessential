<?php
/**
 * The template for displaying search results pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#search-result
 *
 * @package heliotrope
 */

get_header();
?>


<?php get_template_part( 'partials/toppers/topper', 'search' ); ?>

<div class="container body-content">
  <section id="content" class="row justify-content-center">
    <div class="col-md-10 col-lg-8">
			<?php if ( have_posts() ) : ?>

				<?php
				/* Start the Loop */
				while ( have_posts() ) :
					the_post();

					/**
					 * Run the loop for the search to output the results.
					 * If you want to overload this in a child theme then include a file
					 * called content-search.php and that will be used instead.
					 */
					get_template_part( 'partials/content', 'search' );

				endwhile;

			else :
				get_template_part( 'partials/content', 'none' );
			endif;
			?>
		</div>
	</section>
</div><!-- .site-main -->
<?php get_template_part( 'partials/pagination', 'search' ); ?>

	<?php
get_footer();
