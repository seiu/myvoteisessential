<?php
/**
 * Custom template tags for this theme
 *
 * Eventually, some of the functionality here could be replaced by core features.
 *
 * @package heliotrope
 */

if ( ! function_exists( 'heliotrope_posted_on' ) ) :
	/**
	 * Prints HTML with meta information for the current post-date/time.
	 */
	function heliotrope_post_date( $post_id = null, $format = null ) {
		$time_string = '<time class="posted-on entry-date published updated" datetime="%1$s">%2$s</time>';
		// if ( get_the_time( 'U' ) !== get_the_modified_time( 'U' ) ) {
		// 	$time_string = '<time class="entry-date published" datetime="%1$s">%2$s</time><time class="updated" datetime="%3$s">%4$s</time>';
		// }

		$time_string = sprintf( $time_string,
			esc_attr( get_the_date( DATE_W3C, $post_id ) ),
			esc_html( get_the_date( $format, $post_id ) ),
			esc_attr( get_the_modified_date( DATE_W3C, $post_id ) ),
			esc_html( get_the_modified_date( $format, $post_id ) )
		);

		// $posted_on = sprintf(
		// /* translators: %s: post date. */
		// 	esc_html_x( 'Posted on %s', 'post date', '_s' ),
		// 	'<a href="' . esc_url( get_permalink() ) . '" rel="bookmark">' . $time_string . '</a>'
		// );

		echo $time_string; // WPCS: XSS OK.
	}
endif;

if ( ! function_exists( 'heliotrope_posted_by' ) ) :
	/**
	 * Prints HTML with meta information for the current author.
	 */
	function heliotrope_posted_by() {
		if ( get_the_author() ){

			
      $author_override = get_post_meta( get_the_ID(), 'custom_author', true );
		
      if( $author_override ){
        $author_name = $author_override;
      }
      else{
        $author_name = get_the_author();
        $user_title = get_the_author_meta('user_job_title');
        if ( $user_title ) {
          /* translators: used between list items, there is a space after the comma */
          $author_name .= esc_html_x( ', ', 'list item separator', 'heliotrope' ) . $user_title;
        }
      }

			// <a class="url fn n" href="' . esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) . '">
			$byline = sprintf(
				/* translators: %s: post author. */
				esc_html_x( 'by %s', 'post author', 'heliotrope' ),
				'<span class="author vcard">' . esc_html( $author_name ) . '</span>'
			);

			echo '<span class="byline"> ' . wp_kses_post( $byline ) . '</span>'; // WPCS: XSS OK.
		}
	}
endif;

if ( ! function_exists( 'heliotrope_entry_categories' ) ) :
	/**
	 * Prints HTML with meta information for the categories
	 */
	function heliotrope_entry_categories( $featured = false ) {
		// Hide category and tag text for pages.
		if ( 'post' === get_post_type() ) {
      // var_dump(heliotrope_get_primary_taxonomy_term()); exit();
			
			$categories_list = $featured ?
        heliotrope_get_primary_taxonomy_term() :
        /* translators: used between list items, there is a space after the comma */
        get_the_category_list( esc_html__( ', &nbsp; ', 'heliotrope' ) );
			if ( $categories_list ) {
				/* translators: 1: list of categories. */
				echo '<span class="tax-links cat-links">' . wp_kses_post( $categories_list ) . '</span>'; // WPCS: XSS OK.
			}
		}
	}
endif;

if ( ! function_exists( 'heliotrope_entry_tags' ) ) :
	/**
	 * Prints HTML with meta information for the tags
	 */
	function heliotrope_entry_tags() {
		/* translators: used between list items, there is a space after the comma */
		$tags_list = get_the_tag_list( '<ul class="tax-links tag-links row"><li class="col">', '</li><li class="col">', '</li></ul>' );

		if ( $tags_list && ! is_wp_error( $tags_list ) ) {
			echo wp_kses_post( $tags_list ); // WPCS: XSS
		}
	}
endif;

if ( ! function_exists( 'heliotrope_print_edit_button' ) ) :
	function heliotrope_print_edit_button() {
		edit_post_link(
			sprintf(
				wp_kses(
					/* translators: %s: Name of current post. Only visible to screen readers */
					__( 'Edit <span class="screen-reader-text">%s</span>', 'heliotrope' ),
					array(
						'span' => array(
							'class' => array(),
						),
					)
				),
				get_the_title()
			),
			'<span class="edit-link"> ',
			'</span>'
		);
	}
endif;

if ( ! function_exists( 'heliotrope_post_thumbnail' ) ) :
	/**
	 * Displays an optional post thumbnail.
	 *
	 * Wraps the post thumbnail in an anchor element on index views, or a div
	 * element when on single views.
	 */
	function heliotrope_post_thumbnail( $size = 'featured_item', $attr = array() ) {
		if ( post_password_required() || is_attachment() || ! has_post_thumbnail() ) {
			return;
		}

		if ( is_singular() ) :
			?>

			<figure class="post-thumbnail">
				<?php the_post_thumbnail( $size, $attr ); ?>
			</figure><!-- .post-thumbnail -->

		<?php else : ?>

		<a class="post-thumbnail" href="<?php the_permalink(); ?>" aria-hidden="true" tabindex="-1">
			<?php
			the_post_thumbnail( 'post-thumbnail', array(
				'alt' => the_title_attribute( array(
					'echo' => false,
				) ),
			) );
			?>
		</a>

		<?php
		endif; // End is_singular().
	}
endif;

if ( ! function_exists( 'heliotrope_list_child_pages' ) ) :
	/**
	 * Finds all the child pages of a page and prints them out as a nav list
	 *
	 */
function heliotrope_list_child_pages() { 
 
  global $post; 
  $string = '';
   
  if ( is_page() && $post->post_parent ){
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->post_parent . '&echo=0' );
  }
  else{
    $childpages = wp_list_pages( 'sort_column=menu_order&title_li=&child_of=' . $post->ID . '&echo=0' );
  }

  if ( $childpages ) {
    $string .= '<ul class="row justify-content-start inverse-link-colors">';
    $string .= preg_replace( '/<li class="/', '<li class="col-auto ', $childpages );
    $string .= '</ul>';
  }
   
  return $string;
}
endif;

add_filter('post_thumbnail_html', 'modify_post_thumbnail_html', 25, 5);
function modify_post_thumbnail_html($html, $post_id, $post_thumbnail_id, $size, $attr = array() ) {
    $id = get_post_thumbnail_id(); // gets the id of the current post_thumbnail (in the loop)
    $src = wp_get_attachment_image_src( $id, $size ); // gets the image url specific to the passed in size (aka. custom image size)

    $sizes = isset( $attr['sizes'] ) ? $attr['sizes'] : wp_get_attachment_image_sizes( $id, $size );
    $srcset = wp_get_attachment_image_srcset( $id, $size );
    $alt = get_the_title( $id ); // gets the post thumbnail title
    $class = isset( $attr['class'] ) ? $attr['class'] : '';// gets classes passed to the post thumbnail, defined here for easier function access

    $is_lazy = false !== strpos($class, 'lazy-load');
    $lazy_prepend = $is_lazy ? 'data-' : '';
    $is_svg = substr_compare($src[0], '.svg', -strlen('.svg')) === 0;
    $srcurl = $src[0];
    $src = $lazy_prepend . 'src="' . $srcurl . '"';
    $width =  $is_svg ? '' : ' width="' . $src[1] . '"';
    $height = $is_svg ? '' : ' height="' . $src[2] . '"';
    $srcset = $is_svg ? '' : $lazy_prepend . 'srcset="' . $srcset . '"';
    $sizes = $is_svg ? '' : ' sizes="' . $sizes . '"';

    //set the loader
    $onload = isset( $attr['onload'] ) ?
    	'onload="' . $attr['onload'] . '"' :
    	( $class && false !== strpos( 'lazy-load', $class ) ?
    		'onload="this.parentNode.className += \' img-loaded\'"' :
    		''
    	);

    $html = '<img '
      . $src . ' ' . $onload
      . ' alt="' . $alt . '"' . ' class="' . $class . '"'
      . ( ! $is_svg ? ($width . $height . $srcset . $sizes) : '')
      . '/>'
      . ($is_lazy ? ('<noscript><img src="'. $srcurl .'" alt="' . $alt . '" ' . ( ! $is_svg ? ($width . $height . $srcset . $sizes) : '') . ' class="' . str_replace( 'lazy-load', '', $class ) . '" /></noscript>') : '');

    return $html;
}

// adds lazy loading classes to select images
add_filter( 'wp_get_attachment_image_attributes', 'heliotrope_change_attachment_image_attr', 99999, 1 );
function heliotrope_change_attachment_image_attr( $attr ){

		$class = isset( $attr['class'] ) ? $attr['class'] : '';
		$attr['class'] = $class;
    if (false !== strpos($class, 'lazy-load')) {
		    $onload = isset( $attr['onload'] ) ?
		    	'onload="' . $attr['onload'] . '"' :
		    	( false !== strpos( $class, 'lazy-load' ) ?
		    		'this.parentNode.className += \' img-loaded\'' :
		    		''
		    	);
		    $attr['onload'] = $onload;
        
        $attr['data-src'] = isset( $attr['src'] ) ? $attr['src'] : '';

        // clean up svg markup
		    if ( isset( $attr['src'] ) && substr_compare($attr['src'], '.svg', -strlen('.svg')) === 0 ) {
		      unset( $attr['data-srcset'] );
          unset( $attr['data-sizes'] );
		    }
		    else{
        	$attr['data-srcset'] = isset( $attr['srcset'] ) ? $attr['srcset'] : '';
		    }
        unset( $attr['src'] );
        unset( $attr['srcset'] );
    }

    return $attr;
}

// updates images in post content to have lazy loading capability
add_filter('acf_the_content', 'add_responsive_class', 9999, 1);
function add_responsive_class($content) {
    $content = mb_convert_encoding($content, 'HTML-ENTITIES', "UTF-8");
    if (!empty($content)) {
        $document = new DOMDocument();
        libxml_use_internal_errors(true);
        $document->loadHTML(utf8_decode($content), LIBXML_HTML_NODEFDTD);

        $imgs = $document->getElementsByTagName('img');

        foreach ($imgs as $img) {
          $src = $img->getAttribute('src');
          if( $img->hasAttribute('data-src') ) continue;
          if( $img->parentNode->tagName == 'noscript' ) continue;
          if( substr_compare($src, '.svg', -strlen('.svg')) === 0){
            $img->removeAttribute('srcset');
            $img->removeAttribute('sizes');
            $img->setAttribute('data-src', $src);
            $img->removeAttribute('src');
            continue;
          } 

          $clone = $img->cloneNode();
          
          $img->setAttribute('data-src', $src);
          $img->setAttribute('data-crap', 'blorp');
          $srcset = $img->getAttribute('srcset');
          if( ! empty($srcset)) {
              $img->setAttribute('data-srcset', $srcset);
          }
          $classes = $img->getAttribute('class');
          $img->setAttribute('class', $classes . ' lazy-load');
          $img->setAttribute('onload', 'this.parentNode.className += \' img-loaded\'');
          $img->removeAttribute('srcset');
          $img->removeAttribute('src');
          $no_script = $document->createElement('noscript');
          $no_script->appendChild($clone);

          $img->parentNode->insertBefore($no_script, $img);
        }

        // fixes doctype and other nonsense
        $trim_off_front = strpos($document->saveHTML(),'<body>') + 6;
        $trim_off_end = (strrpos($document->saveHTML(),'</body>')) - strlen($document->saveHTML());
        return substr( $document->saveHTML(), $trim_off_front, $trim_off_end );
    }
}

// function for printing out social items, possibly in navigation menus
if ( ! function_exists( 'heliotrope_social_items' ) ) :
  /**
   * returns extra lis for menus
   */
  function heliotrope_social_items() {
    $social_urls = get_theme_mod('social_urls');
    $items_wrap = '<ul id="%1$s" class="%2$s">%3$s</ul>';

    if ( $social_urls && strpos( $social_urls, ',' ) !== false ) {
      $social_links = '<li class="col-md-4 col-lg social-org-links top-level-menu-item"><span class="nav-category">' . get_theme_mod('social_menu_title', 'Social'). '</span><ul class="footer-sub-menu social-links row justify-content-center">';
      $social_urls = explode( ',', $social_urls );
      foreach ( $social_urls as $pair ) {
        if ( $pair && strpos( $pair, '||' ) !== false ) {
          $pair = explode( '||', $pair );
          $social_links .= '<li class="col-auto"><a rel="me" href="' . esc_url( $pair[0] ) . '" target="_blank" rel="noopener">';
          $social_links .= we_inline_svg( 'assets/icons/'. $pair[1] .'.svg', false );
          $social_links .= '<span class="screen-reader-text">Link to '. $pair[1] .'</span></a></li>';
        }
      }
      $items_wrap = '<ul id="%1$s" class="%2$s">%3$s';
      $items_wrap .= sprintf( '%1$s</ul>', $social_links );
    }
    return $items_wrap;
  }
endif;