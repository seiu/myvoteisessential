<?php
/*
 * This file manages all topper-related functions for the theme.
 */

/**
 * Returns a string that will be appended to the topper element HTML attributes
 * based on ACF values.
 *
 * @return {String} HTML attribute(s) to be appended to the topper element.
 */
function we_get_topper_type() {
  $topperImage = get_field( 'topper_image' );
  $catTopperImage = get_field( 'topper_image', get_option( 'page_for_posts' ) );
  $topperThumb = $topperImage['sizes'][ 'topper-image-retina' ];
  $catTopperThumb = $catTopperImage['sizes'][ 'topper-image' ];

  if  (
    'tall' == get_field( 'topper_size' ) &&
    'image' == get_field( 'topper_type' ) &&
    "" != $topperThumb &&
    ! is_home() &&
    ! is_category()
  ) {
    $bannerType = 'style="background-image: url(' . $topperThumb . ')"';
  } elseif  (
    is_front_page() && 
    'image' == get_field( 'topper_type' )
  ) {
    $bannerType = 'style="background-image: url(' . $topperThumb . ')"';
  } elseif (
    'image' == get_field( 'topper_type', get_option( 'page_for_posts' ) ) &&
    '' != $catTopperImage &&
    is_home()
  ) {
    $bannerType = 'style="background-image: url(' . $catTopperThumb . ')"';
  } elseif (
    'image' == get_field( 'topper_type', get_option( 'page_for_posts' ) ) &&
    '' != $catTopperImage &&
    is_category()
  ) {
    $bannerType = 'style="background-image: url(' . $catTopperThumb . ')"';
  } else {
    $bannerType = "";
  }

  return $bannerType;
}

/**
 * Returns an image id for the particular topper image and type selected
 *
 * @return {String} Image ID
 */
function we_get_topper_img_id($field = 'topper_image') {
  $topperImage = get_field( $field );
  $catTopperImage = get_field( $field, get_option( 'page_for_posts' ) );

  if (
    'tall' == get_field( 'topper_size' ) || 'medium' == get_field( 'topper_size' ) &&
    'image' == get_field( 'topper_type' ) &&
    "" != $topperImage &&
    ! is_home() &&
    ! is_category()
  ) {
    return $topperImage['ID'];
  } elseif (
    'image' == get_field( 'topper_type', get_option( 'page_for_posts' ) ) &&
    '' != $catTopperImage &&
    is_home()
  ) {
    return $catTopperImage['ID'];
  } elseif (
    'image' == get_field( 'topper_type', get_option( 'page_for_posts' ) ) &&
    '' != $catTopperImage &&
    is_category()
  ) {
    return $catTopperImage['ID'];
  } else {
    return false;
  }
}

/**
 * Returns a string indicating the topper size.
 *
 * @return {String} - "tall" | "short" | "medium"
 */
function we_get_topper_size() {
  if(is_search()){
    $topperSize = 'short';
  }
  elseif ( 'tall' == get_field( 'topper_size' ) ) {
    $topperSize = 'tall';
  }
  elseif ( 'medium' == get_field( 'topper_size' ) ) {
    $topperSize = 'medium';
  }
  elseif (
    is_front_page()
  ) {
    $topperSize = 'tall';
  }
  elseif (
    'tall' == get_field( 'topper_size', get_option( 'page_for_posts' ) )  &&
    is_home()
  ) {
    $topperSize = 'tall';
  }
  else {
    $topperSize = 'short';
  }
  return $topperSize;
}

/**
 * Returns a string that represents an HTML attribute(s) for the CTA.
 *
 * @return {String} - HTML attribute(s) for the CTA.
 */
function we_get_cta_type() {
  if ( 'same-window' === get_field( 'button_action' ) ) {
    $buttonAction = 'class="btn" target="_self"';
  } elseif ( 'new-window' === get_field( 'button_action' ) ) {
    $buttonAction = 'class="btn" target="_parent"';
  } else {
    $buttonAction = 'class="lightbox btn"';
  }
  return $buttonAction;
}

/**
 * Returns the `author_override` ACF field value or blank, if it does not exist.
 *
 * @return {String} - `author_override` ACF field value
 */
function we_get_author_override() {
  if ( get_field( 'author_override' ) ) {
    $authorName = get_field( 'author_override' );
  } else {
    $authorName = '';
  }
  return $authorName;
}
