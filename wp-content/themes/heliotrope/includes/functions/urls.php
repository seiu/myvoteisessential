<?php
/*
 * This file manages functions used for manipulating URLs.
 */

/**
 *  Like array_merge, but will recursively merge array values.
 *
 *  @param {Array} $a1 - The array to be merged into.
 *  @param {Array} $a2 - The array to merge in. Overwrites $a1 when string keys
 *                       conflict. Numeric keys will just be appended.
 *  @return {Array} The array, post-merge.
 */
function we_merge_query_var_arrays( $a1, $a2 ) {
  foreach ( $a2 as $k2 => $v2 ) {
    if ( is_string( $k2 ) ) {
      $a1[ $k2 ] = isset( $a1[ $k2 ] ) && is_array( $v2 ) ?
        we_merge_query_var_arrays( $a1[ $k2 ], $v2 )
        :
        $v2;
    } else {
      $a1[] = $v2;
    }
  }
  return $a1;
}

/**
 * Adds items to a query string by merging
 *
 *  @param {String} $query_string - The query string to add to.
 *  @param {String|Array} $vars_to_add - Either a string in var=val&[...]
 *                                       format, or an array.
 *  @return {String} The new query string. Duplicate vars are overwritten.
 */
function we_add_query_vars( $query_string, $vars_to_add = '' ) {
  if ( is_string( $vars_to_add ) ) {
    parse_str( $vars_to_add, $vars_to_add );
  }

  if ( preg_match( '/.*\?/', $query_string, $match ) ) {
    $query_string = preg_replace( '/.*\?/', '', $query_string );
  }

  parse_str( $query_string, $query_vars );

  $query_vars = we_merge_query_var_arrays( $query_vars, $vars_to_add );
  return http_build_query( $query_vars );
}

/**
 * Pulls apart a possibly parsed url into components
 *
 * @param {Array} $parsed_url
 * @return {String}
 */
function we_unparse_url( $parsed_url ) {
  $scheme = isset( $parsed_url['scheme'] ) ? $parsed_url['scheme'] . '://' : '';
  $host = isset( $parsed_url['host'] ) ? $parsed_url['host'] : '';
  $path = isset( $parsed_url['path'] ) ? $parsed_url['path'] : '';
  $query = isset( $parsed_url['query'] ) ? '?' . $parsed_url['query'] : '';
  $fragment = isset( $parsed_url['fragment'] ) ? '#' . $parsed_url['fragment'] : '';
  return "{$scheme}{$host}{$path}{$query}{$fragment}";
}

/**
 * Merge url parameters into a url that may or may not already have url parameters
 *
 * @param {String} - $url
 * @param {String|Array} - $new_params
 * @return {String}
 */
function we_merge_url_params( $url = '', $new_params = '' ) {
  $url_params = parse_url( html_entity_decode( $url ) );

  // If the URL is not parsable.
  if ( ! $url_params || ! $new_params ) {
    return $url;
  }

  $new_params = preg_replace( '/.*\?/', '', $new_params );

  if ( ! array_key_exists( 'query', $url_params ) ) {
    $url_params['query'] = '';
  }

  $url_params['query'] = we_add_query_vars( $url_params['query'], $new_params );

  return we_unparse_url( $url_params );
}


/**
 * Given a URL string of an AN embed, returns a URL string with modified URL params.
 * Specifically, clear_id and source are set if they're not already, and we
 * also add a unique id matching the target insertion element.
 *
 * @param {String} - $url
 * @return {String}
 */
function we_filter_an_url( $url, $id ) {
  $extra_params = array();
  $extra_params['format'] = 'js';
  $extra_params['can_widget_id'] = $id; // 'can-form-topper-'.$id;

  if ( ! array_key_exists( 'clear_id', $extra_params ) ) {
    $extra_params['clear_id'] = 'true';
  }

  if ( ! array_key_exists( 'source', $extra_params ) ) {
    $extra_params['source'] = 'website';
  }

  return we_merge_url_params(
    $url,
    $extra_params
  );

}
