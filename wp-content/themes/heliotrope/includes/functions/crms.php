<?php
/**
 * Given a WP location, type, and submit button text, renders the markup for
 * one of our integrated CRM forms.
 *
 * @param {Int|String} $location - pid or 'options'
 * @param {String} $type - 'global' or 'alert' or 'inner'
 * @param {String} $submit_text - Submit button text
 * @return {void}
 */
function we_render_crm( $location, $type, $submit_text='Submit' ) {
  $uniqid = uniqid();
  $disclaimer = wp_kses( get_theme_mod( 'disclaimer_placeholder', __('*Mobile alerts from the SIEU.', 'heliotrope') ), array() );
  if ( 'inner' == $type ) {
    // Gets fields for inner type
    //--------------------------------------------------------------------------
    $crm_type = get_sub_field( 'default_integration', $location );

    // NGP fields
    $ngp_redirect_url = get_sub_field( 'ngp_redirect_url', $location );
    $ngp_everyaction_form_id = get_sub_field( 'ngp_everyaction_form_id', $location );
    $ngp_form_id = get_sub_field( 'ngp_form_id', $location );

  } else {
    // Gets fields for global type
    //--------------------------------------------------------------------------
    $crm_type = get_field( 'default_integration', $location );

    // NGP fields
    $ngp_redirect_url = get_field( 'ngp_redirect_url', $location );
    $ngp_everyaction_form_id = get_field( 'ngp_everyaction_form_id', $location );
    $ngp_form_id = get_field( 'ngp_form_id', $location );
  }

  switch ( $crm_type ) {
    case 'ngp-van':
      $crmChoice = '<div class="ngp-code add-callback" id="ngp-header-form-' . $uniqid .'" data-redirect-url="' . esc_url( $ngp_redirect_url ) . '">';
      $crmChoice .= '<div class="ngp-form" data-form-url="https://secure.everyaction.com/v1/Forms/'. esc_attr( $ngp_everyaction_form_id ) . '"></div>';
      $crmChoice .= '</div>';
      $crmChoice .= '<div class="ngp-code disclaimer">' . $disclaimer .'</div>';
  }

  return $crmChoice;
}
