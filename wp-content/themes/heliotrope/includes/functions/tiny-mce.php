<?php

// force the visual editor to be the default, so that users can't save a text editor and have that default
add_filter( 'wp_default_editor', function() {
  return 'tinymce';
});

// can add custom fonts to the editors
function heliotrope_theme_add_mce_editor_styles() {
    if ( get_theme_mod( 'use_google_fonts' ) && get_theme_mod( 'google_fonts_url' ) ) :
        $font_url = str_replace( ',', '%2C', esc_url( get_theme_mod( 'google_fonts_url' ) ) );
        add_editor_style( $font_url );
    endif;
    // calls editor-style.min.css for TinyMCE edit screens. Would allow for more WYSIWYG
    add_editor_style( trailingslashit( get_template_directory_uri() ) . 'assets/css/editor-style.min.css' );

}
add_action( 'after_setup_theme', 'heliotrope_theme_add_mce_editor_styles' );

// allows you to limit the number of colors available
// function we_custom_colorpicker_options($settings) {

//     $custom_colours = '
//         "000", "Black",
//         "ff0000", "Red",
//     ';

//     // build colour grid default+custom colors
//     $init['textcolor_map'] = '['.$custom_colours.']';

//     // change the number of rows in the grid if the number of colors changes
//     // 8 swatches per row
//     $init['textcolor_rows'] = 1;

//     return $init;
// }
// add_filter('tiny_mce_before_init', 'my_mce4_options');

// removes custom colors from text colorpickers
add_filter( 'tiny_mce_plugins', 'wpse_tiny_mce_remove_custom_colors' );
function wpse_tiny_mce_remove_custom_colors( $plugins ) {       

    foreach ( $plugins as $key => $plugin_name ) {
        if ( 'colorpicker' === $plugin_name ) {
            unset( $plugins[ $key ] );
            return $plugins;            
        }
    }

    return $plugins;            
}

add_filter('mce_buttons', 'we_remove_buttons');
function we_remove_buttons($buttons) {
  $remove_buttons = array(
    'wp_more'
  );

  foreach ( $buttons as $button_key => $button_value ) {
      if ( in_array( $button_value, $remove_buttons ) ) {
          unset( $buttons[ $button_key ] );
      }
  }

  return $buttons;
}

add_filter('mce_buttons_2', 'we_remove_buttons_row_2');
function we_remove_buttons_row_2($buttons) {
  $remove_buttons = array(
    'forecolor'
  );

  foreach ( $buttons as $button_key => $button_value ) {
      if ( in_array( $button_value, $remove_buttons ) ) {
          unset( $buttons[ $button_key ] );
      }
  }

  return $buttons;
}

// Adds more WYSIWYG toolbar options: Bold only, and minimal
// https://www.advancedcustomfields.com/resources/customize-the-wysiwyg-toolbars/
add_filter( 'acf/fields/wysiwyg/toolbars', 'my_toolbars' );
function my_toolbars( $toolbars ) {

  $toolbars['Bold'] = array();
  $toolbars['Bold'][1] = array(
    'bold',
    'link',
    // 'unlink',
    //'blockquote_extended',
    //'sh_custom_button'
  );

  $toolbars['Minimal'] = array();
  $toolbars['Minimal'][1] = array(
    'bold',
    'italic',
    'link',
    // 'unlink'
  );

  $toolbars['MinimalOneLine'] = array();
  $toolbars['MinimalOneLine'][1] = array(
    'bold',
    'italic',
    'link',
    // 'unlink'
  );

  return $toolbars;
}

// spacing/indentation with the nowdoc below is suuuuuper important, do not indent
function we_tinymce_settings( $settings, $editor_id ) {

  $newline_regex = '/\r?\n/g';
  $settings['setup'] = <<<'JS'
function (editor) {
    var DOWN_ARROW_KEY_CODE = 40;
    var ENTER_KEY_CODE = 13;
    var UP_ARROW_KEY_CODE = 38;

    editor.addShortcut('alt+ctrl+h', '', function () { console.alert('h'); });
    editor.addShortcut('alt+ctrl+p', '', function (...args) { console.log(args); });
    editor.shortcuts.remove('alt+ctrl+p');
    if (window.acf){
      acf.add_filter('wysiwyg_tinymce_settings', (init, id) => {
        const _setup = init.setup;
        const editorWrapper = jQuery('#'+id).closest('.acf-editor-wrap');
        const acfClasses = jQuery( '#' + id ).closest( '.acf-field-wysiwyg' ).attr( 'class' );
        const isToolbarBold = editorWrapper.data('toolbar') === 'bold';
        const isToolbarMinimal = editorWrapper.data('toolbar') === 'minimal';

        //forward acf classes to tinyMCEbody
        init.body_class += acfClasses;

        //console.log(id, isToolbarBold, jQuery('#'+id).closest('.acf-editor-wrap').data('toolbar'));

        if(isToolbarBold){
          init.body_class += ' toolbar-type-bold';
          init.wp_autoresize_on = true;
          init.forced_root_block = 'h1';
          init.forced_root_blocks = false;
          init.convert_newlines_to_brs = false;
          init.remove_linebreaks = true;
          init.force_br_newlines = true;
          init.block_formats = 'Heading h1=h1';
          init.invalid_elements = 'br,p';
          init.paste_as_text = true;
          init.paste_text_sticky = true;
          init.paste_text_sticky_default = true;
          init.paste_data_images = false;
          init.wpautop = false;
          init.paste_preprocess = function(plugin, args){
            args.content = jQuery(
              jQuery.parseHTML(('<div>'+args.content+'</div>').replace(/></gm, '> <'))
            ).text().replace({$newline_regex}, ' ').replace(/\s{2,}/g, ' ').trim();
          }
          init.setup = function(editor) {
            editorWrapper.find('iframe').attr('height', 100).css('height', '100px');
            editor.on('keyDown', function (e) {
              if (e.keyCode === ENTER_KEY_CODE || e.keyCode === UP_ARROW_KEY_CODE || e.keyCode === DOWN_ARROW_KEY_CODE) {
                editor.dom.events.cancel(e);
                tinymce.dom.Event.cancel(e)
                e.preventDefault();
                e.stopPropagation();
                e.stopImmediatePropagation();

                return false;
              }
            });

            // run parent method
            return _setup.apply(this, arguments);
          };
        }

        if(isToolbarMinimal){
          init.body_class += ' toolbar-type-minimal';
          init.wp_autoresize_on = true;
        }

        if( !isToolbarMinimal && !isToolbarBold){
          init.block_formats = 'Paragraph=p; Heading 2=h2; Heading 3=h3; Heading 4=h4; Heading 5=h5; Heading 6=h6;';
        }

        return init;
      });
      acf.add_action('wysiwyg_tinymce_init', function(ed, id, mceInit, field) {
        const editorWrapper = jQuery('#'+id).closest('.acf-editor-wrap');
        const isToolbarBold = editorWrapper.data('toolbar') === 'bold';
        const isToolbarMinimal = editorWrapper.data('toolbar') === 'minimal';
        if (isToolbarBold) {
          ed.settings.autoresize_min_height = 80;
          editorWrapper.find('iframe').css('height', 'auto').css('min-height', '80px').attr('height', 80);
        }
        if (isToolbarMinimal) {
          ed.settings.autoresize_min_height = 140;
          editorWrapper.find('iframe').css('height', 'auto').css('min-height', '140px').attr('height', 140);
        }
      });
    }
  }
JS;
return $settings;
}
add_filter( 'tiny_mce_before_init', 'we_tinymce_settings', 10, 2 );
