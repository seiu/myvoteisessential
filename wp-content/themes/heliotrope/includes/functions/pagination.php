<?php
/*
 * This file manages all helpers related to pagination.
 */

function we_pagination_arrow( $direction = 'Next', $type = 'Page' ) {

  $transform = 'Next' !== $direction ? 'transform="rotate(180)"' : '';
  return '<svg xmlns="http://www.w3.org/2000/svg" '. $transform .' preserveAspectRatio="xMidYMid slice" viewBox="0 0 354.2 356.5" aria-labelledby="' . $direction . '-page-control"><title id="' . $direction . 'page-control">' . $direction . ' ' . $type . '</title><path d="M348.4 162.5c-.4-.4-1-.6-1.5-1L191.7 7.2c-8.7-8.6-22.7-8.6-31.3 0-8.7 8.6-8.7 22.6 0 31.1L278.9 156H22.5C10.4 156 0 165.4-.4 177.4c-.4 12.6 9.7 23 22.3 23h256.9L160.4 318.2c-8.7 8.6-8.7 22.6 0 31.1 8.7 8.6 22.7 8.6 31.3 0L347 195c.4-.4 1-.5 1.4-.9.7-.7.7-1.6 1.3-2.4 1.9-2.3 3.2-4.9 4-7.6.3-1.1.5-2 .6-3.1 1-6.6-.7-13.5-5.9-18.5z"/></svg>';
}

if ( ! function_exists( 'wpex_pagination' ) ) {
  /**
   * Echoes custom pagination markup where called.
   *
   * @return {void}
   */
  function wpex_pagination( $post_type_text = "Page" ) {
    global $wp_query;
    global $we_pagination_arrow;

    $total = $wp_query->max_num_pages;
    if ( $total <= 1 ) return;

    $big = 999999999; // need an unlikely integer

    if ( $total > 1 )  {
      if ( ! $current_page = get_query_var( 'paged' ) ) {
        $current_page = 1;
      }

      if ( get_option( 'permalink_structure' ) ) {
        $format = 'page/%#%/';
      } else {
        $format = '&paged=%#%';
      }

      echo paginate_links( array(
        'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),
        'format' => $format,
        'current' => max( 1, get_query_var( 'paged' ) ),
        'total' => $total,
        'mid_size' => 3,
        'type' => 'list',
        'prev_text' => we_pagination_arrow( 'Previous' ),
        'next_text' => we_pagination_arrow(),
        'before_page_number' => '<span class="screen-reader-text">'. $post_type_text . ' </span>',
      ) );
    }
  }
}

//fixes weird attempts to get sf_pro params into the link breaking the link
function filter_paginate_links( $link ) {
  $link = str_replace( 'sf_paged', '&sf_paged', $link );
  return $link;
}
add_filter( 'paginate_links', 'filter_paginate_links', 10, 1 ); 

/**
 * Echoes custom pagination markup where called.
 *
 * @return {void}
 */
function wpex_pagination_search() {
  global $wp_query;
  global $we_prev_arrow;
  global $we_next_arrow;

  $total = $wp_query->max_num_pages;
  $big = 999999999; // need an unlikely integer

  if ( $total > 1 )  {
    if ( !$current_page = get_query_var( 'paged' ) ) {
      $current_page = 1;
    }

    $format = '&paged=%#%';
    $base = $format === '?paged=%#%' ?
      $base = str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ) :
      $base = @add_query_arg( 'paged', '%#%' );



    echo paginate_links( array(
      'base' => $base,
      'format' => $format,
      'current' => max( 1, get_query_var( 'paged' ) ),
      'total' => $total,
      'mid_size' => 3,
      'type' => 'list',
      'prev_text' => $we_prev_arrow,
      'next_text' => $we_next_arrow,
    ) );
  }
}
