<?php
/**
 * Checks for a `section_part` ACF subfield and prints out an HTML ID attribute
 * with a slugified version of the subfield value.
 *
 * @return {void}
 */
function we_print_slugified_section_part( $section_part = '' ) {
  if ( ! $section_part ) { 
    $section_part = get_sub_field( 'section_part' );
  }

  $section_part = $section_part ?
    $section_part :
    get_the_title() . '-'. get_row_index();

  if ( $section_part ) {
    $slugified_section_part = we_slugify( $section_part );
    echo "id='{$slugified_section_part}'";
  }
}

/**
 * Checks for a `section_title` ACF subfield and prints out an HTML data
 * attribute with a slugified version of the subfield value.
 *
 * @return {void}
 */
function we_print_slugified_section_title( $section_title = '' ) {
  if ( ! $section_title ) {
    if ( ! $section_part ) { 
      $section_part = get_sub_field( 'section_part' );
    }
    $section_title = get_sub_field( 'section_title' );
  }

  $section_title = $section_title ?
    get_sub_field( 'section_title' ) :
    get_the_title() . '-'. get_row_index();

  if ( $section_title ) {
    $slugified_section_title = we_slugify( $section_title );
    echo "id='{$slugified_section_title}'";
  }
}

/**
 * Helper that calls both functions for slugified section parts and titles.
 *
 * @return {void}
 */
function we_print_section_meta( $section_title = '' ) {
  $is_new_section = 'true';
  if ( ! $section_title ) {
    if( get_sub_field( 'section_title' ) ) {
      $section_title = get_sub_field( 'section_title' );
    }
    else {
      $section_title = get_the_title() . '-'. get_row_index();
      $is_new_section = 'false';
    }
  }

  if ( $section_title ) {
    $slugified_section_title = we_slugify( $section_title );
    echo "id='{$slugified_section_title}' data-new-section='{$is_new_section}' data-id='{$slugified_section_title}'";
  }
}