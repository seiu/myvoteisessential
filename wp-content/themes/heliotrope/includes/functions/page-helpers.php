<?php

function we_get_page_id( $id = '' ) {
  if( is_home() ){
    return apply_filters( 'wpml_object_id', get_option( 'page_on_front' ), 'page' );
  }
  else if ( $id ) {
    return apply_filters( 'wpml_object_id', $id, 'page' );
  }
  else {
    return get_the_ID();
  }
}

// removes "Category" and "tag" and "Author" from titles on archive pages
add_filter( 'get_the_archive_title', function ($title) {
  if ( is_category() ) {    
    $title = single_cat_title( '', false );    
  } elseif ( is_tag() ) {    
    $title = single_tag_title( '', false );    
  } elseif ( is_author() ) {    
    $title = '<span class="vcard">' . get_the_author() . '</span>' ;    
  } elseif ( is_tax() ) { 
    $title = sprintf( __( '%1$s', 'heliotrope' ), single_term_title( '', false ) );
  } elseif ( is_post_type_archive() ) {
    $title = post_type_archive_title( '', false );
  }
  return $title;    
});

function we_is_simple_topper() {
  return 'simple' == get_post_meta( we_get_page_id(), 'topper_size', true );
}

//forces video iframes to have responsive wrappers
add_filter('embed_oembed_html', function ($html, $url, $attr, $post_id) {
  if( strpos( $html, 'youtube.com' ) !== false || strpos( $html, 'youtu.be' ) !== false || strpos( $html, 'vimeo.com') !== false ){
      return '<span class="responsive-embed responsive-embed-16by9">' . $html . '</span>';
  } else {
   return $html;
  }
}, 10, 4);

// remove [&hellip;] from excerpts
function carawebs_remove_hellip( $more ) {
      return '&hellip;';
  }
  
add_filter('excerpt_more', 'carawebs_remove_hellip');

function heliotrope_dewidow_titles( $title ) {
  return in_the_loop() && ! is_admin() ? we_dewidow( $title ) : $title;
}

//dewidows titles
add_filter( 'the_title', 'heliotrope_dewidow_titles', 10, 1 );
