<?php
/**
 * This file contains helper functions for all import-related functionality.
 */

// versioned assets are kept up to date in a manifest file


function get_revision_manifest() {
  global $we_manifest_json;
  
  $we_manifest_path = get_stylesheet_directory() . '/assets/rev-manifest.json';
  
  if ( file_exists( $we_manifest_path ) ) {
    ob_start();
    include $we_manifest_path;
    $file_contents = ob_get_clean();
    $we_manifest_json = json_decode( $file_contents, true );
  }
  else {
    $we_manifest_json = array();
  }
}

get_revision_manifest();
/**
 * Import a versioned asset
 *
 * @param string $filename
 * @return string
 */
function we_asset_path( $filename ) {
  global $we_manifest_json;

  if ( array_key_exists( $filename, $we_manifest_json ) ) {
    return get_template_directory_uri() . '/assets/' . $we_manifest_json[ $filename ];
  }

  return get_template_directory_uri() . '/assets/' . $filename;
}


/**
 * Import an inline SVG given a theme relative path.
 *
 * @param {String} $path - Relative path to theme.
 */
function we_inline_svg( $path, $echo = true ) {
  $file_path = get_template_directory() . '/' . $path;
  if ( file_exists( $file_path ) ){
    if ( $echo ) {
      include( $file_path );
    }
    else {
      return file_get_contents( $file_path );
    }
  }
}

/**
 * Import an inline SVG given a theme relative path.
 *
 * @param {String} $path - Relative path to theme.
 */
function we_lazy_inline_svg( $path ) {
  echo '<img class="svgloader" src=' . get_template_directory_uri() . '/' . $path . ' onload="var c=this;!window.fetch?window.llItems.push(this):fetch(this.currentSrc||this.src).then(x=>x.text()).then(svgText=>{this.outerHTML = svgText});" />';
}
