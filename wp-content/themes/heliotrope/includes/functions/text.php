<?php
/*
 * This file manages functions related to text manipulation.
 */

/**
 * Wraps each word in the given string with a given tag type.
 *
 * @param {String} $str - String to manipulate
 * @param {String} $tag - Tag type to wrap each word in (e.g. span)
 * @return {String} Manipulated string
 */
function we_wrap_words_with_tag( $str, $tag ) {
  // Return the string if it's empty.
  if ( 0 === strlen( $str ) ) {
    return $str;
  }

  // Loop through each word and wrap it with the tag.
  $words = explode( ' ', $str );
  $output = '';
  foreach ( $words as $word ) {
    $output .= "<{$tag}>{$word}</{$tag}>";
  }

  return $output;
}

/**
 * Make sure that the last word doesn't hang on its own in a line.
 *
 * @param {String} $text
 * @param {Int} $min_words
 * @param {Int} $max_last_length
 * @return {String} Formatted $text
 */
function we_dewidow( $text, $num_last = 2, $min_words = 4, $max_last_length = 15 ) {
  $return = $text;
  $words = explode( ' ', trim($text) );
  $length = count( $words );
  $last = $words[ $length - 1 ];
  $ending_words = array();

  if ( $length >= $min_words && strlen( $last ) < $max_last_length ) {

    foreach( range( $num_last, 1 ) as $index ) {
      array_push( $ending_words, $words[ $length - $index ] );
    }

    $return = implode( ' ', array_slice( $words, 0, $length - $num_last ) ) . ' '. '<span class="dewidow">' . implode( ' ', $ending_words ) . '</span>';
  }

  return $return;
}

/**
 * Given a string, slugify it.
 *
 * @param {String} $text
 * @return {String} - Slugified version of $text
 */
function we_slugify( $text ) {
  $text = strtolower( htmlentities( (string) $text ) );
  $text = str_replace( get_html_translation_table(), '-', $text );
  $text = preg_replace( '/&(?:[a-z\d]+|#\d+|#x[a-f\d]+);/i', '', $text );//so dashes do not collapse
  $text = str_replace( '-', ' ', $text );//so dashes do not collapse
  $text = preg_replace( '~\R~u', ' ', $text );
  $text = preg_replace('#[[:punct:]]#', '', $text);
  $text = str_replace( ' ', '-', $text );
  $text = str_replace( '/', '', $text );
  $text = preg_replace( '/[-]+/i', '-', $text );
  return $text;
}
