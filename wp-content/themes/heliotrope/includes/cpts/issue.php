<?php

add_action( 'init', function() {

  register_post_type( 'issues',
    array(
      'labels' => array(
        'name' => __( 'Issues', 'heliotrope' ),
        'singular_name' => __( 'Issue', 'heliotrope' ),
        'add_new' => __( 'Add New', 'heliotrope' ),
        'add_new_item' => __( 'Add New Issue', 'heliotrope' ),
        'edit' => __( 'Edit', 'heliotrope' ),
        'edit_item' => __( 'Edit Issue', 'heliotrope' ),
        'new_item' => __( 'New Issue', 'heliotrope' ),
        'view' => __( 'View', 'heliotrope' ),
        'view_item' => __( 'View Issue', 'heliotrope' ),
        'search_items' => __( 'Search Issue', 'heliotrope' ),
        'not_found' => __( 'No Issue found', 'heliotrope' ),
        'not_found_in_trash' => __( 'No Issue found in Trash', 'heliotrope' ),
        'parent' => __( 'Parent Issue', 'heliotrope' ),
      ),
      'public' => true,
      'show_in_rest'       => true,
      'show_in_nav_menus' => true,
      'menu_position' => 11,
      'has_archive' => false,// 'issues',
      'supports' => array( 'title', 'editor', 'custom-fields', 'thumbnail' ),
      'taxonomies' => array( '' ),
      'menu_icon' => 'dashicons-admin-site',
    )
  );
} );

// we can remove the editor when the issues template is selected like this
// function heliotrope_remove_editor_for_issues() {
//     if (isset($_GET['post'])) {
//         $id = $_GET['post'];
//         $template = get_post_meta($id, '_wp_page_template', true);
//         switch ($template) {
//             case 'template-issues.php':
//             remove_post_type_support('page', 'editor');
//             break;
//             default :
//             // Don't remove any other template.
//             break;
//         }
//     }
// }
// add_action('admin_head', 'heliotrope_remove_editor_for_issues');