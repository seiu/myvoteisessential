<?php

if ( ! function_exists( 'heliotrope_setup' ) ) :
  /**
   * Sets up theme defaults and registers support for various WordPress features.
   *
   * Note that this function is hooked into the after_setup_theme hook, which
   * runs before the init hook. The init hook is too late for some features, such
   * as indicating support for post thumbnails.
   */
  function heliotrope_setup() {
    /*
     * Make theme available for translation.
     * Translations can be filed in the /languages/ directory.
     * If you're building a theme based on heliotrope, use a find and replace
     * to change 'heliotrope' to the name of your theme in all the template files.
     */
    load_theme_textdomain( 'heliotrope', get_template_directory() . '/languages' );

    // Add default posts and comments RSS feed links to head.
    add_theme_support( 'automatic-feed-links' );

    /*
     * Let WordPress manage the document title.
     * By adding theme support, we declare that this theme does not use a
     * hard-coded <title> tag in the document head, and expect WordPress to
     * provide it for us.
     */
    add_theme_support( 'title-tag' );

    /*
     * Enable support for Post Thumbnails on posts and pages.
     *
     * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
     */
    add_theme_support( 'post-thumbnails' );

    add_theme_support( 'responsive-embeds' );

    /*
     * Switch default core markup for search form, gallery, and caption
     * to output valid HTML5.
     */
    add_theme_support( 'html5', array(
      'search-form',
      'gallery',
      'caption',
    ) );

    // since our theme is custom and not public, disable looking for theme updates
    add_filter( 'auto_update_theme', '__return_false' );

    // set defaults for inline images
    update_option('image_default_align', 'center' );
    update_option('image_default_size', 'large' );

    // Set up the WordPress core custom background feature.
    // add_theme_support( 'custom-background', apply_filters( 'heliotrope_custom_background_args', array(
    //  'default-color' => 'ffffff',
    //  'default-image' => '',
    // ) ) );

    // Add theme support for selective refresh for widgets.
    add_theme_support( 'customize-selective-refresh-widgets' );



    /**
     * Add support for core custom logo. Not something we use by default
     *
     * @link https://codex.wordpress.org/Theme_Logo
     */
    // add_theme_support( 'custom-logo', array(
    //  'height'      => 250,
    //  'width'       => 250,
    //  'flex-width'  => true,
    //  'flex-height' => true,
    // ) );
  }
endif;
add_action( 'after_setup_theme', 'heliotrope_setup' );

// Remove the extra Gutenberg block styles by default if the classic editor plugin is active
add_action(
  'wp_enqueue_scripts',
  function() {
    if ( ! function_exists( 'is_plugin_active' ) ) {
      include_once ABSPATH . 'wp-admin/includes/plugin.php';
    }

    if ( is_plugin_active( 'classic-editor/classic-editor.php' ) ) {
      wp_dequeue_style( 'wp-block-library' );
    }
    
  }
);

/**
 * Set the maximum size of content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 * https://developer.wordpress.com/themes/content-width/
 *
 * @global int $content_width
 */
function heliotrope_content_width() {
  // This variable is intended to be overruled from themes.
  // Open WPCS issue: {@link https://github.com/WordPress-Coding-Standards/WordPress-Coding-Standards/issues/1043}.
  // phpcs:ignore WordPress.NamingConventions.PrefixAllGlobals.NonPrefixedVariableFound
  $GLOBALS['content_width'] = apply_filters( 'heliotrope_content_width', 640 );
}
add_action( 'after_setup_theme', 'heliotrope_content_width', 0 );

// Head customization.
// ------------------------------------------------------------------------------
remove_action( 'wp_head', 'rsd_link' );
remove_action( 'wp_head', 'wp_generator' );
remove_action( 'wp_head', 'feed_links', 2 );
remove_action( 'wp_head', 'index_rel_link' );
remove_action( 'wp_head', 'wlwmanifest_link' );
remove_action( 'wp_head', 'feed_links_extra', 3 );
remove_action( 'wp_head', 'rel_canonical' );// managed by Yoast

// remove the ability to change themes
add_action('admin_init', 'remove_theme_menus');
function remove_theme_menus() {
  global $submenu;
  unset($submenu['themes.php'][5]);
  unset($submenu['themes.php'][15]);
}

#wp cron event schedule
// an action to clear any default notification nags
add_action( 'we_clear_default_notifications', function() {
  update_option( 'duplicate_post_show_notice', 0, true);
}, 10, 0 );


// Misc customization.
// ------------------------------------------------------------------------------
// Remove emojis.
add_action(
  'init',
  function() {
    remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
    remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
    remove_action( 'wp_print_styles', 'print_emoji_styles' );
    remove_action( 'admin_print_styles', 'print_emoji_styles' );
    remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
    remove_filter( 'comment_text_rss', 'wp_staticize_emoji' );
    remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
    add_filter(
      'tiny_mce_plugins',
      function( $plugins ) {
        if ( is_array( $plugins ) ) {
          return array_diff( $plugins, array( 'wpemoji' ) );
        } else {
          return array();
        }
      }
    );
    add_filter( 'emoji_svg_url', '__return_false' );
  }
);

// Remove some widgets
// ------------------------------------------------------------------------------
function wporg_remove_dashboard_widget() {
    remove_meta_box( 'dashboard_quick_press', 'dashboard', 'side' );
    remove_meta_box( 'dashboard_primary', 'dashboard', 'side' );
}
add_action( 'wp_dashboard_setup', 'wporg_remove_dashboard_widget' );

// allow iframes to appear in post content with the attributes they need to be responsive
// ------------------------------------------------------------------------------
function we_custom_wpkses_post_tags( $tags, $context ) {

    $tags['iframe'] = array(
        'class'           => true,
        'src'             => true,
        'data-src'             => true,
        'height'          => true,
        'allow'           => true,
        'width'           => true,
        'scrolling'       => true,
        'frameborder'     => true,
        'allowfullscreen' => true,
        'title'           => true,
    );
    $tags['noscript'] = array();
    $tags['img'] = array(
      'src'           => true,
      'onload'        => true,
      'data-src'      => true,
      'data-srcset'   => true,
      'width'         => true,
      'height'        => true,
      'srcset'        => true,
      'sizes'         => true,
      'alt'           => true,
      'class'         => true,
    );

    return $tags;
}

add_filter( 'wp_kses_allowed_html', 'we_custom_wpkses_post_tags', 10, 2 );
