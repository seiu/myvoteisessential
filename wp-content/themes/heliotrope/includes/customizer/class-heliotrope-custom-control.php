<?php

if ( class_exists( 'WP_Customize_Control' ) ) {

  function heliotrope_switch_sanitization( $input, $settings ) {
    return isset( $input ) && $input ? 1 : 0;
  }

  function heliotrope_underscore_slug( $string ) {
    return preg_replace( '/[^A-Za-z0-9_]/', '', $string );
  }

  function heliotrope_is_required( $validity, $value ){
    if( !isset($value) || "" === $value ){
      $validity->add( 'missing_required_value', __( 'This field is required', 'heliotrope' ) );
    }
    return $validity;
  }

  function heliotrope_slug_sanitize_select( $input, $setting ) {

      //input must be a slug: lowercase alphanumeric characters, dashes and underscores are allowed only
      $input = sanitize_key( $input );

      //get the list of possible select options
      $choices = $setting->manager->get_control( $setting->id )->choices;

      //return input if valid or return default option
      return ( array_key_exists( $input, $choices ) ? $input : $setting->default );
  }

  class Heliotrope_Custom_Control extends WP_Customize_Control {
    protected function get_resource_url() {
      if ( strpos( wp_normalize_path( __DIR__ ), wp_normalize_path( WP_PLUGIN_DIR ) ) === 0 ) {
        return plugin_dir_url( __DIR__ );
      }

      return trailingslashit( get_template_directory_uri() );
    }
  }


  class Heliotrope_Sortable_Repeater_Custom_Control extends Heliotrope_Custom_Control {
    /**
     * The type of control being rendered
     */
    public $type = 'sortable_repeater';
    /**
     * Button labels
     */
    public $button_labels = array();
    /**
     * Constructor
     */
    public function __construct( $manager, $id, $args = array(), $options = array() ) {
      parent::__construct( $manager, $id, $args );
      // Merge the passed button labels with our default labels
      $this->button_labels = wp_parse_args( $this->button_labels,
        array(
          'add' => __( 'Add', 'heliotrope' ),
        )
      );
    }

    public function render_content() {
    ?>
      <div class="sortable_repeater_control">
        <?php if( !empty( $this->label ) ) { ?>
          <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <?php } ?>
        <?php if( !empty( $this->description ) ) { ?>
          <span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
        <?php } ?>
        <input type="hidden" id="<?php echo esc_attr( $this->id ); ?>" name="<?php echo esc_attr( $this->id ); ?>" value="<?php echo esc_attr( $this->value() ); ?>" class="customize-control-sortable-repeater" <?php $this->link(); ?> />
        <div class="sortable_repeater sortables">
          <div class="repeater">
            <input type="url" pattern="https?://.+" value="" required class="repeater-input" placeholder="https://" /><span class="dashicons dashicons-sort"></span><a class="customize-control-sortable-repeater-delete" href="#"><span class="dashicons dashicons-no-alt"></span></a>
          </div>
        </div>
        <button class="button customize-control-sortable-repeater-add" type="button"><?php echo $this->button_labels['add']; ?></button>
      </div>
    <?php
    }
  }

  class Heliotrope_TinyMCE_Custom_Control extends Heliotrope_Custom_Control {

    public $type = 'tinymce_editor';

    public function enqueue() {
      wp_enqueue_script( 'customizer-controls-js', trailingslashit( get_template_directory_uri() ) . 'includes/customizer/js/tinymce.js', array( 'jquery' ), '1.0', true );
      wp_enqueue_style( 'customizer-controls-css', trailingslashit( get_template_directory_uri() ) . 'includes/customizer/css/tinymce.css', array(), '1.0', 'all' );
      wp_enqueue_editor();
    }

    public function to_json() {
      parent::to_json();
      $this->json['tinymcetoolbar1'] = isset( $this->input_attrs['toolbar1'] ) ? esc_attr( $this->input_attrs['toolbar1'] ) : 'bold italic bullist numlist alignleft aligncenter alignright link';
      $this->json['tinymcetoolbar2'] = isset( $this->input_attrs['toolbar2'] ) ? esc_attr( $this->input_attrs['toolbar2'] ) : '';
      $this->json['mediabuttons'] = isset( $this->input_attrs['mediaButtons'] ) && ( true === $this->input_attrs['mediaButtons'] ) ? true : false;
      $this->json['nobr'] = isset( $this->input_attrs['nobr'] ) && ( true === $this->input_attrs['nobr'] ) ? true : false;
    }
    /**
     * Render the control in the customizer
     */
    public function render_content() {
    ?>
      <div class="tinymce-control">
        <span class="customize-control-title"><?php echo esc_html( $this->label ); ?></span>
        <?php if ( ! empty( $this->description ) ) { ?>
          <span class="customize-control-description"><?php echo esc_html( $this->description ); ?></span>
        <?php } ?>
        <textarea style="width:100%;padding:10px;" id="<?php echo esc_attr( $this->id ); ?>" class="customize-control-tinymce-editor" <?php $this->link(); ?>><?php echo esc_attr( $this->value() ); ?></textarea>
      </div>
    <?php
    }
  }
}
