<?php

add_action( 'customize_register', 'heliotrope_alert_customizer_settings' );
function heliotrope_alert_customizer_settings( $wp_customize ) {

  // Add Redirect Section
  $wp_customize->add_section('heliotrope_alerts', array(
    'title' => __( 'Alert Bar', 'heliotrope' ),
    'description' => __( 'The alert bar will display at the top of every page on the site.', 'heliotrope' ),
    'priority' => 180,
    'capability'     => 'edit_theme_options',
    'active_callback' => '',
    'theme_supports' => '',
  ));

  $wp_customize->add_setting( 'enable_alert_bar', array(
      'default'     => 'no',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'enable_alert_bar', array(
    'label'       => __( 'Enable the alert', 'heliotrope' ),
    'section'     => 'heliotrope_alerts',
    'type'        => 'radio',
    'choices'  => array(
      'yes'  => 'Yes',
      'no' => 'No',
    ),
  ) );

  $wp_customize->add_setting( 'alert_headline', array(
      'default'     => '',
      'transport'   => 'postMessage',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( new Heliotrope_TinyMCE_Custom_control( $wp_customize, 'alert_headline',
      array(
        'priority' => 10, // Within the section.
        'section' => 'heliotrope_alerts', // Required, core or custom.
        'label' => __( 'Alert Headline', 'heliotrope' ),
        'description' => __( 'Text for the alert bar.', 'heliotrope' ),
        'input_attrs' => array(
          'toolbar1' => 'italic',
          'nobr' => true,
          'mediaButtons' => false,
          'placeholder' => __( 'Text Us anytime you need', 'heliotrope' ),
        ),
      )
    )
  );

  $wp_customize->add_setting( 'alert_link_url', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_url',
  ));

  $wp_customize->add_control( 'alert_link_url',
    array(
      'type' => 'url',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_alerts', // Required, core or custom.
      'label' => __( 'Destination URL', 'heliotrope'),
      'description' => __( 'The url to link users to', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 512,
        'pattern' => '^[a-zA-Z0-9]*$',
        'placeholder' => '/donate or https://external.site.co',
      ),
    )
  );

  $wp_customize->add_setting( 'alert_link_title', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'alert_link_title',
    array(
      'type' => 'url',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_alerts', // Required, core or custom.
      'label' => __( 'Link Title', 'heliotrope'),
      'description' => __( 'A description of the link destination, for screenreaders', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'required' => true,
        'maxlength' => 512,
        'placeholder' => 'A link to a donation form',
      ),
    )
  );

  if( ! defined('DISALLOW_COOKIES') || true !== constant('DISALLOW_COOKIES') ){

    $wp_customize->add_setting( 'alert_only_once_per_session', array(
      'default'     => 'yes',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
    ));

    // Control: Enable.
    $wp_customize->add_control( 'alert_only_once_per_session', array(
      'label'       => __( 'Show Only Once Per Session?', 'heliotrope' ),
      'description' => __( 'If "yes" will only show the alert on the first page the user sees while on the site for that session.', 'heliotrope' ),
      'section'     => 'heliotrope_alerts',
      'type'        => 'radio',
      'choices'  => array(
        'yes'  => 'Yes',
        'no' => 'No',
      ),
    ) );

    $wp_customize->add_setting( 'alert_is_cookied', array(
      'default'     => 'yes',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
    ));

    // Control: Enable.
    $wp_customize->add_control( 'alert_is_cookied', array(
      'label'       => __( 'Use a Cookie?', 'heliotrope' ),
      'description' => __( 'Remember if a user has closed the alert, and keep it closed.', 'heliotrope' ),
      'section'     => 'heliotrope_alerts',
      'type'        => 'radio',
      'choices'  => array(
        'yes'  => 'Yes',
        'no' => 'No',
      ),
    ) );



  }

}

