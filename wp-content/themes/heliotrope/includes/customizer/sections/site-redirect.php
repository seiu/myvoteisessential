<?php

add_action( 'customize_register', 'heliotrope_redirect_customizer_settings' );
function heliotrope_redirect_customizer_settings( $wp_customize ) {

  // Add Redirect Section
  $wp_customize->add_section('heliotrope_redirect', array(
    'title' => __( 'Site Redirect', 'heliotrope' ),
    'description' => __( 'A "splash" redirect can will force redirect all vistors to a specific url of your choice, either internal or external. It can be set to affect either just the home page or else all pages on the site.', 'heliotrope' ),
    'priority' => 300,
    'capability'     => 'edit_theme_options',
    'active_callback' => '',
    'theme_supports' => '',
  ));

  $wp_customize->add_setting( 'enable_splash_page', array(
      'default'     => 'no',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'enable_splash_page', array(
    'label'       => __( 'Enable Splash Page Redirect', 'heliotrope' ),
    'section'     => 'heliotrope_redirect',
    'type'        => 'radio',
    'choices'  => array(
      'yes'  => 'Yes',
      'no' => 'No',
    ),
  ) );

  $wp_customize->add_setting( 'splash_page_url', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_url',
  ));

  $wp_customize->add_control( 'splash_page_url',
    array(
      'type' => 'url',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_redirect', // Required, core or custom.
      'label' => __( 'Destination URL', 'heliotrope'),
      'description' => __( 'The url to redirect users to. It can be either another page on the site or an external site.', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 512,
        'required' => true,
        'pattern' => '^[a-zA-Z0-9]*$',
        'placeholder' => '/splash or https://external.site.co',
      ),
    )
  );

  $wp_customize->add_setting( 'only_redirect_home', array(
      'default'     => 'yes',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'only_redirect_home', array(
    'label'       => __( 'Only Redirect visitors to the homepage', 'heliotrope' ),
    'description' => __( 'Caution: if "no" then all site pages will redirect (including pages like Privacy Policy)', 'heliotrope' ),
    'section'     => 'heliotrope_redirect',
    'type'        => 'radio',
    'choices'  => array(
      'yes'  => 'Yes',
      'no' => 'No',
    ),
  ) );

  $wp_customize->add_setting( 'only_direct_home', array(
      'default'     => 'yes',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'only_direct_home', array(
    'label'       => __( 'Only Redirect on direct Home page visits', 'heliotrope' ),
    'description' => __( 'Only redirect users who visit home page directly, but not if they visit some other page first and then go to the homepage.', 'heliotrope' ),
    'section'     => 'heliotrope_redirect',
    'type'        => 'radio',
    'choices'  => array(
      'yes'  => 'Yes',
      'no' => 'No',
    ),
  ) );

  if( ! defined('DISALLOW_COOKIES') || true !== constant('DISALLOW_COOKIES') ){

    $wp_customize->add_setting( 'is_splash_cookied', array(
      'default'     => 'yes',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
    ));

    // Control: Enable.
    $wp_customize->add_control( 'is_splash_cookied', array(
      'label'       => __( 'Use a Cookie?', 'heliotrope' ),
      'description' => __( 'It\'s highly recommended that you use a cookie to prevent users from being redirected more than once. Only set this to no if you intend to route all your site visits to this single url while the redirect is enabled.', 'heliotrope' ),
      'section'     => 'heliotrope_redirect',
      'type'        => 'radio',
      'choices'  => array(
        'yes'  => 'Yes',
        'no' => 'No',
      ),
    ) );

    $wp_customize->add_setting( 'splash_cookie_name', array(
        'default'     => 'splash_redirect',
        'sanitize_callback' => 'heliotrope_underscore_slug',
    ));

    $wp_customize->add_control( 'splash_cookie_name',
      array(
        'type' => 'url',
        'priority' => 10, // Within the section.
        'section' => 'heliotrope_redirect', // Required, core or custom.
        'label' => __( 'Splash Cookie Name', 'heliotrope'),
        'description' => __( 'This should be lowercase and use underscores. Changing the name will reset the redirect behavior for all users.', 'heliotrope' ),
        'capability' => 'edit_theme_options',
        'input_attrs' => array(
          'maxlength' => 512,
          'pattern' => '^[a-zA-Z0-9]*$',
          'placeholder' => 'eoy_splash',
        ),
      )
    );

    $wp_customize->add_setting( 'cookie_length', array(
        'default'     => '7',
        'sanitize_callback' => 'absint',
    ));

    $wp_customize->add_control( 'cookie_length',
      array(
        'type' => 'number',
        'priority' => 10, // Within the section.
        'section' => 'heliotrope_redirect', // Required, core or custom.
        'label' => __( 'Cookie Duration', 'heliotrope'),
        'description' => __( 'Cookie expiration in days (example: 30). Visitors will be redirected again after their cookie expires.', 'heliotrope' ),
        'capability' => 'edit_theme_options',
        'input_attrs' => array(
          'maxlength' => 4,
          'pattern' => '^[0-9]*$',
          'placeholder' => '3',
        ),
      )
    );
  }

}

