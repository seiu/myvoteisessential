<?php



add_action( 'customize_register', 'heliotrope_advanced_customizer_settings' );
function heliotrope_advanced_customizer_settings( $wp_customize ) {


  $wp_customize->add_panel( 'heliotrope_theme_advanced', array(
    'title' => __( 'Advanced', 'heliotrope' ),
    'description' => __( 'Advanced', 'heliotrope' ), // Include html tags such as <p>.
    'priority' => 99999, // Mixed with top-level-section hierarchy.
    'capability' => 'edit_advanced_theme_options', // Optional. Default: 'edit_theme_options'
  ) );

  // Add Footer Section
  $wp_customize->add_section('heliotrope_theme_options', array(
    'title' => __( 'Theme Options', 'heliotrope' ),
    'description' => '',
    'priority' => 9999,
    'panel' => 'heliotrope_theme_advanced',
    'capability' => 'edit_advanced_theme_options', // Optional. Default: 'edit_theme_options'
  ));

  // Add Footer Section
  $wp_customize->add_section('heliotrope_theme_we', array(
    'title' => __( 'Wide Eye Internal', 'heliotrope' ),
    'description' => '',
    'priority' => 9999,
    'panel' => 'heliotrope_theme_advanced',
    'capability' => 'edit_advanced_theme_options', // Optional. Default: 'edit_theme_options'
  ));

  $wp_customize->add_setting( 'made_by_wide_eye', array(
      'default'     => false,
      'transport'   => 'refresh',
      'sanitize_callback' => 'heliotrope_switch_sanitization',
  ));

  $wp_customize->add_control( 'made_by_wide_eye',
     array(
        'label' => __( 'Made By Wide Eye', 'heliotrope' ),
        'description' => __( 'Show the Made By Wide Eye text', 'heliotrope' ),
        'section'  => 'heliotrope_theme_we',
        'priority' => 10, // Optional. Order priority to load the control. Default: 10
        'type' => 'checkbox',
        'capability' => 'edit_advanced_theme_options', // Optional. Default: 'edit_theme_options'
     )
  );

  $wp_customize->add_setting( 'bugherd_key', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'bugherd_key',
     array(
        'type' => 'text',
        'priority' => 10, // Within the section.
        'section' => 'heliotrope_theme_we', // Required, core or custom.
        'label' => __( 'BugHerd Project Key', 'heliotrope' ),
        'input_attrs' => array(
          'maxlength' => 24,
          'pattern' => '^[a-zA-Z0-9]*$',
          'placeholder' => 'e.g. ibnxoz9natnkqyngkdqdcq',
        ),
        'capability' => 'edit_advanced_theme_options', // Optional. Default: 'edit_theme_options'
     )
  );

  $wp_customize->add_setting( 'search_design', array(
      'default'     => 'no',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'search_design', array(
    'label'       => __( 'Search Type', 'heliotrope' ),
    'section'     => 'heliotrope_theme_options',
    'type'        => 'radio',
    'choices'  => array(
      'off'  => 'None',
      'pull_down' => 'Pull Down',
      'page_overlay' => 'Overlay',
    ),
  ) );

  $wp_customize->add_setting( 'alert_position', array(
      'default'     => 'bottom',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  // Control: Enable.
  $wp_customize->add_control( 'alert_position', array(
    'label'       => __( 'Whether the alert bar is top or bottom aligned', 'heliotrope' ),
    'section'     => 'heliotrope_theme_options',
    'type'        => 'radio',
    'choices'  => array(
      'bottom'  => 'Bottom',
      'top' => 'Top',
    ),
  ) );

  $wp_customize->add_setting( 'use_typekit', array(
      'default'     => false,
      'transport'   => 'refresh',
      'sanitize_callback' => 'heliotrope_switch_sanitization',
  ));

  $wp_customize->add_control( 'use_typekit',
     array(
        'label' => __( 'Use Typekit?', 'heliotrope' ),
        'section'  => 'heliotrope_theme_options',
        'priority' => 10, // Optional. Order priority to load the control. Default: 10
        'type' => 'checkbox',
        'capability' => 'edit_theme_options',
     )
  );

  $wp_customize->add_setting( 'typekit_kit_id', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control( 'typekit_kit_id',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_options', // Required, core or custom.
      'label' => __( 'Typekit font kit ID', 'heliotrope'),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 10,
        'pattern' => '^[a-zA-Z0-9]*$',
        'placeholder' => 'sfg75nf',
      ),
    )
  );

  $wp_customize->add_setting( 'use_google_fonts', array(
      'default'     => false,
      'transport'   => 'refresh',
      'sanitize_callback' => 'heliotrope_switch_sanitization',
  ));

  $wp_customize->add_control( 'use_google_fonts',
     array(
        'label' => __( 'Use Google Fonts?', 'heliotrope' ),
        'section'  => 'heliotrope_theme_options',
        'priority' => 10, // Optional. Order priority to load the control. Default: 10
        'type' => 'checkbox',
        'capability' => 'edit_theme_options',
     )
  );

  $wp_customize->add_setting( 'google_fonts_url', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_url',
  ));

  $wp_customize->add_control( 'google_fonts_url',
    array(
      'type' => 'textarea',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_options', // Required, core or custom.
      'label' => __( 'Google Fonts Url', 'heliotrope'),
      'description' => __( 'Full https:// url of google fonts embed (the href component, not the full &lt;link/&gt; tag.', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 356,
        'data-extract-href' => true,
        'placeholder' => 'e.g. https://fonts.googleapis.com/css?family=Public+Sans&display=swap',
      ),
    )
  );

}

