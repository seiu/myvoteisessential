<?php

add_action( 'customize_register', 'heliotrope_tracking_customizer_settings' );
function heliotrope_tracking_customizer_settings( $wp_customize ) {

  // Add Footer Section
  $wp_customize->add_section('heliotrope_marketing', array(
    'title' => __( 'Pixels & Tracking', 'heliotrope' ),
    'description' => '',
    'priority' => 180,
  ));

  $wp_customize->add_setting( 'gtm_container_id', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control( 'gtm_container_id',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_marketing', // Required, core or custom.
      'label' => __( 'Google Tag Manager Container ID', 'heliotrope' ),
      'description' => __( 'If you\'re using Google Tag Manager, enter your container Id (e.g. GTM-XXXX). This will output the GTM embed code on all pages.', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 30,
        'placeholder' => __( 'GTM-XXXXXXXX', 'heliotrope' ),
      ),
    )
  );

  $wp_customize->add_setting( 'ga_id', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control( 'ga_id',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_marketing', // Required, core or custom.
      'label' => __( 'Google Analytics ID', 'heliotrope' ),
      'description' => __( 'If you\'re using Google Analytics, enter your Tracking ID (e.g. GA-XXXX). This will output the GA tracking code on all pages.', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 30,
        'placeholder' => __( 'UA-XXXXXXX', 'heliotrope' ),
      ),
    )
  );

  $wp_customize->add_setting( 'facebook_pixel_id', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control( 'facebook_pixel_id',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_marketing', // Required, core or custom.
      'label' => __( 'Facebook Pixel ID', 'heliotrope' ),
      'description' => __( 'If you\'re using Facebook Pixel Analytics, enter your Pixel ID (e.g. XXXXXXXXXXXXXXXXX). This will output the Facebook pixel code on all pages.', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 30,
        'placeholder' => __( 'XXXXXXXXXXXXXXXXX', 'heliotrope' ),
      ),
    )
  );

  // https://developers.facebook.com/ads/blog/post/2019/06/21/official-facebook-pixel-plugin-for-wordpress-drupal-and-joomla/
  // Facebook also offers a customized pixel plugin
  // https://business.twitter.com/en/help/campaign-measurement-and-analytics/conversion-tracking-for-websites.html
}

