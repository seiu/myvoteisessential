<?php

add_action( 'customize_register', 'heliotrope_footer_customizer_settings' );
function heliotrope_footer_customizer_settings( $wp_customize ) {

  // Add Footer Section
  $wp_customize->add_section('heliotrope_theme_footer', array(
    'title' => __( 'Footer Content', 'heliotrope' ),
    'description' => __( 'Customize elements in the site\'s footer section.', 'heliotrope' ),
    'priority' => 130,
  ));

  $wp_customize->add_setting( 'copyright_start_year', array(
      'default'     => gmdate('Y'),
      'transport'   => 'postMessage',
      'sanitize_callback' => 'absint',
  ));

  $wp_customize->add_control( 'copyright_start_year',
    array(
      'type' => 'number',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_footer', // Required, core or custom.
      'label' => __( 'Copyright Start year', 'heliotrope' ),
      'description' => __( 'The copyright start year should be the 4-digit year of the oldest content copyrighted content contained on the site.', 'heliotrope' ),
      'input_attrs' => array(
        'step' => 1,
        'min' => 1900,
        'max' => gmdate('Y'),
        'placeholder' => '19XX',
      ),
    )
  );

  $wp_customize->add_setting( 'copyright_title', array(
      'default'     => '',
      'transport'   => 'postMessage',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( 'copyright_title',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_footer', // Required, core or custom.
      'label' => __( 'Copyright Holder', 'heliotrope' ),
      'description' => __( 'The organization or legal entity to which the copyright belongs.', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 100,
        'placeholder' => __( 'Organization, LTD.', 'heliotrope' ),
      ),
    )
  );


    // Add our Sortable Repeater setting and Custom Control for Social media URLs
    $wp_customize->add_setting( 'social_urls',
      array(
        'default' => array(),
        'transport' => 'postMessage',
        'sanitize_callback' => 'esc_attr'
      )
    );
    $wp_customize->add_control( new Heliotrope_Sortable_Repeater_Custom_Control( $wp_customize, 'social_urls',
      array(
        'label' => __( 'Social Links', 'heliotrope' ),
        'section' => 'heliotrope_theme_footer',
        'button_labels' => array(
          'add' => __( 'Add URL', 'heliotrope' ),
        )
      )
    ) );


  // $wp_customize->add_setting( 'below_copyright', array(
  //     'default'     => '',
  //     'transport'   => 'refresh',
  //     'sanitize_callback' => 'wp_kses_post',
  // ));

  // $wp_customize->add_control( new Heliotrope_TinyMCE_Custom_control( $wp_customize, 'below_copyright',
  //     array(
  //       'priority' => 10, // Within the section.
  //       'section' => 'heliotrope_theme_footer', // Required, core or custom.
  //       'label' => __( 'Text Below Copyright', 'heliotrope' ),
  //       'description' => __( 'Additional information, such as an organizational address or phone number.', 'heliotrope' ),
  //       'input_attrs' => array(
  //         'toolbar1' => 'bold italic bullist numlist alignleft aligncenter alignright link wpautop',
  //         'mediaButtons' => false,
  //         'placeholder' => 'Lorem Ipsum',
  //       ),
  //     )
  //   )
  // );

  $wp_customize->add_setting( 'footer_hashtag', array(
      'default'     => '',
      'transport'   => 'postMessage',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( 'footer_hashtag',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_footer', // Required, core or custom.
      'label' => __( 'Footer Hashtag', 'heliotrope' ),
      'description' => __( '', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 50,
        'placeholder' => __( '#StrikeForBlackLives', 'heliotrope' ),
      ),
    )
  );

  $wp_customize->add_setting( 'footer_hashtag_url', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'footer_hashtag_url',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_theme_footer', // Required, core or custom.
      'label' => __( 'Footer Hashtag Link', 'heliotrope' ),
      'description' => __( '', 'heliotrope' ),
      'input_attrs' => array(
        'maxlength' => 150,
        'placeholder' => __( 'https://twitter.com/hashtag/StrikeForBlackLives', 'heliotrope' ),
      ),
    )
  );

}

