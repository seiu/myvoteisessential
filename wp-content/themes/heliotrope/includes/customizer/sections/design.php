<?php

add_action( 'customize_register', 'heliotrope_design_customizer_settings' );
function heliotrope_design_customizer_settings( $wp_customize ) {

  $wp_customize->add_panel( 'heliotrope_forms', array(
    'title' => __( 'Forms & Calls to Action', 'heliotrope' ),
    'description' => __( 'Design Elements (Advanced)', 'heliotrope' ), // Include html tags such as <p>.
    'priority' => 120, // Mixed with top-level-section hierarchy.
  ) );

  // Add Forms Section
  $wp_customize->add_section('heliotrope_design_ctas', array(
    'title' => __( 'Calls to Action', 'heliotrope' ),
    'description' => __( 'Customize the fallback language of sitewide calls to action.', 'heliotrope' ),
    'panel' => 'heliotrope_forms',
    'priority' => 120,
  ));

  // Add Forms Section
  $wp_customize->add_section('heliotrope_design_form_labels', array(
    'title' => __( 'Form Labels', 'heliotrope' ),
    'description' => __( 'Customize the exact language of form labels and placeholders across the site.', 'heliotrope' ),
    'panel' => 'heliotrope_forms',
    'priority' => 120,
  ));


  $wp_customize->add_setting( 'signup_promo_text', array(
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_control( 'signup_promo_text',
    array(
      'type' => 'textarea',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_ctas', // Required, core or custom.
      'label' => __( 'Global call to action for signups', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 200,
        'placeholder' => __( 'e.g. Sign up now for updates', 'heliotrope' )
      ),
    )
  );

  $wp_customize->add_setting( 'submit_button_text', array(
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'submit_button_text',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_ctas', // Required, core or custom.
      'label' => __( 'Global Submit Button Text', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Sign Up',
      ),
    )
  );

  $wp_customize->add_setting( 'signup_thank_you_message', array(
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'signup_thank_you_message',
    array(
      'type' => 'textarea',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_ctas', // Required, core or custom.
      'label' => __( 'Global thank you message to users who\'ve filled out a form.', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 140,
        'placeholder' => __( 'Thanks for signing up! To complete the subscription process, please click the link in the email we just sent you.', 'heliotrope' )
      ),
    )
  );


  $wp_customize->add_setting( 'first_name_label', array(
      'default'     => __( 'First Name', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_setting( 'first_name_placeholder', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'first_name_label',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'First Name Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'First Name',
      ),
    )
  );

  $wp_customize->add_control( 'first_name_placeholder',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'First Name Field Placeholder', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Jill',
      ),
    )
  );

  $wp_customize->add_setting( 'last_name_label', array(
      'default'     => __( 'Last Name', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_setting( 'last_name_placeholder', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'last_name_label',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Last Name Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Last Name',
      ),
    )
  );

  $wp_customize->add_control( 'last_name_placeholder',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Last Name Field Placeholder', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Smith',
      ),
    )
  );

  $wp_customize->add_setting( 'email_label', array(
      'default'     => __( 'Email Address', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_setting( 'email_placeholder', array(
      'default'     => __( 'e.g. username@service.domain', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( 'email_label',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Email Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Email Address',
      ),
    )
  );

  $wp_customize->add_control( 'email_placeholder',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Email Field Placeholder', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'e.g. username@service.domain',
      ),
    )
  );

  $wp_customize->add_setting( 'zip_code_label', array(
      'default'     => __( 'Zip Code', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_setting( 'zip_code_placeholder', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'zip_code_label',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Zip Code Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => __( 'Zip Code', 'heliotrope' ),
      ),
    )
  );

  $wp_customize->add_control( 'zip_code_placeholder',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Zip Code Field Placeholder', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 10,
        'placeholder' => '#####-####',
      ),
    )
  );

  $wp_customize->add_setting( 'phone_label', array(
      'default'     => __( 'Phone Number', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_setting( 'phone_placeholder', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'phone_label',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Phone Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => 'Phone. Number',
      ),
    )
  );

  $wp_customize->add_control( 'phone_placeholder',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Phone Field Placeholder', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 20,
        'placeholder' => '###-###-####',
      ),
    )
  );

  $wp_customize->add_setting( 'disclaimer_label', array(
      'default'     => __( 'Disclaimer', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_attr',
  ));

  $wp_customize->add_setting( 'disclaimer_placeholder', array(
      'default'     => '',
      'transport'   => 'refresh',
      'sanitize_callback' => 'esc_html',
  ));

  $wp_customize->add_control( 'disclaimer_placeholder',
    array(
      'type' => 'textarea',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_design_form_labels', // Required, core or custom.
      'label' => __( 'Disclaimer Label', 'heliotrope' ),
      'capability' => 'edit_theme_options',
      'input_attrs' => array(
        'maxlength' => 500,
        'placeholder' => '*Mobile alerts from the SIEU. By providing your cell phone number you consent to receive periodic campaign updates from the SIEU. Txt HELP to xxxxx for help, STOP to xxxxx to end. Msg & Data rates may apply.',
      ),
    )
  );

}
