<?php

add_action( 'customize_register', 'heliotrope_404_customizer_settings' );
function heliotrope_404_customizer_settings( $wp_customize ) {

  // Add Footer Section
  $wp_customize->add_section('heliotrope_404', array(
    'title' => __( '404 Page', 'heliotrope' ),
    'description' => __( 'Below you can edit the 404 error page content. This is the page that people see whenever they type in a URL that is not found.', 'heliotrope' ),
    'priority' => 170,
  ));

  $wp_customize->add_setting( '404_headline', array(
      'default'     => __( '404: Page Not Found', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( '404_headline',
    array(
      'type' => 'text',
      'priority' => 10, // Within the section.
      'section' => 'heliotrope_404', // Required, core or custom.
      'label' => __( 'Headline', 'heliotrope' ),

      'input_attrs' => array(
        'step' => 1,
        'min' => 1900,
        'max' => gmdate('Y'),
        'placeholder' => __( '404', 'heliotrope' ),
      ),
    )
  );

  $wp_customize->add_setting( '404_message', array(
      'default'     => __( 'We could not locate that resource.', 'heliotrope' ),
      'transport'   => 'refresh',
      'sanitize_callback' => 'wp_kses_post',
  ));

  $wp_customize->add_control( new Heliotrope_TinyMCE_Custom_control( $wp_customize, '404_message',
      array(
        'priority' => 10, // Within the section.
        'section' => 'heliotrope_404', // Required, core or custom.
        'label' => __( '404 Page content', 'heliotrope' ),
        'description' => __( 'Additional information on 404 page', 'heliotrope' ),
        'input_attrs' => array(
          'toolbar1' => 'bold italic',
          'mediaButtons' => false,
          'placeholder' => __( 'Lorem Ipsum', 'heliotrope' ),
        ),
      )
    )
  );

}

