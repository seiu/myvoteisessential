/**
 * File customizer-controls.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to enhance native controls and fields in the theme.
 */
(function($) {
  wp.customize.bind('ready', () => {
    console.log('customizer panels ready');
    // this allows toggle boxes to dynamically show/hide fields when updated

    const toggle_controls = {
      use_typekit: ['typekit_kit_id'],
      use_google_fonts: ['google_fonts_url'],
      enable_alert_bar: [
        'alert_headline',
        'alert_link_url',
        'alert_link_title',
        'alert_is_cookied',
        'alert_only_once_per_session',
      ],
      alert_link_url: ['alert_link_title'],
      enable_splash_page: [
        'splash_page_url',
        'only_redirect_home',
        'only_direct_home',
        'is_splash_cookied',
        'splash_cookie_name',
        'cookie_length',
      ],
    };

    Object.keys(toggle_controls).forEach((controlId) => {
      $(`#customize-control-${controlId}`)
        .addClass('toggle-control')
        .data('controls', toggle_controls[controlId]);
    });

    $('.toggle-control input, .toggle-control select').each((i, el) => {
      const $control = $(el);
      const controlType = el.type;
      const fieldName = $control.data('customize-setting-link');
      const $li = $control.closest('.customize-control');
      const controlledControls = $li.data('controls') || [];

      wp.customize.control(fieldName, (control) => {
        const toggleControl = (value) => {
          console.log(fieldName, control, control.setting.get());
          if (
            control.active() &&
            value &&
            (controlType !== 'radio' || (value !== 'no' && value !== 'off'))
          ) {
            controlledControls.forEach((fieldName) => {
              const child_control = wp.customize.control(fieldName);
              if (child_control) {
                child_control.activate();
                child_control.toggle(true);
                console.log(`toggling ${fieldName} on`, value, child_control);
              }
            });
          } else {
            controlledControls.forEach((fieldName) => {
              const child_control = wp.customize.control(fieldName);
              if (child_control) {
                // child_control.deactivate();
                child_control.toggle(false);
                console.log(`toggling ${fieldName} off`, value, child_control);
              }
            });
          }
        };

        toggleControl(control.setting.get());
        control.setting.bind(toggleControl);
      });
    });

    // uses js to extract the href element of all pasted link tags
    $('[data-extract-href]').on('paste keyup', (e) => {
      const { value } = e.target;
      if (/<\/?[a-z][\s\S]*>/i.test(value)) {
        const $asNode = $(value);
        if ($asNode && $asNode.eq(0).is('link') && $asNode.attr('href')) {
          e.target.value = $asNode.attr('href');
        }
      }
    });

    $('.sortable_repeater_control').each(function() {
      // If there is an existing customizer value, populate our rows
      const defaultValuesArray = $(this)
        .find('.customize-control-sortable-repeater')
        .val()
        .split(',');
      const numRepeaterItems = defaultValuesArray.length;

      if (numRepeaterItems > 0) {
        if (numRepeaterItems === 1) {
          $(this)
            .find('.repeater')
            .addClass('only-row');
        }
        // Add the first item to our existing input field
        $(this)
          .find('.repeater-input')
          .val(
            String(defaultValuesArray[0]).substring(
              0,
              defaultValuesArray[0].indexOf('||'),
            ),
          );
        // Create a new row for each new value
        if (numRepeaterItems > 1) {
          let i;
          for (i = 1; i < numRepeaterItems; ++i) {
            appendRow(
              $(this),
              String(defaultValuesArray[i]).substring(
                0,
                defaultValuesArray[i].indexOf('||'),
              ),
            );
          }
        }
      }
    });

    $('body')
      .find('.sortable_repeater.sortables')
      .sortable({
        update: function(event, ui) {
          getAllInputs($(this).parent());
        },
      });

    $('.sortable_repeater.sortables').on(
      'click',
      '.customize-control-sortable-repeater-delete',
      function(event) {
        event.preventDefault();
        const numItems = $(this)
          .closest('.sortables')
          .find('.repeater').length;

        if (numItems > 1) {
          $(this)
            .closest('.repeater')
            .slideUp('fast', function() {
              const parentContainer = $(this).closest('.sortables');
              $(this).remove();
              getAllInputs(parentContainer);
            });
        } else {
          $(this)
            .closest('.repeater')
            .addClass('only-row')
            .find('.repeater-input')
            .val('');
          getAllInputs($(this).closest('.sortables'));
        }
      },
    );

    // Add new item
    $('.customize-control-sortable-repeater-add').click(function(event) {
      $(this)
        .parent()
        .find('.repeater')
        .removeClass('only-row');
      event.preventDefault();
      appendRow($(this).parent());
      getAllInputs($(this).parent());
    });

    // Refresh our hidden field if any fields change
    $('.sortable_repeater.sortables').change(function() {
      getAllInputs($(this).parent());
    });

    const parseLink = (string) => {
      let link = string;
      try {
        link = new URL(string);
      } catch (e) {
        return false;
      }
      return link;
    };

    // Append a new row to our list of elements
    function appendRow($element, defaultValue = '') {
      const newRow =
        '<div class="repeater" style="display:none"><input type="url" value="' +
        defaultValue +
        '" class="repeater-input" pattern="https?://.+" placeholder="https://" /><span class="dashicons dashicons-sort"></span><a class="customize-control-sortable-repeater-delete" href="#"><span class="dashicons dashicons-no-alt"></span></a></div>';

      $element.find('.sortables').append(newRow);
      $element
        .find('.sortables')
        .find('.repeater:last')
        .slideDown('fast', function() {
          $(this)
            .find('input')
            .focus();
        });
    }

    // Get the values from the repeater input fields and add to our hidden field
    function getAllInputs($element) {
      console.log($element);
      const inputValues = $element
        .find('.repeater-input')
        .filter((i, el) => String($(el).val()).trim())
        .map((i, el) => {
          const url = String($(el).val()).trim();
          let icon = 'link';
          if (parseLink(url)) {
            const { hostname } = parseLink(url);
            if (/facebook/.test(hostname)) {
              icon = 'facebook';
            }
            if (/twitter/.test(hostname)) {
              icon = 'twitter';
            }
            if (/instagram/.test(hostname)) {
              icon = 'instagram';
            }
            if (/linkedin/.test(hostname)) {
              icon = 'linkedin';
            }
            if (/tumblr/.test(hostname)) {
              icon = 'tumblr';
            }
            if (/reddit/.test(hostname)) {
              icon = 'reddit';
            }
            if (/pinterest/.test(hostname)) {
              icon = 'pinterest';
            }
            if (/tiktok/.test(hostname)) {
              icon = 'tiktok';
            }
          }

          return `${url}||${icon}`;
        })
        .toArray();
      console.log(inputValues);
      // Add all the values from our repeater fields to the hidden field (which is the one that actually gets saved)
      $element.find('.customize-control-sortable-repeater').val(inputValues);
      // Important! Make sure to trigger change event so Customizer knows it has to save the field
      $element.find('.customize-control-sortable-repeater').trigger('change');
    }

    // Add https:// to the start of the URL if it doesn't have it
    $('.sortable_repeater.sortables').on(
      'change',
      '.repeater-input',
      function() {
        const $input = $(this);
        const dirty = $input.hasClass('dirty');

        const val = String($input.val()).trim();
        console.log({ val, dirty });
        if (val) {
          $input.toggleClass('invalid', !parseLink(val));
        } else if (dirty) {
          $input.addClass('invalid');
        }
      },
    );

    $('.sortable_repeater.sortables').on(
      'input',
      '.repeater-input',
      function() {
        $(this).addClass('dirty');
      },
    );

    // Add https:// to the start of the URL if it doesn't have it
    $('.sortable_repeater.sortables').on('blur', '.repeater-input', function() {
      const $input = $(this);
      let val = String($input.val()).trim();
      if (val) {
        if (val && !val.match(/^.+:\/\/.*/)) {
          val = 'https://' + val;
        }
        $input.toggleClass('invalid', !parseLink(val));
        $input.val(val);
      } else {
        $input.addClass('invalid');
      }
      $input.trigger('change');
    });

    const is_alert_enabled =
      jQuery('[data-customize-setting-link="enable_alert_bar"]').val() ===
      'yes';
    let alert_headline_settings = false;

    wp.customize('alert_headline', (setting) => {
      setting.validate = function(value) {
        let notification;
        alert_headline_settings = setting;
        console.log(value);

        const code = 'required';
        if (
          !value &&
          jQuery('[data-customize-setting-link="enable_alert_bar"]').val() ===
            'yes'
        ) {
          notification = new wp.customize.Notification(code, {
            message: 'Alert text is required',
          });
          setting.notifications.add(code, notification);
        } else {
          setting.notifications.remove(code);
        }
        return value;
      };
    });

    wp.customize('enable_alert_bar', (setting) => {
      setting.validate = function(value) {
        if (value === 'no' && alert_headline_settings) {
          alert_headline_settings.notifications.remove('required');
        } else {
          setTimeout(() => {
            jQuery('textarea#alert_headline').trigger('change');
          }, 200);
        }
        return value;
      };
    });

    const is_redirect_enabled =
      jQuery('[data-customize-setting-link="enable_splash_page"]').val() ===
      'yes';
    let redirect_destination_settings = false;

    wp.customize('splash_page_url', (setting) => {
      setting.validate = function(value) {
        let notification;
        redirect_destination_settings = setting;

        const code = 'required';
        if (
          !value &&
          jQuery('[data-customize-setting-link="enable_splash_page"]').val() ===
            'yes'
        ) {
          notification = new wp.customize.Notification(code, {
            message: 'Redirect url is required',
          });
          setting.notifications.add(code, notification);
        } else {
          setting.notifications.remove(code);
        }
        return value;
      };
    });

    wp.customize('enable_splash_page', (setting) => {
      setting.validate = function(value) {
        if (value === 'no' && redirect_destination_settings) {
          redirect_destination_settings.notifications.remove('required');
        }
        return value;
      };
    });

    setTimeout(() => {
      jQuery('[data-customize-setting-link="splash_page_url"]').trigger(
        'change',
      );
      jQuery('textarea#alert_headline').trigger('change');
    }, 400);

    $('#save.save').on('click', () => {
      jQuery('[data-customize-setting-link="splash_page_url"]').trigger(
        'change',
      );
      jQuery('textarea#alert_headline').trigger('change');
    });

    // show the 404 page or footer when editing the 404 panel or footer items
    (function(api) {
      api.section('heliotrope_404', (section) => {
        let previousUrl;
        let clearPreviousUrl;
        let previewUrlValue;
        previewUrlValue = api.previewer.previewUrl;
        clearPreviousUrl = function() {
          previousUrl = null;
        };

        section.expanded.bind((isExpanded) => {
          let url;
          if (isExpanded) {
            url =
              api.settings.url.home +
              '404?' +
              Math.random()
                .toString(36)
                .substring(2, 15) +
              Math.random()
                .toString(36)
                .substring(2, 15);
            previousUrl = previewUrlValue.get();
            previewUrlValue.set(url);
            console.log({ home: api.settings.url.home, url });
            previewUrlValue.bind(clearPreviousUrl);
          } else {
            previewUrlValue.unbind(clearPreviousUrl);
            if (previousUrl) {
              previewUrlValue.set(previousUrl);
            }
          }
        });
      });

      api.section('heliotrope_theme_footer', (section) => {
        section.expanded.bind((isExpanded) => {
          let url;
          if (isExpanded) {
            console.log('footer expanded');
            api.previewer.send('scroll-to', '#site-footer');
          } else {
            console.log('footer closed');
            api.previewer.send('unscroll', '');
          }
        });
      });
    })(wp.customize);
  });
})(jQuery);
