/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */
const currentYear = new Date().getFullYear();
console.log('loading custom customizer js');
(function($) {
  const { customize } = wp;

  wp.customize('alert_headline', (value) => {
    value.bind((to) => {
      $('.alert-bar__simple').html(to);
    });
  });

  wp.customize('copyright_start_year', (value) => {
    value.bind((to) => {
      const yearRange =
        currentYear === to ? currentYear : `${to}&ndash;${currentYear}`;
      $('.copyright-years').html(yearRange);
    });
  });

  wp.customize('copyright_title', (value) => {
    value.bind((to) => {
      $('.copyright-holder').html(to);
    });
  });

  wp.customize('social_urls', (value) => {
    // console.log('SOCIAL URL value', value);
    value.bind((to) => {
      console.log('SOCIAL URL to', to);
      if (!to) {
        return false;
      }
      const $section = $('.social-org-links');
      let $existing = $section.find('ul.social-links');
      const rows = to.split(',').map((row) => row.split('||'));
      if (!$existing.length) {
        $existing = $('<ul class="social-links/>');
        $section.append($existing);
      }
      $existing.html(`
        ${rows
          .map(
            ([url, name]) =>
              `<li class="col-auto"><a rel="me" href="${url}" target="_blank" rel="noopener"><img src="/wp-content/themes/heliotrope/assets/icons/${name}.svg" onload="fetch(this.currentSrc||this.src).then(x=>x.text()).then(svgText=>{this.outerHTML = svgText})"/><span class="screen-reader-text">Link to ${name}</span></a></li>`,
          )
          .join('')}
      `);
    });
  });

  wp.customize.bind('preview-ready', () => {
    console.log('custom customizer js loaded', window.name);

    let prevY = 0;
    const previewWindow = window;

    customize.preview.bind('scroll-to', (data) => {
      const el = document.querySelector(data);
      prevY = previewWindow.scrollY;
      console.log(
        'scroll-to',
        data,
        el,
        prevY,
        window.name,
        previewWindow && previewWindow.name,
      );
      if (data === '#site-footer') {
        window.scrollTo(0, window.document.body.scrollHeight);
        setTimeout(
          () => window.scrollTo(0, window.document.body.scrollHeight),
          600,
        );
      } else if (el) {
        el.scrollIntoView(true);
      }
    });

    customize.preview.bind('unscroll', () => {
      console.log('scrollBack', prevY, previewWindow && previewWindow.name);
      previewWindow.scrollTo(0, prevY);
    });
  });
})(jQuery);
