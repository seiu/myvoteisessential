/**
 * File customizer.js.
 *
 * Theme Customizer enhancements for a better user experience.
 *
 * Contains handlers to make Theme Customizer preview reload changes asynchronously.
 */

const DOWN_ARROW_KEY_CODE = 40;
const ENTER_KEY_CODE = 13;
const UP_ARROW_KEY_CODE = 38;

(function($) {
  wp.customize.bind('ready', () => {
    $('.customize-control-tinymce-editor').each(function() {
      // Get the toolbar strings that were passed from the PHP Class
      const tinyMCEToolbar1String =
        _wpCustomizeSettings.controls[$(this).attr('id')].tinymcetoolbar1;
      const tinyMCEToolbar2String =
        _wpCustomizeSettings.controls[$(this).attr('id')].tinymcetoolbar2;
      const tinyMCEMediaButtons =
        _wpCustomizeSettings.controls[$(this).attr('id')].mediabuttons;
      const tinyMCENoBR =
        _wpCustomizeSettings.controls[$(this).attr('id')].nobr;

      const config = {
        tinymce: {
          wpautop: false,
          toolbar1: tinyMCEToolbar1String,
          toolbar2: tinyMCEToolbar2String,
        },
        quicktags: false,
        mediaButtons: tinyMCEMediaButtons,
      };

      if (tinyMCENoBR) {
        config.tinymce.setup = function(editor) {
          editor.on('keyDown', (e) => {
            if (e.keyCode === ENTER_KEY_CODE) {
              editor.dom.events.cancel(e);
              tinymce.dom.Event.cancel(e);
              e.preventDefault();
              e.stopPropagation();
              e.stopImmediatePropagation();

              return false;
            }
          });
          // run parent method
          return editor;
        };
      }

      wp.editor.initialize($(this).attr('id'), config);
    });
    $(document).on('tinymce-editor-init', (event, editor) => {
      editor.on('change', (e) => {
        tinyMCE.triggerSave();
        $('#' + editor.id).trigger('change');
      });
    });
  });
})(jQuery);
