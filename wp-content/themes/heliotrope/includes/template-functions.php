<?php

add_filter( 'body_class', function( $classes ) {
  $extra_body_classes =  array();
  if ( 'page_overlay' == get_theme_mod( 'search_design', 'off' ) ) {
    $extra_body_classes[] = 'has-overlay-search';
  }
  elseif ( 'pull_down' == get_theme_mod( 'search_design', 'off' ) ) {
    $extra_body_classes[] = 'has-pulldown-search';
  }
  return array_merge( $classes, $extra_body_classes );
});


//adds nojs and vh unit script hints
add_action( 'wp_head', function() {
  ?>
  <script>!function(e){"function"==typeof define&&define.amd?define("bundleActionnetworkLabels",e):e()}((function(){"use strict";var e=document.documentElement,n=e.style;e.className=e.className.replace("no-js","js");var r=function(){return n.setProperty("--vh","".concat(.01*window.innerHeight,"px"))};r(),window.setVHUnit=r,window.llItems=[];var t=function(e){e=String(e).replace(/(\[|\])/g,"\\$1");var n=new RegExp("[\\?&]"+e+"=([^&#]*)").exec(window.location.href);return null===n?"":n[1]};document.addEventListener("can_embed_loaded",(function(){var e=t("source"),n=t("referrer");e&&window.URL&&setTimeout((function(){!function(e,n,r){var t=document.querySelector(e);if(t&&t.href){var o=t.href;if(o){var c=new URL(o);c.searchParams.set("source",n),c.searchParams.set("referrer",r),t.href=c}}}("#host_button",e,n)}),100)}))}));</script>
  <?php
}, 999);


function add_gtm_to_head() {
  $gtm_container_id = trim( get_theme_mod( 'gtm_container_id' ) );
  ?><script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','<?php echo esc_html( $gtm_container_id ); ?>');</script><?php
}

function add_ga_to_footer() {
  $ga_id = trim( get_theme_mod( 'ga_id' ) );
  ?><!-- Google Analytics --><script>(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)})(window,document,'script','//www.google-analytics.com/analytics.js','ga');ga('create', '<?php echo esc_html( $ga_id ); ?>', 'auto');ga('send', 'pageview');</script><?php
}

function add_fbpixel_to_footer() {
  $facebook_pixel_id = trim( get_theme_mod( 'facebook_pixel_id' ) );
  $facebook_adpixel_id = trim( get_field( 'fbad_init_id' ) );//overrides
  if($facebook_adpixel_id){
    $facebook_pixel_id = $facebook_adpixel_id;
  }
  ?><!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '<?php echo esc_html( $facebook_pixel_id ); ?>');
  fbq('track', 'PageView');
  <?php
    while ( have_rows( 'fbad_track_event' ) ) :
      the_row(); ?>
      fbq('track', '<?php echo wp_kses_post( get_sub_field('event') ) ?>' );
      <?php
    endwhile;
  ?>
</script>
<noscript><img height="1" width="1" style="display:none"
  src="https://www.facebook.com/tr?id=<?php echo esc_html( $facebook_pixel_id ); ?>&ev=PageView&noscript=1"
/></noscript>
<!-- End Facebook Pixel Code --><?php
}

function add_gtm_post_body_open() {
  $gtm_container_id = trim( get_theme_mod( 'gtm_container_id' ) );
  ?>
  <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=<?php echo esc_attr( $gtm_container_id ); ?>" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
  <?php
}

function add_bugherd_script() {
  $bugherd_key = get_theme_mod( 'bugherd_key' );
  ?>
    <script type='text/javascript'>
    (function (d, t) {
    var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];bh.type = 'text/javascript';bh.async = true;bh.defer = true;bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=<?php echo esc_attr( $bugherd_key ); ?>';s.parentNode.insertBefore(bh, s);
    })(document, 'script')
    </script><?php
}

// adds resources to the header
add_action( 'wp_enqueue_scripts', function() {

  //adds custom fonts
  if (
    get_theme_mod( 'use_typekit' ) && get_theme_mod( 'typekit_kit_id' )
    ) :
      $typekit_handle = 'typekit_css';
      $typekit_url = 'https://use.typekit.net/' . get_theme_mod( 'typekit_kit_id' ) . '.css';
      wp_enqueue_style( 'typekit_css', esc_url( $typekit_url ), array( 'theme_css' ) );
  endif;
  if (
    get_theme_mod( 'use_google_fonts' ) && get_theme_mod( 'google_fonts_url' )
    ) :
    wp_enqueue_style( 'google_fonts_defer', esc_url( get_theme_mod( 'google_fonts_url' ) ), array( 'theme_css' ), null, 'print' );
    add_action( 'wp_head', function(){
      ?><link href="https://fonts.gstatic.com" rel="preconnect" as="style" crossorigin><?php
    }, 2 );
    
  endif;

  //add gtm_container_id
  $gtm_container_id = trim( get_theme_mod( 'gtm_container_id' ) );

  if ( $gtm_container_id ) {
    add_action( 'wp_head', 'add_gtm_to_head', 2 );
    add_action( 'wp_body_open', 'add_gtm_post_body_open' );
  }

  //add ga_id
  $ga_id = trim( get_theme_mod( 'ga_id' ) );

  if ( $ga_id ) {
    add_action( 'wp_footer', 'add_ga_to_footer', 9999 );
  }

  //add add_fbpixel_to_footer
  $facebook_pixel_id = trim( get_theme_mod( 'facebook_pixel_id' ) );

  if ( $facebook_pixel_id ) {
    add_action( 'wp_footer', 'add_fbpixel_to_footer', 9999 );
  }

  //add bugherd, conditionally
  $bugherd_key = get_theme_mod( 'bugherd_key' );
  $host = isset( $_SERVER['HTTP_HOST'] ) ? esc_url_raw( wp_unslash( $_SERVER['HTTP_HOST'] ) ) : '';
  if ( $host
    && $bugherd_key
    && ( false !== strpos( $host, 'ideeyeclient' ) || false !== strpos( $host, 'local' ) )
    && ( '1w7sloltbyfmu99xygy7gw' != $bugherd_key
    || false !== strpos( $host, 'tarter' ) )
  ) {
      add_filter( 'wp_footer', 'add_bugherd_script', 9999 );
      add_action( 'admin_footer_text', 'add_bugherd_script', 9999 );
  }

});

// adds custom javascript to admin <footer>
add_action( 'admin_footer_text', 'heliotrope_bugherd_admin_enqueue' );
function heliotrope_bugherd_admin_enqueue( $hook ) {
  $bugherd_key = get_theme_mod( 'bugherd_key' );
  $host = isset( $_SERVER['HTTP_HOST'] ) ? esc_url_raw( wp_unslash( $_SERVER['HTTP_HOST'] ) ) : '';
  if ( $host
    && $bugherd_key
    && ( false !== strpos( $host, 'ideeyeclient' ) || false !== strpos( $host, 'local' ) )
    && ( '1w7sloltbyfmu99xygy7gw' != $bugherd_key
    || false !== strpos( $host, 'tarter' ) )
  ) {
      add_action( 'admin_footer_text', 'add_bugherd_script', 9999 );
  }
}