<?php

// function heliotrope_add_role_caps() {
//     // Gets the simple_role role object.
//     $role = get_role( 'administrator' );

//     // Add a new capability.
//     $role->add_cap( 'edit_advanced_theme_options', true );
// }

// // Add simple_role capabilities, priority must be after the initial role definition.
// add_action( 'init', 'heliotrope_add_role_caps', 11 );

// adds an extra user field
function modify_contact_methods($profile_fields) {

  // Add new fields
  $profile_fields['user_job_title'] = 'Organizational Title';

  return $profile_fields;
}
add_filter('user_contactmethods', 'modify_contact_methods');