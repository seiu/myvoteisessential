<?php

/*
  Add any custom shortcodes
*/
 
add_shortcode('child_pages_menu', 'heliotrope_list_child_pages_sc');
function heliotrope_list_child_pages_sc() {
  $childpages = heliotrope_list_child_pages();
  if ( $childpages ) {
    return '<nav class="nav-list">' . $childpages . '</nav>';
  }
  return '';
}

