<?php
/*
 * This file manages all API-related customization.
 */

// By default, disable the api entirely and remove reference to it
// remove_action( 'wp_head', 'rest_output_link_wp_head', 10 );
// add_filter(
// 	'rest_authentication_errors',
// 	function( $result ) {
// 		if ( ! empty( $result ) ) {
// 			return $result;
// 		}

// 		if ( ! is_user_logged_in() ) {
// 			return new WP_Error( 'rest_not_logged_in', 'Only authenticated users can access the REST API.', array( 'status' => 401 ) );
// 		}

// 		return $result;
// 	}
// );
