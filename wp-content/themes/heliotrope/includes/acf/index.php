<?php

if ( function_exists( 'acf_add_local_field_group' ) ) :
  include get_template_directory() . '/includes/acf/hooks.php';
  // include get_template_directory() . '/includes/acf/counter/class-acf-input-counter.php';
  include get_template_directory() . '/includes/acf/acf-input-counter/class-acf-input-counter.php';
  include get_template_directory() . '/includes/acf/field-groups.php';
endif;

// (Optional) Hide the ACF admin menu item.
// add_filter('acf/settings/show_admin', 'my_acf_settings_show_admin');
// function my_acf_settings_show_admin( $show_admin ) {
//     return false;
// }
