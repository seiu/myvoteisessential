<?php
  if ( function_exists( 'acf_add_local_field_group' ) ) :
  	include get_template_directory() . '/includes/acf/groups/modules.php';
  	include get_template_directory() . '/includes/acf/groups/feature-bottom-cta.php';
    include get_template_directory() . '/includes/acf/groups/page-home.php';
    include get_template_directory() . '/includes/acf/groups/fb-ads-pixel.php';
    include get_template_directory() . '/includes/acf/groups/post-images.php';
    include get_template_directory() . '/includes/acf/groups/topper.php';
  endif;
