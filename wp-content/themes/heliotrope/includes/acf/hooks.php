<?php

// ACF hooks

if ( function_exists( 'acf_add_local_field_group' ) ) {

  // Add a default Google API key if one doesn't exist in options.
  add_action( 'acf/init', function() {
    $google_api_key = get_option( 'google_maps_api_key' );
    if ( ! $google_api_key ) {
      $google_api_key = 'AIzaSyDF9vNrgEmUHFM8C_fXPeuBRqWVemuCCn0';
    }
    return acf_update_setting( 'google_api_key', $google_api_key );
  }, 1 );

  // Sets the Google API key for specific fields
  add_filter( 'acf/fields/google_map/api', function( $api ) {
    $google_api_key = get_option( 'google_maps_api_key' );
    if ( ! $google_api_key ) {
      $google_api_key = 'AIzaSyDF9vNrgEmUHFM8C_fXPeuBRqWVemuCCn0';
    }
    $api['key'] = $google_api_key;
    return $api;
  } );

  // forces particular images in ACF to set the thumbanil as well
  // function acf_set_featured_image( $value, $post_id, $field ) {
  //     // _yoast_wpseo_opengraph-image
  //     // _yoast_wpseo_opengraph-image-id
  //     if ( '' != $value ) {
  //       update_post_meta( $post_id, '_thumbnail_id', $value );
  //     }

  //     return $value;
  // }
  // add_filter( 'acf/update_value/name=topper_image', 'acf_set_featured_image', 10, 3 );

}
