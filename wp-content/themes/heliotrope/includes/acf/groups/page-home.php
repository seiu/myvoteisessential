<?php
  acf_add_local_field_group(array(
    'key' => 'group_97fe2352ecab66470',
    'title' => 'Settings: Home',
    'location' => array(
      array(
        array(
          'param' => 'page_type',
          'operator' => '==',
          'value' => 'front_page',
        ),
      ),
    ),
    'position' => 'acf_after_title',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'hide_on_screen' => array(
      0 => 'the_content',
    ),
  ));

  /* Topper Tab */
  acf_add_local_field(array(
    'key' => 'field_587e7da7faca0',
    'label' => 'Home Topper',
    'type' => 'tab',
    'placement' => 'left',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5762b3b3b9bbc',
    'label' => 'Type',
    'name' => 'topper_type',
    'type' => 'select',
    'wrapper' => array(
      'width' => 40,
    ),
    'choices' => array(
      'color' => 'Solid Background Color',
      'image' => 'Image Background',
    ),

    'return_format' => 'value',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5762b500b9bc0',
    'label' => 'Background Image',
    'name' => 'topper_image',
    'type' => 'image',
    'instructions' => '- Standard: 1250px x 750px minimum<br>- Retina Display (*ideal*): 2500px x 1500px',
    'required' => 1,
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_5762b3b3b9bbc',
          'operator' => '==',
          'value' => 'image',
        ),
      ),
    ),
    'return_format' => 'array',
    'preview_size' => 'topper-image-small',
    'library' => 'all',
    'min_width' => 1250,
    'min_height' => 750,
    'max_size' => 3,
    'wrapper' => array(
      'width' => 40,
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  // acf_add_local_field(array(
  //   'key' => 'field_5ab188d0ce6ed',
  //   'label' => 'Alternate Mobile Background (optional)',
  //   'name' => 'topper_mobile_background',
  //   'type' => 'select',
  //   'conditional_logic' => array(
  //     array(
  //       array(
  //         'field' => 'field_5762b3b3b9bbc',
  //         'operator' => '==',
  //         'value' => 'image',
  //       ),
  //     ),
  //   ),
  //   'choices' => array(
  //     'none' => 'None',
  //     'color' => 'Color',
  //     'image' => 'Image',
  //   ),
  //   'wrapper' => array(
  //     'width' => 60,
  //   ),
  //   'return_format' => 'value',
  //   'parent' => 'group_5762b33aae2c6',
  // ));

  // acf_add_local_field(array(
  //   'key' => 'field_5ab1286dda817',
  //   'label' => 'Alternate Mobile Background Image',
  //   'name' => 'topper_mobile_image',
  //   'type' => 'image',
  //   'instructions' => '- Standard: 400px x 600px<br>- Retina Display (*ideal*): 800px x 1200px',
  //   'conditional_logic' => array(
  //     array(
  //       array(
  //         'field' => 'field_5762b3b3b9bbc',
  //         'operator' => '==',
  //         'value' => 'image',
  //       ),
  //       array(
  //         'field' => 'field_5ab188d0ce6ed',
  //         'operator' => '==',
  //         'value' => 'image',
  //       ),
  //     ),
  //   ),
  //   'return_format' => 'array',
  //   'preview_size' => 'mobile-tall-topper-image-small',
  //   'wrapper' => array(
  //     'width' => 40,
  //   ),
  //   'library' => 'all',
  //   'min_width' => 400,
  //   'min_height' => 600,
  //   'max_size' => 2,
  //   'parent' => 'group_97fe2352ecab66470',
  // ));
  
  acf_add_local_field(array(
    'key' => 'field_5762b55499429',
    'label' => 'Headline',
    'name' => 'large_text',
    'type' => 'text',
    'maxlength' => 70,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5887a862ecea0',
    'label' => 'Deck',
    'name' => 'deck',
    'type' => 'textarea',
    'instructions' => 'Description accompanying the headline',
    'rows' => 5,
    'new_lines' => 'wpautop',
    'maxlength' => 400,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5887a862ecea01',
    'label' => 'Deck Small Text',
    'name' => 'deck_small_text',
    'type' => 'text',
    'instructions' => 'Description accompanying the deck text',
    'maxlength' => 70,
    'parent' => 'group_97fe2352ecab66470',
  ));

  /* Introduction Tab */
  acf_add_local_field(array(
    'key' => 'field_587e7ef79568a',
    'label' => 'Introduction/Sign Up',
    'type' => 'tab',
    'placement' => 'left',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_587e7f5795690',
    'label' => 'Hide this section?',
    'name' => 'introduction_hide_this_section',
    'type' => 'true_false',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_587e7f089568b',
    'label' => 'Headline',
    'name' => 'introduction_headline',
    'type' => 'textarea',
    'rows' => 5,
    'maxlength' => 425,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_587e7f089568b0ea',
    'label' => 'CRM Headline',
    'name' => 'crm_headline',
    'type' => 'text',
    'maxlength' => 25,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_59f23b7f6b825ea',
    'label' => 'CRM Integration',
    'name' => 'default_integration',
    'type' => 'select',
    'wrapper' => array(
      'width' => 50,
    ),
    'choices' => array(
      'ngp-van' => 'EveryAction (NGP VAN 8)',
    ),
    'return_format' => 'value',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_59f23d1a1cf00',
    'label' => 'Form ID',
    'name' => 'ngp_everyaction_form_id',
    'type' => 'text',
    'instructions' => 'Enter the Form ID only. This is the string of text at the end of the "Share Link" URL. Example: https://secure.everyaction.com/FORM_ID or https://actions.ngpvan.com/FORM_ID',
    'required' => 1,
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_59f23b7f6b825',
          'operator' => '==',
          'value' => 'ngp-van',
        ),
      ),
    ),
    'wrapper' => array(
      'width' => 50,
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_59f23d425ff80',
    'label' => 'Redirect',
    'name' => 'ngp_redirect_url',
    'type' => 'text',
    'conditional_logic' => array(
      array(
        array(
          'field' => 'field_59f23b7f6b825',
          'operator' => '==',
          'value' => 'ngp-van',
        ),
      ),
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  /* Text Tab */
  acf_add_local_field(array(
    'key' => 'field_9c71fb3c94c5bb6600',
    'label' => 'Text CTA',
    'type' => 'tab',
    'placement' => 'top',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_9c71fb3c94c5bb6601',
    'label' => 'Hide this section?',
    'name' => 'text_cta_hide_this_section',
    'type' => 'true_false',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_9c71fb3c94c5bb6602',
    'label' => 'Headline',
    'name' => 'text_cta_headline',
    'type' => 'text',
    'maxlength' => 75,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_9c71fb3c94c5bb6603',
    'label' => 'Description',
    'name' => 'text_cta_description',
    'type' => 'wysiwyg',
    'tabs' => 'all',
    'toolbar' => 'full',
    'media_upload' => 0,
    'parent' => 'group_97fe2352ecab66470',
  ));

  /* Actions Tab */
  acf_add_local_field(array(
    'key' => 'field_588285702fafa',
    'label' => 'Actions',
    'type' => 'tab',
    'placement' => 'top',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5882858f2fafb',
    'label' => 'Hide this section?',
    'name' => 'actions_hide_this_section',
    'type' => 'true_false',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_5882858f2fafb0ea',
    'label' => 'Headline',
    'name' => 'actions_headline',
    'type' => 'text',
    'maxlength' => 100,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_588285ab2fafd',
    'label' => 'Description',
    'name' => 'actions_description',
    'type' => 'textarea',
    'rows' => 5,
    'new_lines' => 'wpautop',
    'maxlength' => 200,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_0f5f987ace7f',
    'label' => 'Call to Action Links ',
    'name' => 'actions_links',
    'type' => 'repeater',
    'min' => 2,
    'max' => 3,
    'layout' => 'block',
    'button_label' => 'Add Link',
    'sub_fields' => array(
      array(
        'key' => 'field_09c9ffca2c35ea0',
        'label' => 'Link Icon',
        'name' => 'link_icon',
        'type' => 'select',
        'choices' => array(
          'no-icon' => 'No Icon',
          'find' => 'Find An Event',
          'strike' => 'Strike',
          'host' => 'Host An Event',
        ),
        'return_format' => 'value',
      ),
      array(
        'key' => 'field_09c9ffca2c35',
        'label' => 'Link Text',
        'name' => 'link_text',
        'type' => 'text',
      ),
      array(
        'key' => 'field_d59f79de9e46',
        'label' => 'Link',
        'name' => 'link',
        'type' => 'text',
        'wrapper' => array(
          'width' => 75,
        ),
      ),
      array(
        'key' => 'field_114dd6cfda72',
        'label' => 'Open in a new window?',
        'name' => 'link_open_in_a_new_window',
        'type' => 'true_false',
        'wrapper' => array(
          'width' => 25,
        ),
      ),  
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  /* Demands Tab */
  acf_add_local_field(array(
    'key' => 'field_bce341ac7fcbe0ec00',
    'label' => 'Demands',
    'type' => 'tab',
    'placement' => 'top',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_bce341ac7fcbe0ec01',
    'label' => 'Hide this section?',
    'name' => 'demands_hide_this_section',
    'type' => 'true_false',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_bce341ac7fcbe0ec02',
    'label' => 'Background Image',
    'name' => 'demands_background_image',
    'type' => 'image',
    'instructions' => '- Standard: 1250px x 750px minimum<br>- Retina Display (*ideal*): 2500px x 1500px',
    'return_format' => 'array',
    'preview_size' => 'topper-image-small',
    'library' => 'all',
    'min_width' => 1250,
    'min_height' => 750,
    'max_size' => 3,
    'wrapper' => array(
      'width' => 40,
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_bce341ac7fcbe0ec03',
    'label' => 'Headline',
    'name' => 'demands_headline',
    'type' => 'text',
    'maxlength' => 100,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_bce341ac7fcbe0ec04',
    'label' => 'Description',
    'name' => 'demands_description',
    'type' => 'wysiwyg',
    'tabs' => 'all',
    'toolbar' => 'full',
    'media_upload' => 0,
    'parent' => 'group_97fe2352ecab66470',
  ));

  /* Social Share Tab */
  acf_add_local_field(array(
    'key' => 'field_5987b3b12c3b',
    'label' => 'Social Share/Downloads',
    'type' => 'tab',
    'placement' => 'top',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_ef057d20cee8',
    'label' => 'Hide this section?',
    'name' => 'social_share_downloads_hide_this_section',
    'type' => 'true_false',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_ebe71d721d7f',
    'label' => 'Headline',
    'name' => 'social_share_downloads_headline',
    'type' => 'text',
    'maxlength' => 100,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_f6906d05181c',
    'label' => 'Description',
    'name' => 'social_share_downloads_description',
    'type' => 'textarea',
    'rows' => 5,
    'new_lines' => 'wpautop',
    'maxlength' => 200,
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key'        => 'field_5061769545',
    'label'      => 'Social Image(s)',
    'name'       => 'social_images',
    'type'       => 'repeater',
    'layout'     => 'block',
    'sub_fields' => array(
      array(
        'key'           => 'field_6797216699',
        'label'         => 'Social Image',
        'name'          => 'social_image',
        'return_format' => 'array',
        'preview_size'  => 'preview-square',
        'type'          => 'image',
        'library'       => 'all',
        'min_width'     => 500,
        'required'      => 1,
        'wrapper' => array(
          'width' => 50,
        ),
      ),
      array(
        'key'           => 'field_2427372740',
        'label'         => 'Social Image File',
        'name'          => 'social_image_file',
        'type'          => 'file',
        'return_format' => 'url',
        'preview_size'  => 'preview-square',
        'required'      => 1,
        'max'        => 2,
        'layout'     => 'block',
        'wrapper' => array(
          'width' => 50,
        ),
      ),
      array(
        'key' => 'field_f6906d05181c0ea',
        'label' => 'Facebook Share URL',
        'name' => 'social_share_downloads_facebook',
        'instructions' => 'This photo with text must already be posted on Facebook. Copy the URL from the photo lightbox option on Facebook and paste here. (e.g. https://www.facebook.com/photo/?fbid=3262163687148104&set=pob.100000635135744)',
        'type' => 'textarea',
        'rows' => 2,
        'new_lines' => 'wpautop',
      ),
      array(
        'key' => 'field_f6906d05181c0e1',
        'label' => 'Twitter Tweet Intent Text and URL',
        'name' => 'social_share_downloads_twitter',
        'instructions' => 'This photo must already be posted on Twitter. Copy the pic.twitter.com URL from the embed tweet option on Twitter and paste here along with any share text. (e.g. Text about this image. pic.twitter.com/qtdwnHo9Xl)',
        'type' => 'textarea',
        'rows' => 2,
        'new_lines' => 'wpautop',
      ),
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_8e306ab1bba7',
    'label' => 'Learn more button text',
    'name' => 'social_share_learn_more_button_text',
    'type' => 'text',
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_1d4f5f41e345',
    'label' => 'Learn more link',
    'name' => 'social_share_learn_more_link',
    'type' => 'text',
    'wrapper' => array(
      'width' => 75,
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));

  acf_add_local_field(array(
    'key' => 'field_efcd94d02212',
    'label' => 'Open in a new window?',
    'name' => 'social_share_open_in_a_new_window',
    'type' => 'true_false',
    'wrapper' => array(
      'width' => 25,
    ),
    'parent' => 'group_97fe2352ecab66470',
  ));