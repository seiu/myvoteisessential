<?php

  $default_modules = array(
    'buttons',
    'sign-up',
    'full-width-content',
    'list',
  );

  $default_locations = array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
      array(
        'param' => 'page_type',
        'operator' => '!=',
        'value' => 'front_page',
      ),
      array(
        'param' => 'page',
        'operator' => '!=',
        'value' => get_option( 'page_for_posts' ),
      ),
      array(
        'param' => 'page_template',
        'operator' => '!=',
        'value' => 'template-an-events.php',
      ),
    ),
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'post',
      ),
    ),
  );

  build_modules_group($default_modules, $default_locations, 'default');

  /**
   *  Function: process_layouts
   *
   *  This processes layouts into an array
   *
   * @param array A list of modules that should be included in a specific module group
   * @param string $unique_id A string that is appended to custom fields to prevent conflcits
   * @return string $case A specific case that can feed into a switch function as needed
   */

  function process_layouts( $modules, $unique_id, $case ) {
      $modules_list = array();
      foreach ( $modules as $module ) {
        $module_config = include 'modules/' . $module . '.php';
        $modules_list = array_merge($modules_list, $module_config);
      }
      return $modules_list;
    }


  /**
   *  Function build_modules_group
   *
   *  This function creates an ACF local field group based for modules based on parameters included
   *
   * @param array $modules A list of modules that should be included in a specific module group
   * @param array $locations A list of locations where this module group should display
   * @param string $unique_id A string that is appended to custom fields to prevent conflcits
   * @param string $case A specific case that can feed into a switch function as needed
   * @return function that creates ACF field group
   */
  function build_modules_group( $modules, $locations, $unique_id, $case = 'default' ) {
    $layouts = process_layouts($modules, $unique_id, $case);

    /* Create field group */
    acf_add_local_field_group(array(
      'key' => 'group_we_starter_modules' . $unique_id,
      'title' => 'Page Content',
      'fields' => array(),
      'location' => $locations,
      'menu_order' => 0,
      'hide_on_screen' => array(
        0 => 'the_content',
      ),
    ));

    /* Add modules field */
    acf_add_local_field(array(
      'key' => 'field_57689e5790ef6' . $unique_id,
      'parent' => 'group_we_starter_modules' . $unique_id,
      'label' => 'Sections',
      'name' => 'modules',
      'type' => 'flexible_content',
      'layouts' => $layouts,
      'button_label' => 'Add Section',
    ));
  }