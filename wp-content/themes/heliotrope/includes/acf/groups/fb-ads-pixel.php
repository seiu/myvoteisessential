<?php
if( function_exists('acf_add_local_field_group') ):

acf_add_local_field_group(array(
  'key' => 'group_5f076daba5b6b',
  'title' => 'FB Ads pixels',
  'fields' => array(
    array(
      'key' => 'field_5f076db6d81ef',
      'label' => 'Init Id',
      'name' => 'fbad_init_id',
      'type' => 'text',
      'instructions' => 'Override the existing (Optional)',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'wpml_cf_preferences' => 0,
      'default_value' => '',
      'placeholder' => 'e.g. 1742293926005727',
      'prepend' => '',
      'append' => '',
      'maxlength' => 20,
    ),
    array(
      'key' => 'field_5f076defd81f0',
      'label' => 'Events Tracked',
      'name' => 'fbad_track_event',
      'type' => 'repeater',
      'instructions' => 'Add extra tracking events (single string events only)',
      'required' => 0,
      'conditional_logic' => 0,
      'wrapper' => array(
        'width' => '',
        'class' => '',
        'id' => '',
      ),
      'collapsed' => '',
      'min' => 0,
      'max' => 10,
      'layout' => 'table',
      'button_label' => '',
      'wpml_cf_preferences' => 0,
      'sub_fields' => array(
        array(
          'key' => 'field_5f076e07d81f1',
          'label' => 'Event Name',
          'name' => 'event',
          'type' => 'text',
          'instructions' => 'Event name such as "Lead." PageView is included by default.',
          'required' => 0,
          'conditional_logic' => 0,
          'wrapper' => array(
            'width' => '',
            'class' => '',
            'id' => '',
          ),
          'wpml_cf_preferences' => 0,
          'default_value' => '',
          'placeholder' => 'e.g. Lead',
          'prepend' => '',
          'append' => '',
          'maxlength' => '',
        ),
      ),
    ),
  ),
  'location' => array(
    array(
      array(
        'param' => 'post_type',
        'operator' => '==',
        'value' => 'page',
      ),
    ),
  ),
  'menu_order' => 99,
  'position' => 'normal',
  'style' => 'default',
  'label_placement' => 'top',
  'instruction_placement' => 'label',
  'hide_on_screen' => '',
  'active' => true,
  'description' => 'Override normal ads pixel with a custom one, plus extra events.',
));

endif;