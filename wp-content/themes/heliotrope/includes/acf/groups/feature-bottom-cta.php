<?php
  acf_add_local_field_group(array(
    'key' => 'group_57688cc66ff05',
    'title' => 'Bottom CTA',
    'location' => array(
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'post',
        ),
      ),
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'page',
        ),
        array(
          'param' => 'page',
          'operator' => '!=',
          'value' => get_option( 'page_on_front' ),
        ),
      ),
      array(
        array(
          'param' => 'post_type',
          'operator' => '==',
          'value' => 'issue',
        ),
      ),
    ),
    'menu_order' => 3,
    'position' => 'normal',
    'style' => 'default',
    'label_placement' => 'top',
    'instruction_placement' => 'label',
    'active' => true,
  ));

  acf_add_local_field(array(
    'key' => 'field_57688cd6632c8',
    'label' => 'Disable Bottom CTA',
    'name' => 'disable_bottom_cta',
    'type' => 'button_group',
    'wrapper' => array(
      'width' => '50',
    ),
    'choices' => array(
      'false' => 'No',
      'true' => 'Yes',
    ),
    'default_value' => 'false',
    'layout' => 'horizontal',
    'return_format' => 'value',
    'parent' => 'group_57688cc66ff05',
  ));