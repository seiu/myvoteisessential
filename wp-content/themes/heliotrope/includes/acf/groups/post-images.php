<?php
  acf_add_local_field_group(array(
    'key' => 'group_5c0d788c398a2',
    'title' => 'Image Fields',
    'location' => array(
      array(
        array(
          'param' => 'attachment',
          'operator' => '==',
          'value' => 'image',
        ),
      ),
    ),
  ));

  acf_add_local_field(array(
    'key' => 'field_5c0d78c71c303',
    'label' => 'Photo Credit',
    'name' => 'photocredit',
    'type' => 'text',
    'instructions' => 'Any copyright or photo credit information for this image.',
    'maxlength' => 120,
    'parent' => 'group_5c0d788c398a2',
  ));

  acf_add_local_field(array(
    'key' => 'field_5c0d78da1c304',
    'label' => 'Gradient',
    'name' => 'gradient',
    'type' => 'textarea',
    'instructions' => '(Advanced feature) Include a comma-separated list of CSS color stops for a top to bottom image gradient representation of this image.',
    'placeholder' => 'rgba(108,124,136,1), blue, hsla(146, 100%, 50%, 1), green',
    'rows' => 4,
    'parent' => 'group_5c0d788c398a2',
  ));

  acf_add_local_field(array(
    'key' => 'field_5c0e8023787aa',
    'label' => 'Dominant Color',
    'name' => 'dominant_color',
    'type' => 'text',
    'instructions' => '(Advanced feature) A single color that best represents this image.',
    'wrapper' => array(
      'width' => '40',
    ),
    'placeholder' => 'Any single, valid CSS color.',
    'maxlength' => 24,
    'parent' => 'group_5c0d788c398a2',
  ));
