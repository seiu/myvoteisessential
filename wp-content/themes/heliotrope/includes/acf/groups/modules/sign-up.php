<?php
  if ( ! function_exists( 'build_sign_up' ) ) {
    function build_sign_up() {
      $sub_fields = array();

      $sub_fields[] = array(
        'key' => 'field_93a2907969f215b00',
        'label' => 'CRM Integration',
        'name' => 'default_integration',
        'type' => 'select',
        'wrapper' => array(
          'width' => 50,
        ),
        'choices' => array(
          'ngp-van' => 'EveryAction (NGP VAN 8)',
        ),
    '   return_format' => 'value',
      );

      $sub_fields[] = array(
        'key' => 'field_93a2907969f215b01',
        'label' => 'Form ID',
        'name' => 'ngp_everyaction_form_id',
        'type' => 'text',
        'instructions' => 'Enter the Form ID only. This is the string of text at the end of the "Share Link" URL. Example: https://secure.everyaction.com/FORM_ID or https://actions.ngpvan.com/FORM_ID',
        'required' => 1,
        'conditional_logic' => array(
          array(
            array(
              'field' => 'field_93a2907969f215b00',
              'operator' => '==',
              'value' => 'ngp-van',
            ),
          ),
        ),
        'wrapper' => array(
          'width' => 50,
        ),
      );

      $module = array(
        '93a2907969f215b02' => array(
          'key' => '93a2907969f215b02',
          'label' => 'Sign Up',
          'name' => 'sign_up',
          'display' => 'block',
          'sub_fields' => $sub_fields,
        ),
      );

      return $module;
    }
  }

  return build_sign_up();