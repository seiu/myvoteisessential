<?php
  if ( ! function_exists( 'build_list' ) ) {
    function build_list() {
      $sub_fields = array();

      $sub_fields[] = array(
        'key' => 'field_5ab97ed94a77',
        'label' => 'Headline',
        'name' => 'headline',
        'type' => 'text',
        'instructions' => 'Optional section introduction',
        'maxlength' => 150,
      );

      $sub_fields[] = array(
        'key' => 'field_5a948174878a',
        'label' => 'Description',
        'name' => 'description',
        'type' => 'wysiwyg',
        'maxlength' => 250,
        'rows' => '3',
        'tabs' => 'visual',
        'toolbar' => 'minimal',
        'media_upload' => 0,
      );

      $sub_fields[] = array(
        'key' => 'field_594826287li',
        'label' => 'List Item(s)',
        'name' => 'list_items',
        'type' => 'repeater',
        'layout' => 'row',
        'min' => 1,
        'sub_fields' => array(
          array(
            'key' => 'field_5aj9487f487li',
            'label' => 'Item Description',
            'name' => 'description',
            'type' => 'wysiwyg',
            'required' => 1,
            'wrapper' => array(
              'width' => '',
              'class' => 'short-deck',
              'id' => '',
            ),
            'default_value' => '',
            'tabs' => 'full',
            'toolbar' => 'minimal',
            'maxlength' => 1000,
            'media_upload' => 0,
            'delay' => 0,
          ),
        ),
      );

      $module = array(
        '5ab93ebf487e6' => array(
          'key' => '5ab93ebf487e6',
          'name' => 'list',
          'label' => 'List',
          'display' => 'block',
          'sub_fields' => $sub_fields,
        ),
      );

      return $module;
    }
  }

 return build_list();