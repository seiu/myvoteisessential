<?php
  if ( ! function_exists('build_buttons') ) {
    function build_buttons() {
      $sub_fields = array();

      $sub_fields[] = array(
        'key' => 'field_587fc4fc596e7',
        'label' => 'Add Buttons',
        'name' => 'add_buttons',
        'type' => 'repeater',
        'required' => 1,
        'min' => 1,
        'max' => 6,
        'layout' => 'block',
        'wpml_cf_preferences' => '1',
        'button_label' => 'Add Button',
        'sub_fields' => array(
          array(
            'key' => 'field_587fc516596e8',
            'label' => 'Button',
            'name' => 'button_link',
            'type' => 'link',
            'instructions' => '',
            'required' => 1,
            'conditional_logic' => 0,
            'wrapper' => array(
              'width' => '',
              'class' => 'ctl-button',
              'id' => '',
            ),
            'wpml_cf_preferences' => '1',
            'return_format' => 'array',
          ),
        ),
      );

      $module = array(
        '587fc4f3596e6' => array(
          'key' => '587fc4f3596e6',
          'name' => 'buttons',
          'label' => 'Buttons',
          'display' => 'block',
          'sub_fields' => $sub_fields,
        ),
      );

      return $module;
    }
  }

return build_buttons();
