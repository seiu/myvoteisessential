<?php
  if ( ! function_exists( 'build_full_width_content' ) ) {
    function build_full_width_content() {
      $sub_fields = array();

      $sub_fields[] = array(
        'key' => 'field_57697c43fc4b8',
        'label' => 'Content',
        'name' => 'content',
        'type' => 'wysiwyg',
        'tabs' => 'all',
        'toolbar' => 'full',
        'media_upload' => 1,
      );

      $module = array(
        '57697c37fc4b7' => array(
          'key' => '57697c37fc4b7',
          'label' => 'Full Width Content',
          'name' => 'full_width_content',
          'display' => 'block',
          'sub_fields' => $sub_fields,
        ),
      );

      return $module;
    }
  }

  return build_full_width_content();
