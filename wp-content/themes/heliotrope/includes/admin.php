<?php
/*
 * This file manages all admin-related config.
 */

// Prevents anyone from editing theme/plugin php directly on the server
define( 'DISALLOW_FILE_EDIT', true );

// adds arbitrary custom css we might want to add to things
add_action( 'admin_enqueue_scripts', 'load_admin_style' );
function load_admin_style() {
  wp_enqueue_style( 'we_admin_css', we_asset_path('css/admin.min.css'), false, '1.0.0' );
  if ( get_theme_mod( 'use_google_fonts' ) && get_theme_mod( 'google_fonts_url' ) ) :
    wp_enqueue_style( 'google_fonts', esc_url( get_theme_mod( 'google_fonts_url' ) ), array( 'theme_css' ), null );
  endif;
}

// adds custom javascript to admin <head>
add_action( 'admin_enqueue_scripts', 'my_enqueue' );
function my_enqueue( $hook ) {
	wp_enqueue_script( 'my_custom_script', we_asset_path('js/bundle-admin.min.js'), false, 'v1' );
}

// adds a css class for the current user to admin body class
add_filter( 'admin_body_class', 'we_add_admin_body_class' );
function we_add_admin_body_class( $classes ) {
    global $post;
    if ($post) {
        $is_homepage_class = ( get_option( 'page_on_front' ) === $post->ID ) ? 'is-home' : '';
        return "$classes $is_homepage_class user-" . get_current_user_id() . " post_id-" . $post->ID;
    }
}

// Remove certain menu options from the back end.
// Remove comments and comment editing
add_action(
	'admin_menu',
	function() {
		remove_menu_page( 'edit-comments.php' );
	}
);

// remove page attributes for the head
add_action('admin_head', function () {
    global $post;
    if( $post && $post->ID === get_option( 'page_on_front' ) ) {
        remove_meta_box('pageparentdiv', 'page', 'side');
    }
});

add_action('admin_init', function () {
    // Redirect any user trying to access comments page
    global $pagenow;

    if ( 'edit-comments.php' === $pagenow ) {
        wp_redirect(admin_url());
        exit;
    }

    // Remove comments metabox from dashboard
    remove_meta_box( 'dashboard_recent_comments', 'dashboard', 'normal' );

    // Disable support for comments and trackbacks in post types
    foreach ( get_post_types() as $post_type ) {
        if ( post_type_supports( $post_type, 'comments' ) ) {
            remove_post_type_support( $post_type, 'comments' );
            remove_post_type_support( $post_type, 'trackbacks' );
        }
    }
});

// Close comments on the front-end
add_filter('comments_open', '__return_false', 20, 2);
add_filter('pings_open', '__return_false', 20, 2);

// Hide existing comments
add_filter('comments_array', '__return_empty_array', 10, 2);

// Remove comments page in menu
add_action('admin_menu', function () {
    remove_menu_page('edit-comments.php');
});

// Remove comments links from admin bar
add_action('init', function () {
    if ( is_admin_bar_showing() ) {
        remove_action('admin_bar_menu', 'wp_admin_bar_comments_menu', 60);
    }
});

// Adds a link to edit the current Home Page
add_action(
    'admin_menu',
    function() {
        add_menu_page(
            null, // not an actual page, so title is irrelevant
            __( 'Home Page', 'wectheme' ),
            'edit_posts', // or whatever capability required for this object
            '/post.php?post='. get_option('page_on_front') .'&action=edit',
            null,
            'dashicons-admin-home',
            5
        );
    }
);

// customize the login screen
function wpexplorer_login_logo() { ?>
    <style type="text/css">
        body.login div#login h1 a{
            display:block;
            background-image: url(<?php echo get_site_icon_url( 'large', get_stylesheet_directory_uri() . '/screenshot.png'); ?> );
            background-size: contain;
            width:100%;
        }
    </style>
<?php }
add_action( 'login_enqueue_scripts', 'wpexplorer_login_logo' );

function wpexplorer_login_logo_url() {
    return esc_url( home_url( '/' ) );
}
add_filter( 'login_headerurl', 'wpexplorer_login_logo_url' );

function wpexplorer_login_logo_url_title() {
    return get_bloginfo( 'name' ) . ' : ' . get_bloginfo( 'description' );
}
add_filter( 'login_headertext', 'wpexplorer_login_logo_url_title' );