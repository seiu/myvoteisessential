<?php
/*
 * This file manages all registration of custom menus used in the theme.
 */

register_nav_menus(
  array(
    'primary_navigation'          => esc_html__( 'Primary Site Navigation', 'heliotrope' ),
    'secondary_navigation'        => esc_html__( 'Secondary Site Navigation', 'heliotrope' ),
    'primary_footer_navigation'   => esc_html__( 'Primary Footer Navigation', 'heliotrope' ),
    'secondary_footer_navigation' => esc_html__( 'Secondary Footer Navigation', 'heliotrope' ),
    'mobile_navigation'           => esc_html__( 'Mobile Navigation', 'heliotrope' ),
  )
);

// allows extra classes to be attached to the nav itself
function we_menu_add_class( $atts, $item, $args, $depth ) {
  if ( isset( $args->navlink_class ) && $args->navlink_class ) {
    $atts['class'] = $args->navlink_class;
  }
  if ( isset( $args->link_class ) && $args->link_class ) {
    $atts['class'] = $args->link_class;
  }
  return $atts;
}
add_filter( 'nav_menu_link_attributes', 'we_menu_add_class', 10, 4 );

// allows extra classes to be attached to all li items
function add_additional_class_on_li( $classes, $item, $args ) {
  if( !$item->menu_item_parent ){
    if ( isset( $args->add_li_class ) && $args->add_li_class ) {
      $classes[] = $args->add_li_class;
    }
    if ( is_object($args) && property_exists( $args, 'list_item_class' ) ) {
      $classes[] = $args->list_item_class;
    }
    if (!$item->menu_item_parent){
      $classes[] = 'top-level-menu-item';
    }
  }
  return $classes;
}
add_filter( 'nav_menu_css_class', 'add_additional_class_on_li', 1, 3 );

/**
 * Gets count and string length of a menu
 *
 * @param {Int} $menu_id
 * @return {Array}
 */
function we_count_top_level_menu_items( $menu_id ) {

  $count = 0;
  $length = 0;
  $menu_items = wp_get_nav_menu_items( $menu_id );

  if ( is_array( $menu_items ) ) {
    foreach ( $menu_items as $menu_item ) {
      if ( 0 == $menu_item->menu_item_parent ) {
        $count++;
        $length = $length + strlen( $menu_item->title );
      }
    }
  }

  return array( $count, $length );
}

/**
 * Remove the href from empty links `<a href="#">` in the nav menus
 * @param string $menu the current menu HTML
 * @return string the modified menu HTML
 */
add_filter( 'wp_nav_menu_items', function ( $menu ) {
  return preg_replace( '/<a>(.*?)<\/a>/s', '<span class="nav-category">$1</span>', $menu );
} );
/**
 * Returns sizing classes for menu
 *
 * @param {Int} $menu_id
 * @return {Array}
 */
function we_get_menu_size_data( $menu_id = 'primary-navigation' ) {
  list( $prim_nav_menu_count, $prim_nav_length ) = we_count_top_level_menu_items( $menu_id );

  $sizing_classes = '' .
  ( $prim_nav_menu_count > 4 ? ' over-four-items' : '' ) .
  ( $prim_nav_menu_count > 5 ? ' over-five-items' : '' ) .
  ( $prim_nav_menu_count > 6 ? ' over-six-items' : '' ) .
  ( $prim_nav_length > 40 ? ' over-fourty-characters' : '' ) .
  ( $prim_nav_length > 50 ? ' over-fifty-characters' : '' ) .
  ( $prim_nav_length > 60 ? ' over-sixty-characters' : '' );

  return array( $sizing_classes, $prim_nav_menu_count, $prim_nav_length );
}