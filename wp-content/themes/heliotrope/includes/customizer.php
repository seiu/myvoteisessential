<?php
/**
 * Heliotrope Theme Customizer
 *
 * @package heliotrope
 */

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
function heliotrope_customize_register( $wp_customize ) {
	$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
	$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
	$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	$wp_customize->remove_section( 'static_front_page' );

	if ( isset( $wp_customize->selective_refresh ) ) {
		$wp_customize->selective_refresh->add_partial( 'blogname', array(
			'selector'        => '.site-title a',
			'render_callback' => 'heliotrope_customize_partial_blogname',
		) );
		$wp_customize->selective_refresh->add_partial( 'blogdescription', array(
			'selector'        => '.site-description',
			'render_callback' => 'heliotrope_customize_partial_blogdescription',
		) );
	}
}
add_action( 'customize_register', 'heliotrope_customize_register' );

/**
 * Render the site title for the selective refresh partial.
 *
 * @return void
 */
function heliotrope_customize_partial_blogname() {
	bloginfo( 'name' );
}

/**
 * Render the site tagline for the selective refresh partial.
 *
 * @return void
 */
function heliotrope_customize_partial_blogdescription() {
	bloginfo( 'description' );
}

/**
 * Binds JS handlers to make Theme Customizer preview reload changes asynchronously.
 */
function heliotrope_customize_preview_js() {
	if( is_customize_preview() ) {
		wp_enqueue_script( 'heliotrope-customizer', get_template_directory_uri() . '/includes/customizer/js/customizer-preview.js', array( 'jquery', 'customize-preview' ), '20151215', true );
	}
}
add_action( 'customize_preview_init', 'heliotrope_customize_preview_js' );

function heliotrope_customize_controls_js() {
	wp_enqueue_script( 'heliotrope-customizer', get_template_directory_uri() . '/includes/customizer/js/customizer-controls.js', array( 'jquery' ), '20151215', true );
}
add_action( 'customize_controls_enqueue_scripts', 'heliotrope_customize_controls_js' );

/**
 * Binds customizer styles.
 */
function heliotrope_enqueue_customizer_stylesheet() {

	wp_register_style( 'heliotrope-customizer-css', get_template_directory_uri() . '/includes/customizer/css/customizer.css', null, 'v1', 'all' );
	wp_enqueue_style( 'heliotrope-customizer-css' );

}
add_action( 'customize_controls_print_styles', 'heliotrope_enqueue_customizer_stylesheet' );


require get_template_directory() . '/includes/customizer/class-heliotrope-custom-control.php';
/**
 * Customizer additions.
 */
require get_template_directory() . '/includes/customizer/sections/design.php';
require get_template_directory() . '/includes/customizer/sections/footer.php';
require get_template_directory() . '/includes/customizer/sections/alerts.php';
require get_template_directory() . '/includes/customizer/sections/tracking.php';
require get_template_directory() . '/includes/customizer/sections/404.php';
require get_template_directory() . '/includes/customizer/sections/site-redirect.php';
require get_template_directory() . '/includes/customizer/sections/advanced.php';

