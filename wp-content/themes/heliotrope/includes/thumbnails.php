<?php
/*
 * This file defines custom thumbnail sizes for the theme.
 */

// Remove all the default image sizes here.
add_filter( 'intermediate_image_sizes_advanced', 'we_prefix_remove_default_images', 1, 1 );
function we_prefix_remove_default_images( $image_sizes ) {
	return array_diff( $image_sizes, [ 'thumbnail', 'small', 'medium', 'large', 'medium_large', '1536x1536', '2048x2048' ] );
}
add_filter(
	'intermediate_image_sizes',
	function( $image_sizes ) {
		return array_diff( $image_sizes, [ 'thumbnail', 'small', 'medium', 'large', 'medium_large', '1536x1536', '2048x2048' ] );
	},
	999
);

// allow wordpress to save a size that's close to the thumbnail size
function thumbnail_upscale( $default, $orig_w, $orig_h, $new_w, $new_h, $crop ){
    if ( !$crop ) return null; // let the wordpress default function handle this

    $aspect_ratio = $orig_w / $orig_h;
    $size_ratio = max($new_w / $orig_w, $new_h / $orig_h);

    $crop_w = round($new_w / $size_ratio);
    $crop_h = round($new_h / $size_ratio);

    $s_x = floor( ($orig_w - $crop_w) / 2 );
    $s_y = floor( ($orig_h - $crop_h) / 2 );

    return array( 0, 0, (int) $s_x, (int) $s_y, (int) $new_w, (int) $new_h, (int) $crop_w, (int) $crop_h );
}
add_filter( 'image_resize_dimensions', 'thumbnail_upscale', 10, 6 );


// Image Sizes, created on upload/regeneration. Adjust to your theme needs
$we_image_sizes = array(
	// square sizes (shares)
	array( 'square', 300, 300, true ),
	array( 'medium_square', 600, 600, true ),

	// featured feed items
	array( 'featured_item_small', 550, 370, true ),
	array( 'featured_item', 1100,  740, true ),

	// toppers
	array( 'topper-image', 1600, 916, true ),// fullscreen toppers x1
  	array( 'topper-image-retina', 2400, 1374, true ),// largest we're willing to go, x1.5

	//letterbox/featured content (height will vary)
	array( '390x390', 390, 390 ),
	array( '780x780', 780, 780 ),
	array( '1260x1260', 1260, 1260 ),
	array( '1560x1560', 1560, 1560 ),
	array( '1912x1912', 1912, 1912 ),

);

foreach ( $we_image_sizes as $image_size ) {
	call_user_func_array( 'add_image_size', $image_size );
}

/**
 * Set max srcset image width to 2400px if the added image's main size is equal to or wider than 1600px
 */
function we_set_max_srcset_image_width( $max_width, $size_array ) {
	$width = $size_array[0];

	if ( $width >= 1800 ) {
		$max_width = 2401;
	}

	return $max_width;
}
add_filter( 'max_srcset_image_width', 'we_set_max_srcset_image_width', 1, 2 );


// do not allow WP to create attachments pages by default....
function myprefix_redirect_attachment_page() {
	if ( is_attachment() ) {
		global $post;
		if ( $post && $post->post_parent ) {
			wp_safe_redirect( esc_url( get_permalink( $post->post_parent ) ), 301 );
			exit;
		} else {
			wp_safe_redirect( esc_url( home_url( '/' ) ), 301 );
			exit;
		}
	}
}
add_action( 'template_redirect', 'myprefix_redirect_attachment_page' );

