<?php
/*
 * This file manages all loading of styles and scripts for the theme, as well as
 * any modifications of enqueue hooks.
 * 
 * javascript resource slugs with async or "defer" in the title will get those attributes
 * style tags with "defer" will be lazy-loaded
 */

add_action(
  'wp_enqueue_scripts',
  function() {

    // main styles
    wp_enqueue_style(
        'theme_css',
        we_asset_path('css/style.min.css'),
        false,
        'v1',
        'all'
      );

    // scripts
    wp_enqueue_script(
      'vendors_js_defer',
      we_asset_path('js/vendors.min.js'),
      array(),
      'v1',
      true
    );

    wp_enqueue_script(
      'theme_js_defer',
      we_asset_path('js/master.min.js'),
      array( 'vendors_js_defer' ),
      'v1',
      true
    );

    wp_enqueue_script(
      'share_intents_async_defer',
      we_asset_path('js/vendor/share.intents.js'),
      array( 'theme_js_defer' ),
      'v1',
      true
    );

    wp_enqueue_script(
      'add_this_async_defer',
      '//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-51c770552f90ce31',
      array( 'theme_js_defer' ),
      'v1',
      true
    );
    wp_add_inline_script( 'add_this_async_defer', 'var addthis_config={"data_track_addressbar":false};', 'before' );

  },
  10
);


// these hooks look for async or defer in the resource key
// then they add those properties to the script
function add_async_attributes( $tag, $handle ) {
    if ( strpos( $handle, 'async' ) === false ){
      return $tag;
    }
    return str_replace( ' src', ' async src', $tag );
}

function add_defer_attributes( $tag, $handle ) {
    if ( strpos($handle, 'defer') === false ){
      return $tag;
    }
    return str_replace( ' src', ' defer src', $tag );
}
function add_stylesheet_attributes( $html, $handle ) {
    if ( strpos( $handle, 'defer' ) !== false ) {
        $revert_print = str_replace( "'print'", "'all'", $html );
        return str_replace( "/>", "onload=\"this.media='all'\"/><noscript>". $revert_print ."</noscript>", $html );
    }
    return $html;
}
add_filter( 'style_loader_tag', 'add_stylesheet_attributes', 10, 2 );
add_filter( 'script_loader_tag', 'add_async_attributes', 10, 2 );
add_filter( 'script_loader_tag', 'add_defer_attributes', 10, 2 );