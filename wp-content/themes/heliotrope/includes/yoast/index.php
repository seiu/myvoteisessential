<?php
// moves Yoast metabox lower by default
function we_move_yoast_below_acf() {
  return 'low';
}
add_filter( 'wpseo_metabox_prio', 'we_move_yoast_below_acf');

/**
 * Disable Yoast's Hidden love letter about using the WordPress SEO plugin.
 */
add_action( 'template_redirect', function () {
 
    if ( ! class_exists( 'WPSEO_Frontend' ) ) {
        return;
    }
 
    $instance = WPSEO_Frontend::get_instance();
 
    // make sure, future version of the plugin does not break our site.
    if ( ! method_exists( $instance, 'debug_mark') ) {
        return ;
    }
 
    // ok, let us remove the love letter.
     remove_action( 'wpseo_head', array( $instance, 'debug_mark' ), 2 );
}, 9999 );

// helper function to retrieve a link to the "primary" category for a resource
function heliotrope_get_primary_taxonomy_term( $post = 0, $taxonomy = 'category' ) {
    if ( ! $post ) {
        $post = get_the_ID();
    }

    $terms        = get_the_terms( $post, $taxonomy );
    $primary_term = array();

    if ( $terms ) {
        $term_display = '';
        $term_slug    = '';
        $term_link    = '';
        if ( class_exists( 'WPSEO_Primary_Term' ) ) {
            $wpseo_primary_term = new WPSEO_Primary_Term( $taxonomy, $post );
            $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
            $term               = get_term( $wpseo_primary_term );
            if ( is_wp_error( $term ) ) {
                $term_display = $terms[0]->name;
                $term_slug    = $terms[0]->slug;
                $term_link    = get_term_link( $terms[0]->term_id );
            } else {
                $term_display = $term->name;
                $term_slug    = $term->slug;
                $term_link    = get_term_link( $term->term_id );
            }
        } else {
            $term_display = $terms[0]->name;
            $term_slug    = $terms[0]->slug;
            $term_link    = get_term_link( $terms[0]->term_id );
        }
    }
    return '<a href="' . $term_link . '" rel="category tag">' . $term_display . '</a>';
}