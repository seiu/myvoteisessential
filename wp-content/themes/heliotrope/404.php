<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package heliotrope
 */

get_header();
$we_four_oh_four_header = get_theme_mod( '404_headline', '404: Page Not Found' );
$we_four_oh_four_description = get_theme_mod( '404_message', 'We could not locate that resource.' );
?>
	<header data-js-social-hide class="topper">
		<div class="container">
			<div class="row justify-content-center">
				<h1 class="page-title col-md-8"><?php echo wp_kses_post( $we_four_oh_four_header ); ?></h1>
			</div>
		</div>
	</header>
	<section id="content">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="wysiwyg-text" style="text-align:center">
						<br/>
						<br/>
						<br/>
						<?php echo wp_kses_post( $we_four_oh_four_description ); ?>
					</div>

				</div>
			</div>
		</div>
	</section>
<?php
get_footer();
