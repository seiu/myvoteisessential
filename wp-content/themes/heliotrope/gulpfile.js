const fs = require('fs');

// Includes
//------------------------------------------------------------------------------

let userDefaults = {};
try {
  userDefaults = JSON.parse(
    fs.readFileSync(`${__dirname}/gulpconfig.json`, 'utf8'),
  );
} catch (e) {
  console.log('No user config, create one @ /gulpconfig.json');
}
if (
  userDefaults &&
  typeof userDefaults !== 'object' &&
  userDefaults.constructor !== Object
) {
  throw new Error('Config file must be a valid JSON object');
}

const gulp = require('gulp');

const { src, dest, series, parallel, watch, lastRun } = gulp;

// js tasks
const eslint = require('gulp-eslint');
const babel = require('rollup-plugin-babel');
const rollup = require('gulp-better-rollup');
const plumber = require('gulp-plumber');
const resolve = require('rollup-plugin-node-resolve');
const commonjs = require('rollup-plugin-commonjs');
const terser = require('gulp-terser');
const buffer = require('gulp-buffer');

// sass tasks
const autoprefixer = require('autoprefixer');
const sass = require('gulp-dart-sass');
const sourcemaps = require('gulp-sourcemaps');
const postcss = require('gulp-postcss');
const cssnano = require('cssnano');
const gulpStylelint = require('gulp-stylelint');

// image tasks
const newer = require('gulp-newer');
const imagemin = require('gulp-imagemin');
const svgo = require('gulp-svgmin');
const svg2png = require('gulp-svg2png');
// const svgo = require('gulp-svgo');

// php linting tasks
const phpcs = require('gulp-phpcs');
const phpcbf = require('gulp-phpcbf');

// asset revisioning and reporting tasks
const revFormat = require('gulp-rev-format');
const revDel = require('rev-del');
const rev = require('gulp-rev');
const sizereport = require('gulp-sizereport');

// gulp dev build tasks
const browserSync = require('browser-sync').create();

const { reload } = browserSync;
const header = require('gulp-header');
const opn = require('open');
const del = require('del');
const notify = require('gulp-notify');
const minimist = require('minimist');
const path = require('path');
const rename = require('gulp-rename');
const using = require('gulp-using');
const pjson = require('./package.json');

// require('gulp-stats')(gulp);

const defaults = {
  bs: true,
  localDomain:
    (pjson.config && pjson.config.localDomain) || 'starterlite.local',
  nolint: false,
  open: false,
  silent: false,
  ...userDefaults,
  env: process.env.NODE_ENV || 'development',
};

const scriptGlobs = [
  'src/js/**/*.js',
  'src/plugins/**/*.js',
  '!node_modules/**',
  '!src/js/plugins/**/*.js',
  '!src/js/vendor/**/*.js',
];

const topLevelScriptGlob = ['src/js/*.js'];

const bundlesGlob = ['src/js/bundle*.js'];

const masterOnlyGlob = ['src/js/master.js'];

const vendorGlob = ['src/js/vendors.js', 'src/js/vendors/**/*.js'];

const pluginsGlob = ['src/plugins/**/*.js'];

const vendorOnlyGlob = ['src/js/vendors.js'];

const masterOrImports = [
  'src/js/master.js',
  'src/js/config/**/*.js',
  'src/js/helpers/**/*.js',
  'src/js/lib/**/*.js',
];

const helperGlob = [
  'src/js/config/**/*.js',
  'src/js/helpers/**/*.js',
  'src/js/lib/**/*.js',
];

const knownOptions = {
  string: 'env localserver',
  boolean: 'silent nolint open',
  default: defaults,
};

const options = minimist(process.argv.slice(2), knownOptions);

const sounds = !options.silent
  ? {
      error: 'Sosumi',
      success: 'Pop',
    }
  : {
      error: false,
      success: false,
    };

const visibleOptions = Object.keys(options).reduce(
  (acc, x) =>
    x in defaults && options[x] ? Object.assign(acc, { [x]: options[x] }) : acc,
  {},
);

console.log(
  `running w/ options: ${JSON.stringify(visibleOptions)
    .split(',"')
    .join(', "')}`,
);

/* PHP Code Sniffer and Beautifier */
const phpFileGlob = ['**/*.php', '!vendor/**/*.*', '!functions/social/**/*.*'];
const phpCSExcludes = [
  'PEAR.WhiteSpace.ScopeIndent',
  'Squiz.Commenting.FileComment',
  'Generic.WhiteSpace.ScopeIndent',
  'Generic.WhiteSpace.DisallowSpaceIndent',
  'Squiz.Commenting.FunctionComment',
  'WordPress.Arrays.ArrayIndentation',
  // 'WordPress.WP.EnqueuedResourceParameters.MissingVersion',
  'PEAR.Functions.FunctionCallSignature',
  'WordPress.WP.EnqueuedResources',
  // 'Squiz.PHP.EmbeddedPhp.ContentBeforeOpen',
  // 'Squiz.PHP.EmbeddedPhp.ContentBeforeEnd',
  'Squiz.Commenting.InlineComment',
];

const bsreload = function(done) {
  if (typeof done === 'function') {
    done();
  }
  return reload();
};

const phpcsTask = () =>
  src(options.singleFile ? options.singleFile : phpFileGlob)
    .pipe(
      phpcs({
        bin: '../../../vendor/bin/phpcs', // path.normalize('phpcs'),
        showSniffCode: true,
        standard: 'WordPress', //  WordPressVIPMinimum', // 'WordPress-VIP-Go', // 'WordPress',
        warningSeverity: 0,
        colors: true,
        exclude: phpCSExcludes,
      }),
    )
    .on('error', (e) => console.log(e))
    // Log all problems that was found
    .pipe(phpcs.reporter('log'));

const styleLintTask = () =>
  src('src/scss/**/*.scss').pipe(
    gulpStylelint({
      fix: true,
      configBasedir: './',
      failAfterError: false,
      reporters: [{ formatter: 'string', console: true }],
    }),
  );

const stylesTask = () =>
  src(['src/scss/master.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('style.dev.css'))
    .pipe(dest('assets/css'))
    .pipe(
      rename((filename) => {
        filename.basename = filename.basename.replace('.dev', '.min');
      }),
    )
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(header('/*${date}*/\n', { date: new Date() }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('assets/css'))
    .pipe(
      browserSync.stream({
        /* match: 'assets/*.min.css' */
      }),
    )
    .pipe(
      notify({
        sound: sounds.success,
        title: 'Styles complete',
        message: '<%= file.relative %>',
        onLast: true,
        icon:
          'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678134-sign-check-512.png',
      }),
    )
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

const styleGuideTask = () =>
  src(['src/scss/style-guide.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('style-guide.dev.css'))
    .pipe(dest('assets/css'))
    .pipe(
      rename((filename) => {
        filename.basename = filename.basename.replace('.dev', '.min');
      }),
    )
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(header('/*${date}*/\n', { date: new Date() }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('assets/css'))
    .pipe(
      browserSync.stream({
        /* match: 'assets/*.min.css' */
      }),
    )
    .pipe(
      notify({
        sound: sounds.success,
        title: 'Styles complete',
        message: '<%= file.relative %>',
        onLast: true,
        icon:
          'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678134-sign-check-512.png',
      }),
    )
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

const adminStyleTask = () =>
  src(['src/scss/admin.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('admin.dev.css'))
    .pipe(dest('assets/css'))
    .pipe(
      rename((filename) => {
        filename.basename = filename.basename.replace('.dev', '.min');
      }),
    )
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(header('/*${date}*/\n', { date: new Date() }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('assets/css'))
    .pipe(
      browserSync.stream({
        /* match: 'assets/*.min.css' */
      }),
    )
    .pipe(
      notify({
        sound: sounds.success,
        title: 'Admin Styles complete',
        message: '<%= file.relative %>',
        onLast: true,
        icon:
          'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678134-sign-check-512.png',
      }),
    )
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

// TinyMCE editor
const editorStyleTask = () =>
  src(['src/scss/editor-style.scss'])
    .pipe(sourcemaps.init())
    .pipe(sass().on('error', sass.logError))
    .pipe(rename('editor-style.dev.css'))
    .pipe(dest('assets/css'))
    .pipe(
      rename((filename) => {
        filename.basename = filename.basename.replace('.dev', '.min');
      }),
    )
    .pipe(postcss([autoprefixer(), cssnano()]))
    .pipe(header('/*${date}*/\n', { date: new Date() }))
    .pipe(sourcemaps.write('./'))
    .pipe(dest('assets/css'))
    .pipe(
      browserSync.stream({
        /* match: 'assets/*.min.css' */
      }),
    )
    .pipe(
      notify({
        sound: sounds.success,
        title: 'Editor Styles complete',
        message: '<%= file.relative %>',
        onLast: true,
        icon:
          'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678134-sign-check-512.png',
      }),
    )
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

// minifies base images
const imagesTask = () =>
  src(['src/img/**/*'], { since: lastRun(imagesTask) })
    .pipe(newer('assets/img'))
    .pipe(
      imagemin([
        imagemin.svgo({
          plugins: [
            { removeViewBox: false },
            { removeDesc: false },
            { removeTitle: false },
            { removeUselessDefs: false },
            {
              cleanupIDs: {
                // remove: false,
                minify: false,
              },
            },
          ],
        }),
      ]),
    )
    .pipe(dest('assets/img'))
    .pipe(notify({ message: 'Images task complete' }))
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

// minifies SVG icons
const iconsTask = () =>
  src(['src/icons/**/*.svg'], { since: lastRun(iconsTask) })
    .pipe(newer('assets/icons'))
    .pipe(
      svgo((file) => {
        const prefix = path.basename(
          file.relative,
          path.extname(file.relative),
        );
        return {
          prefix,
          plugins: [
            // {
            //   prefixIds: {
            //     prefix: {
            //       toString() {
            //         this.counter = this.counter || 0;
            //         console.log(counter);
            //         return `id-${this.counter++}`;
            //       },
            //     },
            //   },
            // },
            {
              cleanupIDs: {
                remove: false,
                // prefix: {
                //   toString() {
                //     this.counter = this.counter || 0;
                //     console.log(counter);
                //     return `id-${this.counter++}`;
                //   },
                // },
                minify: false,
              },
            },
            { removeUnknownsAndDefaults: false },
            { removeTitle: false },
            { removeDesc: false },
            { removeHiddenElems: false },
            { removeEmptyContainers: false },
          ],
        };
      }),
    )
    .pipe(dest('assets/icons'))
    .pipe(notify({ message: 'Icons task complete' }))
    .pipe(
      sizereport({
        gzip: true,
      }),
    );
const svg2pngTask = () =>
  src('src/svg2png/**/*.svg')
    .pipe(
      svg2png({
        width: 1600,
      }),
    )
    .pipe(gulp.dest('assets/img/png'))
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

const cleanBuild = () =>
  del([
    'assets/**/*.js',
    'assets/**/*.css',
    'assets/**/*.map',
    'assets/**/*.json',
    'assets/img**/*',
    'assets/icons**/*',
  ]);

const cleanBuildDev = () => del(['assets/**/*.dev.*']);

const copyVendorLibsTask = () =>
  src('src/js/vendor/**/*').pipe(gulp.dest('assets/js/vendor'));

const lintTask = () =>
  src(scriptGlobs, { since: lastRun(lintTask) })
    .pipe(using())
    .pipe(eslint())
    .pipe(plumber())
    .pipe(eslint.format('stylish'))
    .pipe(eslint.failAfterError())
    .on(
      'error',
      notify.onError({
        title: 'JavaScript Linting Error',
        subtitle: false,
        sound: sounds.error,
        open: false,
        message: '<%= error.message.replace("Error: ","") %>',
        icon:
          'https://cdn2.iconfinder.com/data/icons/danger-problems/512/boom_explosion-512.png',
        timeout: 10,
      }),
    );

const jsTask = (glob) =>
  function javascriptTask() {
    return src(glob, { since: lastRun(jsTask) })
      .pipe(using())
      .pipe(sourcemaps.init())
      .pipe(
        rollup(
          {
            plugins: [
              babel({
                babelrc: false,
                exclude: 'node_modules/**',
                presets: [['@babel/preset-env', { modules: false }]],
              }),
              resolve({
                browser: true,
                preferBuiltins: false,
              }),
              commonjs({
                include: ['node_modules/**'],
                exclude: [],
                sourceMap: false,
              }),
            ],
          },
          {
            format: 'umd',
          },
        ),
      )
      .pipe(buffer())
      .pipe(rename({ suffix: '.dev' }))
      .pipe(gulp.dest('assets/js'))
      .pipe(
        sizereport({
          gzip: false,
        }),
      )
      .pipe(
        rename((filename) => {
          filename.basename = filename.basename.replace('.dev', '.min');
        }),
      )
      .pipe(
        terser({
          keep_fnames: false,
          mangle: {
            reserved: ['$'],
          },
        }),
      )
      .pipe(header('/*${date}*/\n', { date: new Date() }))
      .pipe(sourcemaps.write('./'))
      .pipe(gulp.dest('assets/js'))
      .pipe(
        sizereport({
          gzip: true,
        }),
      )
      .pipe(
        browserSync.stream({
          /* match: './assets/*.min.js' */
        }),
      )
      .pipe(
        notify({
          sound: sounds.success,
          title: 'Scripts task complete',
          message: '<%= file.relative %>',
          onLast: true,
          icon:
            'https://cdn0.iconfinder.com/data/icons/small-n-flat/24/678134-sign-check-512.png',
        }),
      );
  };

const manifestTask = () =>
  src([
    'assets/**/*.js',
    'assets/**/*.css',
    'assets/**/*.map',
    '!./assets/style.dev.css',
    '!./assets/**/*.dev.js',
    '!./assets/**/*.rev.*',
  ])
    // .pipe(using())
    .pipe(rev())
    .pipe(
      revFormat({
        suffix: '.rev',
        lastExt: false,
      }),
    )
    .pipe(dest('assets'))
    .pipe(
      rev.manifest({
        merge: true,
      }),
    )
    .pipe(
      revDel({
        dest: 'assets',
        suppress: true,
        deleteMapExtensions: true,
      }),
    )
    .pipe(dest('assets'))
    .pipe(
      sizereport({
        gzip: true,
      }),
    );

const removeManifestTask = () => del(['assets/rev-manifest.json']);

const watchAssetsTask = () => {
  gulp.watch(
    [
      'src/scss/**/*.scss',
      'src/plugins/**/scss/**/*.scss',
      '!src/scss/style-guide.scss',
      '!src/scss/admin.scss',
    ],
    series(styleLintTask, stylesTask),
  );
  gulp.watch(['src/scss/admin.scss'], series(styleLintTask, adminStyleTask));
  gulp.watch(
    ['src/scss/style-guide.scss'],
    series(styleLintTask, styleGuideTask),
  );
  gulp.watch(
    ['src/scss/editor-style.scss'],
    series(styleLintTask, editorStyleTask),
  );
  gulp.watch(bundlesGlob, series(lintTask, jsTask(bundlesGlob), bsreload));
  gulp.watch(
    masterOrImports,
    series(lintTask, jsTask(masterOnlyGlob), bsreload),
  );
  gulp.watch(vendorGlob, series(lintTask, jsTask(vendorOnlyGlob), bsreload));
  gulp.watch(pluginsGlob, series(lintTask, jsTask(masterOnlyGlob), bsreload));
  gulp.watch('src/icons/**/*.svg', series(iconsTask, bsreload));

  gulp.watch('src/img/**/*', series(imagesTask, bsreload));
  gulp
    .watch(['../../../**/*.php', '../../../**/*.html'])
    .on('change', bsreload);
};

const serveTask = (done) => {
  if (options.open || options.bs) {
    browserSync.init({
      watchEvents: ['change', 'add', 'unlink', 'addDir', 'unlinkDir'],
      open: false,
      ...(options.localDomain
        ? {
            host: options.localDomain,
            proxy: options.localDomain,
          }
        : {
            server: {
              baseDir: 'build',
              directory: false,
            },
          }),
      ghostMode: {
        scroll: true,
      },
      callbacks: {
        ready: function(err, bs) {
          console.log({ opening: options.open });
          if (options.open) {
            const port = bs.options.get('port');
            const devUrl = `http://${options.localDomain}:${port}`;
            opn(devUrl);
            console.log(`devUrl: ${devUrl}`);
          }
        },
      },
    });
  }
  done();
};

const defaultTask = series(
  cleanBuild,
  lintTask,
  parallel(
    imagesTask,
    iconsTask,
    stylesTask,
    jsTask(topLevelScriptGlob),
    adminStyleTask,
    styleGuideTask,
    editorStyleTask,
    copyVendorLibsTask,
  ),
);

const watchTask = series(defaultTask, parallel(watchAssetsTask, serveTask));

const buildTask = series(
  defaultTask,
  parallel(adminStyleTask, styleGuideTask, editorStyleTask),
  manifestTask,
); // creates and saves the manifest

exports.build = buildTask;
exports.serve = serveTask;
exports.phpcs = phpcsTask;
exports.removeManifest = removeManifestTask;
exports.styles = series(styleLintTask, stylesTask, manifestTask);
exports.styleGuide = styleGuideTask;
exports.images = imagesTask;
exports.icons = iconsTask;
exports.svg2png = svg2pngTask;
exports.clean = cleanBuild;
exports.styleLint = styleLintTask;
exports.editorStyle = editorStyleTask;
exports.adminStyle = adminStyleTask;
exports.copyVendorLibs = copyVendorLibsTask;
exports.js = exports.scripts = series(
  lintTask,
  jsTask(topLevelScriptGlob),
  manifestTask,
);
exports.lint = lintTask;
exports.manifest = manifestTask;
exports.cleanDev = cleanBuildDev;
exports.justwatch = parallel(watchAssetsTask, serveTask);
exports.watch = watchTask;
exports.default = defaultTask;
