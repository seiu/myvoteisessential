<?php
/**
 * Template Name: Style Demo Page
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

// some setup for special features on this page

// uncomment to show the grid helper by default
// add_filter( 'body_class', function( $classes ) {
//     return array_merge( $classes, array( 'show-grid' ) );
// } );
get_header(); ?>

<?php get_template_part( 'partials/toppers/topper', 'style-demo' ); ?>

<div id="content" class="site-main">
  <div class="container">
    <section id="headings" class="row justify-content-center">
      <header class="col-12">
        <h2 class="h3 style-demo-header">Headings & Text Treatments</h2>
      </header>
      <div class="col-md-8">
        <?php the_content(); ?>
      </div>
    </section>
  </div>
  <?php get_template_part( 'partials/modules' ); ?>
  <div class="container">
    <section id="custom" class="row">
      <header class="col-12">
        <div class="style-demo-header">
          <h2 class="h3">Custom Components</h2>
          <small>Add your custom components and their variants.</small>
          <small>Add a <code>.force-hover</code> or <code>.force-focus</code> class to any items that should show with their hover or focus states enabled.<sup data-title="Because ':hover' is a trusted event that cannot be simulated, it requires a special workaround to extract :hover styles from the stylesheet and create special classes for them. Only explicit states styles will be shown, not the browser-native states.">?</sup></small>
        </div>
      </header>
      <div class="col-md-6 component"></div>
      <div class="col-md-6 component"></div>
    </section>
  </div>
  <div class="container">
    <section id="links-and-buttons" class="row">
      <header class="col-12">
        <div class="style-demo-header">
          <h2 class="h3">Links & Buttons</h2>
          <small>Add a <code>.force-hover</code> or <code>.force-focus</code> class to any items that should show with their hover or focus states enabled.<sup data-title="Because ':hover' is a trusted event that cannot be simulated, it requires a special workaround to extract :hover styles from the stylesheet and create special classes for them. Only explicit states styles will be shown, not the browser-native states.">?</sup></small>
        </div>
      </header>
      <div class="col-md-6 component">
        <div class="style-demo-subheader">
          <h4>Links</h4>
          <small>&nbsp;</small>
        </div>
        <p><a href="#">Primary Link</a></p>
        <p><a href="#" class="force-hover">Primary Link, Hovered</a></p>
        <p><a href="#" class="force-hover force-focus">Primary Link, Hovered w/ Focus</a></p>
      </div>
      <div class="col-md-6 component inverse-link-colors">
        <div class="style-demo-subheader">
          <h4>Alt/Inverse Link Colors</h4>
          <small>Alternate styling (usually for use on different backgrounds)</small>
        </div>
        <p><a href="#">Primary Link</a></p>
        <p><a href="#" class="force-hover">Primary Link, Hovered</a></p>
        <p><a href="#" class="force-hover force-focus">Primary Link, Hovered w/ Focus</a></p>
      </div>
      <div class="col-md-6 component">
        <h4 class="style-demo-subheader">Buttons</h4>
        <p><button class="btn" role="button">Primary Button</button></p>
        <p><button class="btn force-hover" role="button">Primary Button, Hovered</button></p>
        <p><button class="btn force-hover force-focus" role="button">Primary Button, Hovered w/ Focus</button></p>
      </div>
      <div id="forms" class="col-12">
        <h4 class="style-demo-subheader">Form Elements</h4>
        <form>
          <div class="d-flex component row" data-type="Inputs">
            <div class="col-md"><input type="text" placeholder="Basic Input"></div>
            <div class="col-md"><input type="text" placeholder="Basic Input" value="Input w/ Text"></div>
            <div class="col-md"><input type="text" class="force-hover" placeholder="Basic Input, Hovered"></div>
            <div class="col-md"><input type="text" class="force-hover force-focus" placeholder="Basic Input, Focused"></div>
          </div>
          <div class="d-flex component row" data-type="Textareas">
            <div class="col-md"><textarea placeholder="Textarea"></textarea></div>
            <div class="col-md"><textarea placeholder="Textarea">TextArea w/ Text</textarea></div>
            <div class="col-md"><textarea class="force-hover force-focus" placeholder="Textarea">TextArea w/ Text</textarea></div>
          </div>
          <div class="d-flex component" data-type="Selects">
            <div class="col-md"><select><option>Yes</option><option>No</option></select></div>
            <div class="col-md"><select class="force-hover"><option>Yes (Hovered)</option><option>No</option></select></div>
            <div class="col-md"><select class="force-hover force-focus"><option>Yes (Focused)</option><option>No</option></select></div>
          </div>
          <div class="d-flex component" data-type="Radios & Checkboxes">
            <div class="col-md"><input name="foo" type="radio" title="default"></div>
            <div class="col-md"><input name="foo" type="radio" checked title="Checked"></div>
            <div class="col-md"><input name="foo" class="force-hover" type="radio" title="Hovered"></div>
            <div class="col-md"><input name="foo" class="force-hover force-focus" type="radio" title="Focused"></div>
            <div class="w-100"></div>
            <div class="col-md"><input name="bar" type="checkbox" title="Checked"></div>
            <div class="col-md"><input name="bar" type="checkbox" checked></div>
            <div class="col-md"><input name="bar2" class="force-hover" type="checkbox" title="Hovered"></div>
            <div class="col-md"><input name="bar3" class="force-hover force-focus" type="checkbox" title="Focused"></div>
          </div>
          <div class="d-flex component row" data-type="Labels & Field Combos">
            <div class="form-field email col">
            <label for="test-field">Label/Field Title Example</label>
            <input id="test-field" type="text" placeholder="Basic Input">
            </div>
          </div>
          <div class="d-flex component row" data-type="Submit Controls">
            <div class="col-md"><input value="Input Submit" type="submit"></div>
            <div class="col-md"><input class="force-hover" value="Input Submit Hovered" type="submit"></div>
            <div class="col-md"><input class="force-hover force-focus" value="Input Submit w/ Focus" type="submit"></div>
            <div class="w-100"></div>
            <div class="col-md"><button class="btn" type="submit">Button Submit</button></div>
            <div class="col-md"><button class="btn force-hover" type="submit">Button Submit, Hovered</button></div>
            <div class="col-md"><button class="btn force-hover force-focus" type="submit">Button Submit, w/ Focus</button></div>
          </div>
        </form>
      </div>
    </section>
</div><!-- .site-main -->
<div id="grid" class="show-grid grid-demo-section">
  <div class="container">
    <section class="row justify-content-center">
      <header class="col-12">
        <h2 class="h3 style-demo-header">Small Grid Columns</h2>
      </header>
      <?php
        $demo_columns = 12;
        foreach(range(1, $demo_columns) as $index) {
          ?><div class="col-md"><div class="demo-col-content"><?php echo $index; ?></div></div><?php
        }
      ?>
    </section ><!-- section.row -->
    <section class="site-main__content row justify-content-center">
      <?php
        $demo_columns = 4;
        foreach(range(1, $demo_columns) as $index) {
          ?><div class="col-md-3"><div class="demo-col-content"><?php echo $index; ?></div></div><?php
        }
      ?>
    </section ><!-- section.row -->
    <section class="site-main__content row justify-content-center">
      <?php
        $demo_columns = 3;
        foreach(range(1, $demo_columns) as $index) {
          ?><div class="col-md-4"><div class="demo-col-content"><?php echo $index; ?></div></div><?php
        }
      ?>
    </section ><!-- section.row -->
    <section class="site-main__content row justify-content-center">
      <?php
        $demo_columns = 2;
        foreach(range(1, $demo_columns) as $index) {
          ?><div class="col-md-6"><div class="demo-col-content"><?php echo $index; ?></div></div><?php
        }
      ?>
    </section ><!-- section.row -->
  </div>
</div>
<?php 
get_template_part( 'partials/grid-helpers' );
get_footer();
