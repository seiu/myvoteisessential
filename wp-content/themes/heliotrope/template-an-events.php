<?php /* Template Name: Action Network Events */ ?>
<?php get_header(); ?>
<main>
  <article <?php post_class(); ?>>
    <?php get_template_part( 'partials/toppers/topper', 'topper' ); ?>  
  </article>
  <?php if(function_exists('printANWidget')){ printANWidget(); }?>
  <div class="module buttons col-xs-12 col-md-8">
      <div class="wem-mobilize-back-btn">
        <a href="/" target="_blank"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 31 46" style="transform: rotate(180deg);"><path stroke="currentColor" fill="transparent" stroke-width="6" stroke-linecap="round" stroke-linejoin="round" d="M5.7 39.3l16.6-16.9L5.7 5.3"/></svg> Back to the home page</a>
      </div>
  </div>
</main>

<?php get_footer(); ?>
