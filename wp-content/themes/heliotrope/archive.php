<?php
/**
 * The default template for displaying any archive pages (like category archives, CPTs, etc.)
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package heliotrope
 */

get_header();

?>

<?php get_template_part( 'partials/toppers/topper', 'archive' ); ?>

<section <?php post_class(); ?>>
  <div class="container">
    <div id="content" class="row justify-content-center">
      <div class="col-md-10 col-lg-8">
        
      <?php if ( have_posts() ) : ?>

        <?php
        /* Start the Loop */
        while ( have_posts() ) :
          the_post();

          /*
           * Include the Post-Type-specific template for the content.
           * If you want to override this in a child theme, then include a file
           * called content-___.php (where ___ is the Post Type name) and that will be used instead.
           */
          get_template_part( 'partials/content', get_post_type() );

        endwhile;

        the_posts_navigation();

      else :

        get_template_part( 'partials/content', 'none' );

      endif;
      ?>
      </div>
    </div>
  </div>
</section>
<?php
get_footer();
