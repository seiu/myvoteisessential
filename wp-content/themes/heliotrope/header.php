<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package heliotrope
 */

$alert_is_cookied  = get_theme_mod( 'alert_is_cookied', false );
$alert_is_enabled = 'yes' == get_theme_mod( 'enable_alert_bar', 'no' );
$alert_class = !$alert_is_cookied && $alert_is_enabled ? ' alert__no-cookie ok-to-alert' : '';
?>
<!doctype html>
<html class="no-js <?php echo $alert_class?>"
<?php
  if ( function_exists( 'language_attributes' ) ) :
    echo esc_attr( language_attributes() );
  ?>
  <?php else : ?>
  lang="en-US"
  <?php endif; ?>
>
<head>
	<meta charset="utf-8"><meta name="google" value="notranslate">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<link rel="profile" href="https://gmpg.org/xfn/11">
	<?php wp_head(); ?>
	<?php get_template_part( 'partials/splash-redirect' ); ?>
</head>

<body <?php body_class(); ?>><?php wp_body_open(); ?>
<div id="page" class="site-wrap">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'heliotrope' ); ?></a>
	<?php get_template_part( 'partials/header/navigation' ); ?>
	<main class="site-main">
