<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package heliotrope
 */

get_header();
?>

<article <?php post_class( 'single' ); ?>>
  <?php get_template_part( 'partials/toppers/topper', 'single' ); ?>

  <div id="content" class="container body-content">
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8">
        <?php
        while ( have_posts() ) :
          the_post();

          the_content( sprintf(
            wp_kses(
              /* translators: %s: Name of current post. Only visible to screen readers */
              __( 'Continue reading<span class="screen-reader-text"> "%s"</span>', 'heliotrope' ),
              array(
                'span' => array(
                  'class' => array(),
                ),
              )
            ),
            get_the_title()
          ) );

          wp_link_pages( array(
            'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'heliotrope' ),
            'after'  => '</div>',
          ) );

        endwhile; // End of the loop.
        ?>
      </div><!-- article.col -->
    </div><!-- section.row -->
  </div>
  <?php get_template_part( '/partials/next-prev-links' ); ?>
</article><!-- .site-main -->

<?php
get_footer();
