<?php

  // allows us to force real "hover" states on things without extra hax
  wp_enqueue_script(
    'pseudostyler_js',
    get_template_directory_uri() . '/assets/js/vendor/pseudostyler.js',
    array(),
    'v1',
    true
  );
  $force_hover_js = <<<EOD
  (async () => {
    let styler = new PseudoStyler();
    await styler.loadDocumentStyles();
    Array.from(document.querySelectorAll('.force-hover')).forEach(el => {
      styler.toggleStyle(el, ':hover');
    });
    Array.from(document.querySelectorAll('.force-focus')).forEach(el => {
      styler.toggleStyle(el, ':focus');
    });
  })();
  EOD;
  wp_add_inline_script( 'pseudostyler_js', $force_hover_js );

  // main styles
  wp_enqueue_style(
    'style-guide_css',
    we_asset_path('css/style-guide.min.css'),
    false,
    'v1'
  );  

  add_action('wp_footer', 'we_add_grid_toggle'); 
  function we_add_grid_toggle() { 
    echo <<<EOD
      <button id="grid-toggle" title="Toggle Grid On/Off" class="js-toggle-grid" role="button">Toggle Grid Guides </button>
      <script>
        document.getElementById('grid-toggle').addEventListener('click', (e) => {
          const is_grid_on = document.body.classList.contains('show-grid');
          document.body.classList[ is_grid_on ? 'remove': 'add' ]('show-grid');
        });
      </script>
    EOD; 
  }