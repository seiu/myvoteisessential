<?php
/**
 * Password Form
 *
 * Password form for password-protected pages
 *
 * @package Purple
 */

?>
<br><br><br><br>
  <div class="container">
    <section id="headings" class="row justify-content-center">
      <div class="col-12">
        <form
          action="<?php echo esc_url( site_url('/wp-login.php?action=postpass', 'login_post' ) ); ?>"
          class="post-password-form"
          method="post"
          style="text-align:center"
        >
          <h4><?php esc_html_e( 'This post is password protected. To view it please enter your password below:', 'heliotrope' ); ?></h4><br><br>
        	<label for="pwbox-197">
        		<?php esc_html_e( 'Password:', 'wectheme' ); ?>
        	  <br /><br>
        	  <input name="post_password" id="pwbox-197" size="20" type="password" />
        	</label>
        	<br />
        	<input name="Submit" value="Submit" type="submit" />
          </p>
        </form><!-- .post-password-form -->
    </div>
  </section>
</div>