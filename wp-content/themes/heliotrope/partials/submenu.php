<?php
/* 
 * Page submenus, usually used in toppers 
 * uses the meta val "submenu_type" to determine the type, if any
*/

$menu_type = get_post_meta( get_the_ID(), 'submenu_type', true );

if( $menu_type && "none" !== $menu_type ){
  ?>
  <section class="submenu">
    <div class="container">
      <div class="row justify-content-center">
          <?php
          if ( "child" === $menu_type ) {
            ?>
            <nav class="col-md-10 col-lg-8 nav-list">
              <?php echo heliotrope_list_child_pages(); ?>
            </nav>
          <?php
          }
          else if ( "custom" === $menu_type ) {
            $menu_id = get_post_meta( get_the_ID(), 'custom_menu_id', true );
            wp_nav_menu(
              array(
                'theme_location'  => 'secondary_navigation flex-list',
                'container'       => 'nav',
                'container_class' => 'col-md-10 col-lg-8 nav-list',
                'menu'            => esc_attr( $menu_id ),
                'before'          => '',
                'depth'          => '1',
                'menu_class'      => 'row menu flex-list',
                'add_li_class'    => 'col-auto',
                'walker'          => new We_Accessible_Walker(),
              )
            );
          }
          ?>
        </nav>
      </div>
    </div>
  </section>
  <?php
}
