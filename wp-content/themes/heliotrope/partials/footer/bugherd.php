<?php
$bugherd_key = get_theme_mod( 'bugherd_key' );
$host = isset( $_SERVER['HTTP_HOST'] ) ? esc_url_raw( wp_unslash( $_SERVER['HTTP_HOST'] ) ) : '';
if ( $host
  && $bugherd_key
  && ( false !== strpos( $host, 'ideeyeclient' ) || false !== strpos( $host, 'local' ) )
  && ( '1w7sloltbyfmu99xygy7gw' != $bugherd_key
  || false !== strpos( $host, 'tarter' ) )
) :
  ?>
  <script type='text/javascript'>
  (function (d, t) {
  var bh = d.createElement(t), s = d.getElementsByTagName(t)[0];
  bh.type = 'text/javascript';
  bh.async = true;
  bh.defer = true;
  bh.src = 'https://www.bugherd.com/sidebarv2.js?apikey=<?php echo esc_attr( $bugherd_key ); ?>';
  s.parentNode.insertBefore(bh, s);
  })(document, 'script')
  </script>
<?php endif; ?>