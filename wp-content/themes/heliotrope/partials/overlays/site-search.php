<?php
/**
 * Search Form
 *
 * @package Purple
 */

?>
<div
  class="search-overlay"
  role="dialog"
  aria-modal="true"
  aria-hidden="true"
  aria-labelledby="dialog1Title"
>
  <div class="screen-reader-text">
	 <h2 id="dialog1Title"><?php esc_attr_e( 'Search Form Overlay', 'heliotrope' ); ?></h2>
  </div>
  <?php get_search_form(); ?>

  <button
	type="button"
	id="btn-search-close"
	class="btn--search-close js-close-site-search"
	aria-label="<?php esc_attr_e( 'Close search form', 'heliotrope' ); ?>"
  >
	 <?php we_inline_svg( 'assets/icons/close-btn-rounded.svg' ); ?>
  </button>
</div><!-- .search -->
