<?php
/**
 * Mobile Menu overlay
 *
 * @package Purple
 */

?>
<div class="mobile-overlay mobile-nav-overlay" role="dialog" aria-modal="true" aria-hidden="true" aria-labelledby="dialog2Title">
	<div class="screen-reader-text">
		<h2 id="dialog2Title"><?php esc_html_e( 'Mobile Menu Overlay', 'heliotrope' ); ?></h2>
	</div>
	<div class="mobile-overlay__inner">
		<div class="mobile-nav-menus">
			<?php if (has_nav_menu('mobile_navigation')) : ?>
			<nav class="primary-mobile-menu header-copy" aria-label="Mobile Navigation" role="navigation">
				<?php
				wp_nav_menu(
					array(
						'theme_location' => 'mobile_navigation',
						'container'      => 'primary-navigation',
						'depth'          => '1',
						'before'         => '',
						'menu_id'        => 'primary-navigation-overlay',
						'menu_class'     => 'menu primary-menu',
						'walker'         => new We_Accessible_Walker(),
					)
				);
				?>
				</nav><!-- .main-mobile-menu -->
			<?php endif; ?>
        
			<div class="mobile-social">
				<ul class="shares-slider__list">
					<span class="small-text">Share</span>
					<?php get_template_part( 'partials/social/page', 'shares' ); ?>
				</ul><!-- .shares-slider__list -->
			</div><!-- .mobile-social -->

			<?php if (has_nav_menu('secondary_navigation')) : ?>
			<nav class="secondary-mobile-menu" aria-label="Secondary Mobile Navigation" role="navigation">
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'secondary_navigation',
					'container'      => 'secondary-navigation',
					'depth'          => '1',
					'menu_id'        => 'secondary-navigation-overlay',
					'menu_class'     => 'menu secondary-menu',
					'before'         => '',
					'navlink_class'  => 'btn',
					'walker'         => new We_Accessible_Walker(),
				)
			);
			?>
			</nav><!-- .secondary -->
			<?php endif; ?>
		</div>
	</div><!-- .main-mobile-menu-inner -->
</div><!-- .menuOverlay -->
