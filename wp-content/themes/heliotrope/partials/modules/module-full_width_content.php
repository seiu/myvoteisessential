<?php
/**
 * Full Width Content Module
 *
 * @package heliotrope
 */

?>

<div
  data-module
  class="module full-content"
  <?php we_print_section_meta(); ?>
>
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-xs-12 col-md-8 col-lg-6 wysiwyg-text">
        <?php echo apply_filters('acf_the_content', wp_kses_post( get_sub_field( 'content' , false, false) ) ); ?>
      </div><!-- .col-xs-12 -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .module.full-content -->
