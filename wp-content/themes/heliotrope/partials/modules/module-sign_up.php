<?php
/**
 * Sign Up Module
 *
 * @package heliotrope
 */

?>

<div
	data-module
	class="module light signup social-hide"
	<?php we_print_section_meta(); ?>
	>
  <div class="container">
    <div class="row justify-content-md-center">
      <div class="col-xs-12 col-md-8 col-lg-6">
				<div class="inner-form">
					<?php echo we_render_crm( get_the_ID(), 'inner', false ); ?>
				</div><!-- .inner-form -->
			</div><!-- .col-xs-12 .col-md-8 .col-lg-6 -->
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .module.signup -->

<?php
if (
'ngp-van' == get_sub_field( 'default_integration', get_the_ID() )
) :
?>
<script type="text/javascript" src="https://d1aqhv4sn5kxtx.cloudfront.net/actiontag/at.js"></script>
<?php endif; ?>

<script type="text/javascript">
  window.ngpFormData = {
    FirstName: {
      label: '<?php echo wp_kses( get_theme_mod( 'first_name_label', __('First Name', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'first_name_placeholder', __('e.g. Jane', 'heliotrope') ), array() ); ?>',
    },
    LastName: {
      label: '<?php echo wp_kses( get_theme_mod( 'last_name_label', __('Last Name', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'last_name_placeholder', __('e.g. Doe', 'heliotrope') ), array() ); ?>',
    },
    PostalCode: {
      label: '<?php echo wp_kses( get_theme_mod( 'zip_code_label', __('Zip Code', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'zip_code_placeholder', __('#####-####', 'heliotrope') ), array() ); ?>',
    },
    EmailAddress: {
      label: '<?php echo wp_kses( get_theme_mod( 'email_label', __('Email Address', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'email_placeholder', __('username@service.domain', 'heliotrope') ), array() ); ?>',
    },
    MobilePhone: {
      label: '<?php echo wp_kses( get_theme_mod( 'phone_label', __('Phone Number', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'phone_placeholder', __('###-###-####', 'heliotrope') ), array() ); ?>',
    },
};
<?php include(get_template_directory() . '/assets/js/bundle-ngp-labels.min.js'); ?>
</script>