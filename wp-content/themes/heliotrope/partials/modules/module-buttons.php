<?php
/**
 * Buttons Module
 *
 * @package heliotrope
 */

?>

<div
	data-module
	class="module buttons"
	<?php we_print_section_meta(); ?>
>
	<div class="container">
		<div class="row justify-content-md-center">
			<div class="col-xs-12 col-md-8 col-lg-6">
				<div class="row justify-content-center buttons__row">
					<?php if ( have_rows( 'add_buttons' ) ) : ?>
						<?php
						while ( have_rows( 'add_buttons' ) ) :
							the_row();
							$link = get_sub_field( 'button_link' );
							?>
							<div class="col-md-auto">
								<a
								class="btn btn--tertiary"
								href="<?php echo esc_url( $link['url'] ); ?>"
								<?php if ( $link['target'] ) : ?>
										target="_blank"
								<?php endif; ?>
									>
								<?php echo esc_html( $link['title'] ); ?>
								</a>
							</div>
						<?php endwhile; ?>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</div>
</div>
