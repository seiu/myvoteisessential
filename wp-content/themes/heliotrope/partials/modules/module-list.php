<?php
/**
 * List Module
 *
 * @package heliotrope
 */

?>

<div
  data-module
  class="module list"
  <?php we_print_section_meta(); ?>
>
  <div class="container">
    <div class="row justify-content-md-center list__intro">
      <div class="col-xs-12 col-md-8 col-lg-6">
    <?php if ( get_sub_field( 'headline' ) ) : ?>
        <h2 class="h3 module-headline"><?php echo wp_kses_post( get_sub_field( 'headline' ) ); ?></h2>
    <?php endif; ?><!-- .description -->

    <?php if ( get_sub_field( 'description' ) ) : ?>
        <div class="description">
        <p><?php echo wp_kses_post( get_sub_field( 'description' ) ); ?></p>
        </div><!-- .description -->
    <?php endif; ?>

      </div><!-- .col-xs-12 -->
    </div><!-- .row -->

    <div class="row justify-content-md-center">
      <div class="col-xs-12 col-md-8 col-lg-6">
          <?php
          $list_item = 1;
          while ( have_rows( 'list_items' ) ) :
            the_row();
            ?>
            <div class="list__item">
              <div class="list__item_counter"><h2 class="alt"><?php echo wp_kses_post( $list_item );?></h2></div>
              <div class="list__item_description wysiwyg-text">
                <?php echo wp_kses_post( get_sub_field( 'description', false ) );?>
              </div>
            </div>
          <?php $list_item++; endwhile; ?>
      </div><!-- .col-xs-12 -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .module.list-page -->
