<?php
/**
 * Search Form Container
 *
 * @package Purple
 */

?>

<div class="top-search" aria-hidden="true">
  <div class="container">
  	<?php get_search_form(); ?>
  </div>
</div>
