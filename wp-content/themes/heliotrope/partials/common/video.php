<figure class="html5-video">
  <video loop muted playsinline preload="metadata" class="unplayed" aria-hidden="true"
    <?php if ( get_sub_field( 'video_poster_image' ) ) : ?>
      data-poster="<?php echo esc_url( get_sub_field( 'video_poster_image' ) ); ?>"
    <?php endif; ?>
  >
    <source data-src="<?php echo esc_url( get_sub_field( 'video_source_webm' ) ); ?>" type="video/webm">
    <source data-src="<?php echo esc_url( get_sub_field( 'video_source_mp4' ) ); ?>" type="video/mp4">
    <source data-src="<?php echo esc_url( get_sub_field( 'video_source_ogv' ) ); ?>" type="video/ogg">
  </video>
  <noscript>
    <video loop muted playsinline preload="auto" aria-hidden="true"
      <?php if ( get_sub_field( 'video_poster_image' ) ) : ?>
        poster="<?php echo esc_url( get_sub_field( 'video_poster_image' ) ); ?>"
      <?php endif; ?>
    >
    <source src="<?php echo esc_url( get_sub_field( 'video_source_webm' ) ); ?>" type="video/webm">
    <source src="<?php echo esc_url( get_sub_field( 'video_source_mp4' ) ); ?>" type="video/mp4">
    <source src="<?php echo esc_url( get_sub_field( 'video_source_ogv' ) ); ?>" type="video/ogg">
    </video>
  </noscript>
  <?php if ( get_sub_field( 'video_caption' ) ) : ?>
    <figcaption><?php echo wp_kses_post( get_sub_field( 'video_caption' ) ); ?></figcaption>
  <?php endif; ?>
</figure>