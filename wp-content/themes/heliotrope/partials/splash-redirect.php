<?php
/**
 * Include the script to redirect the splash page, if necessary
 *
 * @package Purple
 */

if (
  'yes' == get_theme_mod( 'enable_splash_page' )
) :
	if ( isset( $_SERVER['REQUEST_URI'] ) && '' !== $_SERVER['REQUEST_URI'] ) {
		$page_path = esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) );
	} else {
		$page_path = esc_url( get_home_url() );
	}

	$redirect_url = get_theme_mod( 'splash_page_url' ) ?
		esc_url_raw( get_theme_mod( 'splash_page_url' ) ) :
		false;
	$host = false;
	if ( ! empty( esc_url_raw( wp_unslash( $_SERVER['HTTP_HOST'] ) ) ) ) {
		$host = esc_url_raw( wp_unslash( $_SERVER['HTTP_HOST'] ) );
	}
	$this_domain = parse_url( wp_unslash( $host ) );
	$redirect_domain = parse_url( wp_unslash( $redirect_url ) );
	$is_not_cookied = 'no' == get_theme_mod( 'is_splash_cookied' );

	$splash_options = array(
		'cookiename'       => esc_attr( get_theme_mod( 'splash_cookie_name', 'splash_redirect' ) ),
		'redirect_url'     => $redirect_url,
		'no_cookie'        => $is_not_cookied,
		'duration'         => absint( get_theme_mod( 'cookie_length', 7 ) ),
		'is_admin'         => is_admin() || 'wp-login.php' == $GLOBALS['pagenow'],
		'page_path'        => esc_url( $page_path ),
		'is_front_page'    => is_front_page(),
		'only_home'        => 'yes' == get_theme_mod( 'only_redirect_home', 'yes' ),
		'only_direct_home' => 'yes' == get_theme_mod( 'only_direct_home', 'yes' ),
	);

	?><script data-redirect="<?php echo esc_url( $redirect_url ); ?>" data-page-path="<?php echo esc_url( $page_path ); ?>">var SPLASH_OPTIONS = <?php echo wp_json_encode( $splash_options ); ?>;
	<?php
	  ob_start();
    include get_template_directory() . '/assets/js/bundle-splash-redirect.min.js';
    $file_contents = ob_get_clean();
		echo $file_contents;
		?></script>
<?php endif; ?>
