<?php
  $topper_size = we_get_topper_size();
  if ( $topper_size != 'short' && $topper_size != 'simple' ) :
    if (
      (
        'image' == get_field( 'topper_type' ) &&
        ! is_home() &&
        ! is_category()
      ) ||
      (
        'image' == get_field( 'topper_type', get_option( 'page_for_posts' ) ) &&
        ( is_home() || is_category() )
      )
    ) :
    ?>
    <?php if ( get_field( 'topper_image' ) ) : ?>
      <div class="overlay"></div>
    <?php endif; ?>
    <?php elseif ( 'video' == get_field( 'topper_type' ) ) : ?>
      <div
        class="topper-video"
        style="background: url(<?php echo esc_url( get_field( 'topper_video_image' ) ); ?>) no-repeat center center;"
      >
        <video
          loop
          muted
          playsinline
          preload="metadata"
          poster="<?php echo esc_url( get_field( 'topper_video_image' ) ); ?>"
          class="fullscreen-bg__video unplayed"
          aria-hidden="true"
        >
          <source src="<?php echo esc_url( get_field( 'topper_video_webm' ) ); ?>" type="video/webm">
          <source src="<?php echo esc_url( get_field( 'topper_video_mp4' ) ); ?>" type="video/mp4">
          <source src="<?php echo esc_url( get_field( 'topper_video_ogv' ) ); ?>" type="video/ogg">
        </video>
        <noscript>
          <video
            loop
            muted
            playsinline
            preload="auto"
            poster="<?php echo esc_url( get_field( 'topper_video_image' ) ); ?>"
            class="fullscreen-bg__video"
            aria-hidden="true"
          >
            <source src="<?php echo esc_url( get_field( 'topper_video_webm' ) ); ?>" type="video/webm">
            <source src="<?php echo esc_url( get_field( 'topper_video_mp4' ) ); ?>" type="video/mp4">
            <source src="<?php echo esc_url( get_field( 'topper_video_ogv' ) ); ?>" type="video/ogg">
          </video>
        </noscript>
      </div>
    <?php endif;
  endif;
?>