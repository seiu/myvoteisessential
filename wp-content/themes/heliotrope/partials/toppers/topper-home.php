<?php
  $topper_size = we_get_topper_size();
  $banner_type = we_get_topper_type();
  ?>

<div class="topper topper-home <?php echo $topper_size; ?> home social-hide">

  <div class="container">
    <div class="column">
      <div class="topper-cta row">
        <div class="col-xs-12 col-md-7">
        <?php if ( get_field( 'large_text' ) ) : ?>
          <h1 class="super">
            <?php echo wp_kses_post( get_field( 'large_text' ) ); ?>
          </h1><!-- h1.super -->
        <?php else : ?>
          <h1 class="super">
            <?php the_title(); ?>
          </h1><!-- h1.super -->
        <?php endif; ?>
        </div>
  
        <div class="col-xs-12 col-md-5">
        <?php if ( get_field( 'deck' ) ) : ?>
          <h2 class="h4">
            <?php echo wp_kses_post( get_field( 'deck', false, false ) ); ?>
          </h2><!-- h2 -->
        <?php endif; ?>
        <?php if ( get_field( 'deck_small_text' ) ) : ?>
          <span class="small-text">
            <?php echo wp_kses_post( get_field( 'deck_small_text', false, false ) ); ?>
          </span><!-- span -->
        <?php endif; ?>
        <?php
          if (
            is_front_page() &&
            ! get_field( 'on_social_icons_header', get_option( 'page_on_front' ) )
          ) :
          ?>
          <div class="topper-social">
            <ul class="shares-slider__list">
              <span class="small-text"><?php esc_html_e( 'Share', 'heliotrope' ); ?></span>
              <?php get_template_part( 'partials/social/page', 'shares' ); ?>
            </ul><!-- .shares-slider__list -->
          </div><!-- .topper-social -->
        <?php endif; ?>
        </div>
      </div>
    </div><!-- .column -->
  </div><!-- .container -->

  <div class="compound-image">
    <div class="image" <?php echo $banner_type; ?>></div>
  </div>

  <?php get_template_part( 'partials/toppers/shared/s', 'image-video' ); ?>
  
</div><!-- .topper -->