<?php 
  $page_id = we_get_page_id();
  $topper_small_text = get_post_meta( $page_id, 'small_text', true );
  $topper_large_text = get_post_meta( $page_id, 'large_text', true );
  $topper_deck = apply_filters( 'acf_the_content', get_post_meta( $page_id, 'deck', true ) );
  if( ! $topper_large_text ){
    $topper_large_text = is_archive() ?
      get_the_archive_title() :
      get_the_title( $page_id );
  }
  if( ! $topper_deck && is_archive() ){
    $topper_deck = get_the_archive_description();
  }
  $topper_size = get_post_meta( $page_id, 'topper_size', true );
?>

<header class="topper topper-<?php echo esc_attr( $topper_size ? $topper_size : 'simple' ); ?>">
  <div class="container">
    <div class="column">
      <div class="row justify-content-center">
        <div class="col-md-10 col-lg-7">
          <h1 class="super page-title topper__title">
            <?php echo wp_strip_all_tags( $topper_large_text ); ?>
          </h1>
          <?php if ( $topper_small_text ) : ?>
            <h2 class="topper__subtitle h3"><?php echo wp_kses_post( $topper_small_text ); ?></h2>
          <?php endif; ?>
          <?php if ( $topper_deck ) : ?>
            <div class="topper__deck"><span class="topper__subtitle h4"><?php echo wp_kses( $topper_deck, array( 'span' => true, 'strong' => true, 'em' => true, 'a'=>array('href'=>true, 'target'=>true, 'title'=>true) ) ); ?></span></div>
          <?php endif; ?>
          <?php
          if ( 'post' === get_post_type() && is_single() ) :
            ?>
            <div class="topper__entry-meta">
            <?php
              heliotrope_entry_categories();
              heliotrope_post_date();
              ?>
            </div>
            <div class="topper__byline"><?php heliotrope_posted_by(); ?></div>
            <div class="topper__entry-meta topper__entry-tags"><?php heliotrope_entry_tags() ?></div>
          <?php endif; ?>
        </div>
      </div>
    </div>
  </div>
</header>