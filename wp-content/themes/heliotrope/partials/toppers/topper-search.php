<header class="topper topper-simple container">
  <div class="container">
    <div class="row justify-content-center">
      <div class="col-md-10 col-lg-8 column">
        <h1 class="page-title h2">
          <?php
          /* translators: %s: search query. */
          printf( esc_html__( 'Search Results for: %s', 'heliotrope' ), '<span>' . get_search_query() . '</span>' );
          ?>
        </h1>
      </div>
    </div>
  </div>
</header><!-- .page-header -->