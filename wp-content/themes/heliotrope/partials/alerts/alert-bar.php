<?php
/**
 * Alert Bar
 *
 * Display an alert bar at the top of the site
 *
 * @package Purple
 */
  $is_wp_preview = isset( $GLOBALS ) && array_key_exists('wp_customize', $GLOBALS) && isset( $GLOBALS['wp_customize'] );
  $alert_is_cookied  = ! $is_wp_preview && "yes" === get_theme_mod( 'alert_is_cookied', "yes" ) && ! defined('DISALLOW_COOKIES');
  $alert_type        = get_theme_mod( 'alert_type', 'simple' );
  $alert_position    = get_theme_mod( 'alert_position', 'top' );
  $is_simple_alert   = 'simple' == $alert_type;
  $is_button_alert   = 'button' == $alert_type;
  $cookievalue       = isset( $cookievalue ) && $cookievalue ? $cookievalue : 'value';
  $alert_headline    = get_theme_mod( 'alert_headline' );
  $alert_url         = get_theme_mod( 'alert_link_url' );
  $alert_title       = get_theme_mod( 'alert_link_title') ?
    get_theme_mod( 'alert_link_title') :
    "Link to {$alert_url}";
  $suppression_regex = get_theme_mod( 'alert_suppression_regex', false );
  $suppression_regex = $suppression_regex ? trim( $suppression_regex, '|' ) : false;
  
  $alert_bar_classes = '';
  if ( $is_simple_alert ) {
    $alert_bar_classes .= 'alert-bar--is-simple ';
    if ( $alert_url ) {
      $alert_bar_classes .= 'has-link ';
    }
  } else {
    $alert_bar_classes .= 'alert-bar--is-not-simple ';
  }
  ?>

<div <?php echo $is_wp_preview ? 'data-preview':'data-no-preview'; ?> data-js-social-hide aria-live="assertive" role="alert" class="alert-bar alert-bar__<?php echo $alert_position; ?> social-hide <?php echo esc_attr( $alert_bar_classes ); ?> <?php echo $alert_is_cookied ? 'is--initial' : 'ready-to-alert'; ?>" data-modifier="<?php echo $alert_position; ?>" data-is-cookied="<?php echo esc_attr( wp_json_encode( $alert_is_cookied ) ); ?>" data-cookievalue="<?php echo esc_attr( $cookievalue ); ?>" data-linksto="<?php echo esc_attr( $alert_url ); ?>">
  <div class="container-fluid">
    <div class="alert-bar__simple">
      <p><?php echo wp_kses( $alert_headline, array('em' => array(),'strong' => array()) ); ?></p>
    </div>
  </div>
  <?php if ( $alert_url ) : ?>
  <a class="alert-bar__simple__link" href="<?php echo esc_url( $alert_url ); ?>"
  <?php if ( strpos($alert_url, '://' ) !==false ){ echo 'target="_blank" rel="nofollow noopener"'; } ?>
  ><span class="screen-reader-text"><?php echo esc_attr( $alert_title ); ?></span>
  </a>
  <?php endif; ?>
  <button
  aria-label="Close alert"
  class="alert-bar__close 
  <?php
  if ( 'simple' != $alert_type ) {
    echo 'is-top-aligned'; }
  ?>
  "
  >
  <?php we_inline_svg( 'assets/icons/close-btn.svg' ); ?>
  </button>
</div>
<script>
  var ALERT_OPTIONS = 
  <?php
    if ( isset( $_SERVER['REQUEST_URI'] ) && '' !== $_SERVER['REQUEST_URI'] ) {
    $page_path = esc_url_raw( wp_unslash( $_SERVER['REQUEST_URI'] ) );
    } else {
        $page_path = esc_url( get_home_url() );
    }
  echo wp_json_encode(
    array(
      'alert_is_cookied'  => $alert_is_cookied,
      'suppression_regex' => $suppression_regex, // DT.
      'once_per_session'  => ! $is_wp_preview && boolval( 'yes' == get_theme_mod( 'alert_only_once_per_session', 'no' ) ),
      'cookievalue'       => $cookievalue,
      'page_path'         => $page_path,
      'links_to'          => $alert_url,
    )
  );
  ?>
  ;
  <?php echo file_get_contents( get_template_directory() . '/assets/js/bundle-alert.min.js' ); ?>
  setAlert( ALERT_OPTIONS, '<?php echo $alert_position; ?>' );
</script>
</div>