<?php
/**
 * Display all of the modules
 *
 * @package heliotrope
 */

if ( have_rows( 'modules' ) ) :
	while ( have_rows( 'modules' ) ) :
		the_row();
		get_template_part( 'partials/modules/module', get_row_layout() );
  endwhile;
endif;
