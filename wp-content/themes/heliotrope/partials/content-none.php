<?php
/**
 * Template part for displaying a message that posts cannot be found
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package heliotrope
 */

?>

<section <?php post_class( 'no-results not-found' ); ?>>
  <div class="page-content">
    <?php
    if ( is_search() ) :
      ?>

      <p><?php esc_html_e( 'Sorry, but nothing matched your search terms. Please try again with some different keywords.', 'heliotrope' ); ?></p>
      <?php
      get_search_form();

    else :
      ?>

      <p><?php esc_html_e( 'It seems we can&rsquo;t find what you&rsquo;re looking for. Perhaps searching can help.', 'heliotrope' ); ?></p>
      <?php
      get_search_form();

    endif;
    ?>
  </div><!-- .page-content -->
</section><!-- .no-results -->
