<div class="container">
  <div id="pledge" class="home-introduction row">
    <div class="col-xs-12 col-md-7">
    <?php if ( get_field( 'introduction_headline', we_get_page_id( '4' ) ) ) : ?>
      <h2>
        <?php echo wp_kses_post( get_field( 'introduction_headline', we_get_page_id( '4' ) ) ); ?>
      </h2>
    <?php endif; ?>
    </div><!-- introduction headline -->

    <div class="col-xs-12 col-md-5">
    <?php
      $cta_type = get_field( 'default_integration', we_get_page_id( '4' ) );
      if ( $cta_type ) :
        include( get_template_directory() . '/partials/home/home-crm.php' );
      endif;
      ?>
    </div><!-- introduction crm -->
  </div><!-- .home-introduction -->
</div><!-- .container -->