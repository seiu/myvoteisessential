<div class="home-social-share-downloads">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-7">
      <?php if ( get_field( 'social_share_downloads_headline' ) ) : ?>
        <h1>
          <?php echo wp_kses_post( get_field( 'social_share_downloads_headline' ) ); ?>
        </h1>
      <?php endif; ?>
      </div><!-- social-share headline -->

      <div class="col-xs-12 col-md-5">
      <?php if ( get_field( 'social_share_downloads_description' ) ) : ?>
        <?php echo wp_kses_post( get_field( 'social_share_downloads_description' ) ); ?>
      <?php endif; ?>
      </div><!-- social-share description -->
    </div><!-- .row -->

    <?php if ( have_rows( 'social_images' ) ) : ?>
    <?php $r = 0; ?>
    <div class="row row-grid">
      <div class="grid">
      <?php while ( have_rows( 'social_images' ) ) : the_row(); ?>
        <div class="grid-item">
        <?php
          $resourceImage = get_sub_field( 'social_image' );
          $backgroundThumb = $resourceImage['sizes'][ 'medium_square' ];
          $downloadImageUrl = get_sub_field( 'social_image_file' );
          $facebookShareUrl = get_sub_field( 'social_share_downloads_facebook', false, false );
          $twitteShareUrl = get_sub_field( 'social_share_downloads_twitter', false, false );
          ?>
        <?php if ( $resourceImage ) : ?> 
          <div class="grid-image-container">
            <div class="grid-image" style="background-image: url('<?php echo $backgroundThumb; ?>');"></div><!-- .grid-image -->
            <div class="overlay"></div> <!-- .overlay -->
            <div class="overlay bottom"></div> <!-- .overlay.bottom -->
          </div><!-- .grid-image-container -->
        <?php endif; ?> 

          <a href="<?php echo $backgroundThumb; ?>" rel="lightbox-gallery-r-<?php echo '' . $r++; ?>" class="resource-magnifier overlay-link" title="Caption. Can be aligned to any side and contain any HTML.">
              <?php we_inline_svg( 'assets/icons/magnifier.svg' ); ?>
          </a>

          <ul class="shares-slider__list">
            <li class="social-sharing-item facebook">
              <a
                href="http://www.facebook.com/sharer.php?u=<?php echo urlencode( $facebookShareUrl ); ?>"
                class="social-pop"
              >
                <span class="screen-reader-text"><?php esc_html_e( 'Share this image on Facebook', 'heliotrope' ); ?></span>
                <span aria-hidden="true"><?php we_inline_svg( 'assets/icons/facebook.svg' ); ?></span>
              </a>
            </li><!-- .facebook -->
            <li class="social-sharing-item twitter">
              <a
                href="http://twitter.com/intent/tweet?text=<?php echo urlencode( $twitteShareUrl ); ?>
                  <?php
                  if ( get_theme_mod( 'twitter_handle', false ) ) {
                    echo '&via=' . get_theme_mod( 'twitter_handle', false ); }
                  ?>
                "
                class="social-pop"
              >
                <span class="screen-reader-text"><?php esc_html_e( 'Share this image on Twitter', 'heliotrope' ); ?></span>
                <span aria-hidden="true"><?php we_inline_svg( 'assets/icons/twitter.svg' ); ?></span>
              </a>
            </li><!-- .twitter -->
            <li class="social-sharing-item download">
              <a download href="<?php echo esc_url( get_sub_field( 'social_image_file' ) ); ?>" class="social-pop">
                <span class="screen-reader-text"><?php esc_html_e( 'Download this image', 'heliotrope' ); ?></span>
                <?php we_inline_svg( 'assets/icons/download.svg' ); ?>
              </a>
            </li><!-- .download -->
          </ul><!-- share links -->
        </div><!-- .grid-item -->
      <?php endwhile; ?>
      </div><!-- .grid -->
    </div><!-- .row -->
    <?php endif; ?>
  </div><!-- .container -->
</div><!-- .home-actions -->