<div class="home-text-cta">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 col-md-8">
			<?php if ( get_field( 'text_cta_headline', we_get_page_id( '4' ) ) ) : ?>
				<h1>
					<?php echo wp_kses_post( get_field( 'text_cta_headline', we_get_page_id( '4' ) ) ); ?>
				</h1>
			<?php endif; ?>
			</div><!-- text cta headline -->

			<div class="col-xs-12 col-md-4">
			<?php if ( get_field( 'text_cta_description', we_get_page_id( '4' ) ) ) : ?>
				<h2 class="h4">
					<?php echo wp_kses_post( get_field( 'text_cta_description', we_get_page_id( '4' ), false ) ); ?>
				</h2>
			<?php endif; ?>
			</div><!-- text cta description -->
		</div><!-- .row -->
	</div><!-- .container -->
</div><!-- .home-text-cta -->