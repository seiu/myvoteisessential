<div class="form-wrapper">
	<div class="topper-form">
	<?php if ( get_field( 'crm_headline', we_get_page_id( '4' ) ) ) : ?>
  	<h1>
    	<?php echo wp_kses_post( get_field( 'crm_headline', we_get_page_id( '4' ) ) ); ?>
  	</h1>
	<?php endif; ?>
		<?php echo we_render_crm( we_get_page_id( '4' ), 'global', get_field( 'submit_button_text', get_the_ID() )); ?>
	</div><!-- .topper-form -->
</div>

<?php
if (
'ngp-van' == get_field( 'default_integration', we_get_page_id( '4' ) )
) :
?>
<script type="text/javascript" src="https://d1aqhv4sn5kxtx.cloudfront.net/actiontag/at.js"></script>
<?php endif; ?>

<script type="text/javascript">
  window.ngpFormData = {
    FirstName: {
      label: '<?php echo wp_kses( get_theme_mod( 'first_name_label', __('First Name', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'first_name_placeholder', __('e.g. Jane', 'heliotrope') ), array() ); ?>',
    },
    LastName: {
      label: '<?php echo wp_kses( get_theme_mod( 'last_name_label', __('Last Name', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'last_name_placeholder', __('e.g. Doe', 'heliotrope') ), array() ); ?>',
    },
    PostalCode: {
      label: '<?php echo wp_kses( get_theme_mod( 'zip_code_label', __('Zip Code', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'zip_code_placeholder', __('#####-####', 'heliotrope') ), array() ); ?>',
    },
    EmailAddress: {
      label: '<?php echo wp_kses( get_theme_mod( 'email_label', __('Email Address', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'email_placeholder', __('username@service.domain', 'heliotrope') ), array() ); ?>',
    },
    MobilePhone: {
      label: '<?php echo wp_kses( get_theme_mod( 'phone_label', __('Phone Number', 'heliotrope') ), array() ); ?>',
      placeholder: '<?php echo wp_kses( get_theme_mod( 'phone_placeholder', __('###-###-####', 'heliotrope') ), array() ); ?>',
    },
};
<?php include(get_template_directory() . '/assets/js/bundle-ngp-labels.min.js'); ?>
</script>
