<div class="home-demands">
  <div class="container">
  <?php
    $demandsBackgroundImage = get_field( 'demands_background_image' );
    $backgroundThumb = $demandsBackgroundImage['sizes'][ 'topper-image' ];
    ?>
  <?php if ( $demandsBackgroundImage ) : ?> 
    <div class="compound-image" style="background-image: url('<?php echo $backgroundThumb; ?>');"></div><!-- .grid-image -->
    <div class="overlay"></div> <!-- .overlay -->
  <?php endif; ?> 

    <div data-module class="row">
      <div class="col-xs-12 col-md-5">
      <?php if ( get_field( 'demands_headline' ) ) : ?>
        <h1>
          <?php echo wp_kses_post( get_field( 'demands_headline' ) ); ?>
        </h1>
      <?php endif; ?>
      <?php if ( get_field( 'demands_description' ) ) : ?>
          <?php echo wp_kses_post( get_field( 'demands_description' ) ); ?>
      <?php endif; ?>
      </div><!-- demands headline and description -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .home-demands -->