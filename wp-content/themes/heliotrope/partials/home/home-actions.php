<div class="home-actions">
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-md-7">
      <?php if ( get_field( 'actions_headline', we_get_page_id( '4' ) ) ) : ?>
        <h1>
          <?php echo wp_kses_post( get_field( 'actions_headline', we_get_page_id( '4' ) ) ); ?>
        </h1>
      <?php endif; ?>
      </div><!-- actions headline -->

      <div class="col-xs-12 col-md-5">
      <?php if ( get_field( 'actions_description', we_get_page_id( '4' ) ) ) : ?>
        <?php echo wp_kses_post( get_field( 'actions_description', we_get_page_id( '4' ) ) ); ?>
      <?php endif; ?>
      </div><!-- actions description -->
    </div><!-- .row -->
  
    <div class="row grid-row">
      <div class="grid">
      <?php if ( have_rows( 'actions_links', we_get_page_id( '4' ) ) ) : ?>
        <?php while ( have_rows( 'actions_links', we_get_page_id( '4' ) ) ) : the_row(); ?>
          <?php 
          // global $post;
          // setup_postdata( $issue_post->ID );
            $icon = get_sub_field( 'link_icon' );
            $size = 'large-square';
            ?>
          <div class="grid-item<?php if ( $icon ) { echo ' has-icon'; } ?>">
            <?php if ( get_sub_field( 'link', we_get_page_id( '4' ) ) ) : ?>
              <a
                href="<?php the_sub_field( 'link', we_get_page_id( '4' ) ) ?>"
                <?php if ( get_sub_field( 'link_open_in_a_new_window', we_get_page_id( '4' ) ) ) : ?>target="_blank"<?php endif; ?>
                class="overlay-link"
              ><span class="screen-reader-text"><?php the_sub_field( 'link_text', we_get_page_id( '4' ) ); ?></span></a>
            <?php endif; ?>

            <div class="wrapper">
            <?php if ( 'no-icon' != $icon ) : ?>
              <div class="grid-item__icon icon-<?php echo $icon; ?>">
                <?php we_inline_svg( 'assets/icons/icon-'. $icon .'.svg' ); ?>
              </div>
            <?php endif; ?>

              <h2 class="alt"><?php echo esc_html( get_sub_field( 'link_text', we_get_page_id( '4' ) ) ); ?></h2>
            </div><!-- .wrapper -->
          </div><!-- .single-issue -->
        <?php endwhile; ?>
      <?php endif; ?>
      </div><!-- .grid -->
    </div><!-- .row -->
  </div><!-- .container -->
</div><!-- .home-actions -->