<?php
  $prev_post = get_previous_post();
  $next_post = get_next_post();
  $only_post = ( $prev_post && $next_post ) ? '' : ' next-prev__only';
  $post_type_name = get_post_type_object(get_post_type())->labels->singular_name;
  ?>
<nav class="next-prev container">
  <div class="row">
    <?php
      if ( $prev_post ) : ?>
      <div class="next-prev__prev col<?php echo esc_attr( $only_post ); ?>">
        <?php
          $prev_link  = get_permalink( $prev_post->ID );
          $prev_title = $prev_post->post_title;
          ?>
        <a href="<?php echo esc_url( $prev_link ); ?>" class="overlay-link"></a>
        <h6>
          <?php esc_attr_e( 'Previous', 'heliotrope' ); ?>
          <span class="screen-reader-text"> <?php echo $post_type_name; ?></span>
        </h6>
        <h3 class="next-prev__title"><?php echo wp_kses_post( we_dewidow( $prev_title ) ); ?></h3>
        <?php heliotrope_post_date( $prev_post->ID, 'F j, Y' ); ?>
      </div><!-- .next-post -->
    <?php endif; ?>

    <?php if ( $next_post ) : ?>
      <div class="next-prev__next col<?php echo esc_attr( $only_post ); ?>">
        <?php
          $next_link  = get_permalink( $next_post->ID );
          $next_title = $next_post->post_title;
          ?>
        <a href="<?php echo esc_url( $next_link ); ?>" class="overlay-link"></a>
        <h6>
          <?php esc_attr_e( 'Next', 'heliotrope' ); ?>
          <span class="screen-reader-text"> <?php echo $post_type_name; ?></span>
        </h6>
        <h3 class="next-prev__title"><?php echo wp_kses_post( we_dewidow( $next_title ) ); ?></h3>
        <?php heliotrope_post_date( $next_post->ID, 'F j, Y' ); ?>
      </div><!-- .next-post -->
    <?php endif; ?>
  </div>
</nav>