<?php
// per page custom tweet share text
$custom_share_text = get_post_meta( get_the_ID(), 'custom_share_text', true );

$share_image = get_site_icon_url();


$twitter_desc = get_post_meta( get_the_ID(), '_yoast_wpseo_twitter-description', true );

if ( is_plugin_active( 'wordpress-seo/wp-seo.php' ) || is_plugin_active( 'wordpress-seo-premium/wp-seo-premium.php' ) ) {
   $share_img_id = WPSEO_Options::get('og_default_image_id', '');
   if( $share_img_id ){
    $share_image = wp_get_attachment_url( $share_img_id );
   }
}

if( get_the_post_thumbnail_url() ){
  $share_image = get_the_post_thumbnail_url();
}

if ( $custom_share_text ) {
  $share_text = urlencode( $custom_share_text );

} else {
  //fall back to page title
  $share_text = urlencode( html_entity_decode( get_the_title() ) );
}
$twitter_share_text = $twitter_desc ? urlencode( $twitter_desc ) : $share_text;
// global twitter handle setting
$twitter_handle = get_theme_mod( 'twitter_handle', false );

$permalink_param = urlencode( get_permalink() );
?>

<li class="social-sharing-item copy-link">
  <a
    data-copy-href="<?php echo esc_url( $permalink_param ); ?>"
    class="js-click-to-copy"
    role="button"
    title="<?php esc_html_e( 'Click to copy the page url.', 'heliotrope' ); ?>"
  ><span class="click-to-copy-inner-range screen-reader-text"><?php echo esc_url( get_permalink() ); ?></span>
    <span class="link-icon" aria-hidden="true"><?php we_inline_svg( 'assets/icons/link.svg' ); ?></span>
    <span class="check-icon" title="<?php esc_html_e( 'Copied!', 'heliotrope' ); ?>" aria-hidden="true"><?php we_inline_svg( 'assets/icons/check.svg' ); ?></span>
  </a>
</li><!-- .click-to-copy -->
<li class="social-sharing-item email">
  <a
    href="mailto:?subject=<?php echo esc_attr( urlencode( get_the_title() . ' - ' . get_bloginfo( 'name' ) ) ); ?>&body=<?php echo esc_attr( $permalink_param ); ?>"
    class="addthis_button_email"
    target="_blank"
    title="<?php esc_html_e( 'Share this page via Email', 'heliotrope' ); ?>"
    addthis:title="<?php echo the_title_attribute(); ?> - <?php echo esc_html ( get_bloginfo( 'name' ) ); ?>"
    addthis:url="<?php echo esc_url( $permalink_param ); ?>"
  >
    <span class="screen-reader-text"><?php esc_html_e( 'Share this page via Email', 'heliotrope' ); ?></span>
    <span aria-hidden="true"><?php we_inline_svg( 'assets/icons/mail.svg' ); ?></span>
  </a>
</li><!-- .email -->
<li class="social-sharing-item twitter" data-txt="<?php echo urlencode( $twitter_share_text ); ?>">
  <?php
    $twitter_share_string = wp_sprintf( 'https://twitter.com/intent/tweet?url=%s&text=%s%s', $permalink_param, $twitter_share_text, ($twitter_handle ? '&via=' . urlencode( $twitter_handle ) : '') );
    ?>
  <a
    href="<?php echo esc_url( $twitter_share_string ); ?>"
    class="social-pop"
  >
    <span class="screen-reader-text"><?php esc_html_e( 'Share this page on Twitter', 'heliotrope' ); ?></span>
    <span aria-hidden="true"><?php we_inline_svg( 'assets/icons/twitter.svg' ); ?></span>
  </a>
</li><!-- .twitter -->
<li class="social-sharing-item facebook">
  <a
    href="https://www.facebook.com/sharer.php?u=<?php echo esc_attr( $permalink_param ); ?>&t=<?php the_title_attribute(); ?>"
    class="social-pop"
  >
    <span class="screen-reader-text"><?php esc_html_e( 'Share this page on Facebook', 'heliotrope' ); ?></span>
    <span aria-hidden="true"><?php we_inline_svg( 'assets/icons/facebook.svg' ); ?></span>
  </a>
</li><!-- .facebook -->
