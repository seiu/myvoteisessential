<?php
/**
 * Slide-out Share Buttons
 *
 * @package Purple
 */

?>

<aside class="social-nav behind" aria-hidden="true">
  <div class="shares-slider">
    <button id="socialTrigger" class="shares-slider__trigger">
      <div class="shares-slider__icon">
        <?php we_inline_svg( 'assets/icons/connected.svg' ); ?>
      </div>
      <div class="shares-slider__slide">
        <ul class="shares-slider__list">
          <?php get_template_part( 'partials/social/page', 'shares' ); ?>
        </ul>
      </div><!-- .share-slide -->
    </button><!-- .social-side -->
  </div>
</aside><!-- .social-nav -->
