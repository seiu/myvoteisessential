<?php if ( have_posts() ) : ?>
<footer class="container pagination">
  <nav class="row" aria-label="pagination">
    <?php echo wpex_pagination( get_post_type() ); ?>  
  </nav>
</footer>
<?php endif; ?>