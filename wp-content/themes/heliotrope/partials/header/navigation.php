<?php
/**
 * Default Navigation
 *
 * @package heliotrope
 */
?>
<header id="masthead" data-js-social-hide class="site-header header-copy sticky-header">
    <div class="site-header__inner">
      <div class="container-fluid">
        <div class="row site-header__row justify-content-between align-items-stretch">
          <div class="site-header__item site-header__main-logo col-auto">
            <a class="site-header__item_inner" href="<?php echo esc_url( home_url( '' ) ); ?>/" rel="home">
              <?php we_inline_svg( 'assets/img/strike-for-black-lives-logo.svg' ); ?>
            </a>    
          </div>
          <?php
          if (has_nav_menu('primary_navigation')) :
            list( $sizing_classes, $nav_menu_count, $nav_length ) = we_get_menu_size_data( 'primary-navigation' );
          ?>
            <nav
            class="site-header__item site-header__menu-section col primary-menu <?php echo esc_attr( $sizing_classes ); ?>"
            data-itemcount="<?php echo esc_attr( $nav_menu_count ); ?>"
            data-textlength="<?php echo esc_attr( $nav_length ); ?>" role="navigation" aria-label="Primary Navigation"
            >
            <?php
            wp_nav_menu(
              array(
                'theme_location' => 'primary_navigation',
                'container'      => 'primary-navigation',
                'menu_id'        => 'primary-navigation',
                'before'         => '',
                'menu_class'     => 'row menu justify-content-end site-header__item_inner',
                'add_li_class'   => 'col-auto',
                'walker'         => new We_Accessible_Walker(),
              )
            );
            ?>
            </nav>
          <?php endif; 
          if ( 'off' !== get_theme_mod( 'search_design', 'off' ) ) : ?> 
            <button class="site-header__item site-search-button col-auto js-open-site-search" role="button">
              <span class="screen-reader-text"><?php esc_attr_e( 'Search', 'heliotrope' ); ?></span>
              <span class="nav__search-icon" aria-hidden="true"><?php we_lazy_inline_svg( 'assets/icons/search.svg' ); ?></span>
              <span class="nav__close-icon" aria-hidden="true"><?php we_lazy_inline_svg( 'assets/icons/close-btn.svg' ); ?></span>
            </button>
          <?php endif;
            if (has_nav_menu('secondary_navigation')) :
              list( $sizing_classes, $nav_menu_count, $nav_length ) = we_get_menu_size_data( 'secondary-navigation' );
            ?>
            <nav class="site-header__item site-header__menu-section col-auto secondary-menu <?php echo esc_attr( $sizing_classes ); ?>"
            data-itemcount="<?php echo esc_attr( $nav_menu_count ); ?>"
            data-textlength="<?php echo esc_attr( $nav_length ); ?>" role="navigation" aria-label="Secondary Navigation">
              <?php
              wp_nav_menu(
                array(
                  'theme_location' => 'secondary_navigation',
                  'container'      => 'secondary-navigation',
                  'menu_id'        => 'secondary-navigation',
                  'link_class'     => 'btn',// this is what makes these buttons
                  'before'         => '',
                  'menu_class'     => 'row menu justify-content-end site-header__item_inner',
                  'add_li_class'   => 'col-auto',
                  'walker'         => new We_Accessible_Walker(),
                )
              );
              ?>
            </nav>
          <?php endif; ?>
          <div class="site-header__item mobile-menu-toggle col">
            <a href="#primary-footer-menu-section" class="mobile-menu-toggle__control js-mobile-menu-control js-open-main-menu" role="button" aria-labeledby="mm-label"
            >
              <span id="mm-label" class="hamburger-control__label">
                <span class="hamburger-control__open-label">
                  <span class="screen-reader-text"><?php esc_attr_e( 'Site Menu', 'heliotrope' ); ?></span>
                </span>
                <span class="hamburger-control__close-label" aria-hidden="true">
                  <span class="screen-reader-text"><?php esc_attr_e( 'Close Menu', 'heliotrope' ); ?></span>
                </span>
              </span>
              <span class="hamburger-control" aria-hidden="true">
                <span class="hamburger-control__inner"></span>
              </span>
            </a>
          </div>
        </div>
      </div>
    </div>
  </header>