var $j = jQuery.noConflict(),
        $window = $j(window);

$j(document).on('ready', function () {
    "use strict";
    // Mega menu

    var $container = $j('.ht-header .ht-container');

    $j('.ht-menu .menu-item-megamenu.megamenu-full-width').hover(function () {
        var $menuWidth = $container.outerWidth(),
                $menuPosition = $container.offset(),
                $menuItemPosition = $j(this).offset(),
                $PositionLeft = $menuItemPosition.left - $menuPosition.left + 1;

        $j(this).find('.megamenu').css({
            'left': '-' + $PositionLeft + 'px',
            'width': $menuWidth
        });
    });

    // Megamenu auto width
    $j('.ht-menu .menu-item-megamenu.megamenu-auto-width .megamenu').each(function () {
        var $li = $j(this).parent(),
                $window_width = $j(window).width(),
                $liOffset = $li.offset().left,
                $liWidth = $li.outerWidth(),
                $dropdownWidth = $j(this).outerWidth();
        if (square_plus_megamenu.rtl == 'true') {
            if (($window_width - $liOffset - $liWidth) + $dropdownWidth > $window_width + 10) {
                $j(this).css({
                    'right': 'auto',
                    'left': 0
                });
            }

            var $dropdownRight = $dropdownWidth - ($window_width - ($liOffset + $liWidth));

            if ($dropdownRight > 0) {
                $j(this).css({
                    'left': 'auto',
                    'right': -($dropdownWidth - ($liOffset + $liWidth - 10))
                });
            }
        } else {
            if ($liOffset + $dropdownWidth > $window_width + 10) {
                $j(this).css({
                    'left': 'auto',
                    'right': 0
                });
            }

            var $dropdownLeft = $dropdownWidth - ($liOffset + $liWidth);

            if ($dropdownLeft > 0) {
                $j(this).css({
                    'left': 'auto',
                    'right': -($window_width - $liOffset - $liWidth - 10)
                });
            }
        }

        /*
         * 
         * if (square_plus_megamenu.rtl == 'true') {
         $j(this).css({
         'right': -$left,
         //'marginRight': $dropdowntMarginLeft
         });
         } else {
         $j(this).css({
         'left': -$left,
         //'marginLeft': $dropdowntMarginLeft
         });
         }
         if ($dropdowntLeft < 0) {
         var $left = $liOffset - 10;
         $dropdowntMarginLeft = 0;
         } else {
         var $left = $dropdownWidth / 2;
         
         }
         var $dropdownRight = ($window.width()) - ($liOffset - $left + $dropdownWidth + $dropdowntMarginLeft);
         
         if ($dropdownRight < 0) {
         $j(this).css({
         'left': 'auto',
         'right': -($window.width() - $liOffset - $liWidth - 10)
         });
         }*/
    });

    $j('li.heading-yes > a').on('click', function () {
        return false;
    });

    $j('.cat-megamenu-tab > div:first').addClass('active-tab');

    $j('.cat-megamenu-tab > div').hoverIntent(function () {
        var $this = $j(this);
        if ($this.hasClass('active-tab')) {
            return;
        }

        $this.siblings().removeClass('active-tab');
        $this.addClass('active-tab');
        var activeCat = $this.data('catid');
        $this.closest('.megamenu').find('.cat-megamenu-content > ul').hide();
        $this.closest('.megamenu').find('#' + activeCat).fadeIn('fast');
    });
});