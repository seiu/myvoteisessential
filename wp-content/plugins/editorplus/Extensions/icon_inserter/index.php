<?php


function emitStyle() {
    $font_path = plugins_url( 'fonts' , __FILE__ );

    echo "<style id='eplus-custom-fonts'>

    @font-face {
      font-family: 'eplus-icon';
      src: url('$font_path/eplus-icon.eot?xs1351');
      src: url('$font_path/eplus-icon.eot?xs1351#iefix')
          format('embedded-opentype'),
        url('$font_path/eplus-icon.ttf?xs1351') format('truetype'),
        url('$font_path/eplus-icon.woff?xs1351') format('woff'),
        url('$font_path/eplus-icon.svg?xs1351#eplus-icon') format('svg');
      font-weight: normal;
      font-style: normal;
      font-display: block;
    }
  
    </style>";
}


$opt = get_option('editor_plus_extensions_icon_inserter__enable', true);

$is_extension_enabled = $opt === '1' || $opt === true ? true : false;

if ($is_extension_enabled) {
  add_action( 'admin_enqueue_scripts', 'emitStyle' );
  add_action('wp_head', 'emitStyle');
}


