<?php


$opt = get_option('editor_plus_extensions_custom_block_code__enable', true);
$is_extension_enabled = $opt === '1' || $opt === true ? true : false;

if ($is_extension_enabled) {

    add_action('init', function () {

        register_meta(
            'post',
            'editor_plus_custom_css',
            array(
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
                'default'       => '{}'
            )
        );

    });

    add_action('wp_footer', function () {

        global $post;

        if (!empty($post)) {
            $id = $post->ID;
            $custom_css_meta = get_post_meta($id, 'editor_plus_custom_css', TRUE);

            $css = [];


            $decoded_css = json_decode($custom_css_meta);

            if (is_object($decoded_css)) {

                foreach ($decoded_css as $key => $styling) {

                    $css[] = "$styling";
                }

                echo '<style id="ep-custom-css">' . join("\n", array_unique($css)) . '</style>';
            }
        }
    });
}


//for global css/js
add_action('init', function () {

    $css_file_path = plugin_dir_path(__FILE__) . 'dist/global.css';
    $js_file_path = plugin_dir_path(__FILE__) . 'dist/global.js';

    $global_css = wp_strip_all_tags(get_option('ep_custom_global_css'));

    $css_file = fopen($css_file_path, 'w');

    fwrite($css_file, $global_css);

    add_action('enqueue_block_assets', function () {

        $path = plugin_dir_url(__FILE__) . 'dist/';

        wp_enqueue_style(
            'e-plus-global-script',
            $path . 'global.css'
        );

    });
});
