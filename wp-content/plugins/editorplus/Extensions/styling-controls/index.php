<?php


$opt = get_option('editor_plus_extensions_styling__enable', true);

$is_extension_enabled = $opt === '1' || $opt === true ? true : false;

// add_action( 'before_delete_post', function($id) {

//     $path = "generated_css/editor_plus_css__$id.css";

//     $css_file_to_delete = plugin_dir_path( __FILE__ ) . $path;

//     wp_delete_file($css_file_to_delete);

// } );

if ($is_extension_enabled) {

    add_action('init', function () {
        register_meta(
            'post',
            'editor_plus_custom_styling_options_css',
            array(
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
                'default'       => '{}'
            )
        );

        # all the copied stylings will be stored here...        

        register_meta(
            'post',
            'editor_plus_copied_stylings',
            array(
                'show_in_rest' => true,
                'single'       => true,
                'type'         => 'string',
                'default'       => '{}'
            )
        );
    });

    add_action('wp_head', function () {

        global $post;

        if (!empty($post)) {
            $ID = $post->ID;

            $custom_styling_meta = get_post_meta($ID, 'editor_plus_custom_styling_options_css', TRUE);

            $decoded_styling = json_decode($custom_styling_meta);

            $css = [];

            if (is_object($decoded_styling)) {


                foreach ($decoded_styling as $clientID => $styling) {

                    $css[] = $styling;
                }

                $css_code = trim(join("\n", array_unique($css)));

                echo sprintf(
                    '<style id="ep-custom-options-css">%1$s</style>',
                    $css_code
                );

                // $path = "generated_css/editor_plus_css__$ID.css";

                // $css_file_path = plugin_dir_path( __FILE__ ) . $path;

                // $css_file = fopen($css_file_path, 'w');

                // fwrite($css_file, $css_code);

                // $enqueue_path = plugin_dir_url( __FILE__ ) . $path;

                // wp_enqueue_style(
                //     'eplus-options',
                //     $enqueue_path
                // );
            }
        }
    });
}
