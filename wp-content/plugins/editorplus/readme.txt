=== Gutenberg Block Library & Toolkit - Editor Plus ===
Contributors: munirkamal
Tags: block, blocks, editor, gutenberg, gutenberg blocks, wordpress blocks
Requires at least: 5.0
Tested up to: 5.5
Requires PHP: 5.6
Stable tag: 1.6.0
License: GPL-3.0
License URI: http://www.gnu.org/licenses/gpl-3.0.html

Editor Plus extends Gutenberg editor with advanced design controls, icons, Shape Divider and many more features.


== Description ==

## Gutenberg Block Library & Toolkit - Editor Plus

https://www.youtube.com/watch?v=sU0LkhS8OMk

[EditorPlus](https://wpeditorplus.com?utm_medium=wp.org&utm_source=wordpressorg&utm_campaign=readme&utm_content=editorplus) extends Gutenberg editor with advanced design controls, icons, Shape Divider and many more features.

**Design better pages in Gutenberg editor, faster.**

## No-Code Style Editor for WordPress

Customize Gutenberg blocks visually without code. Add background, adjust size, apply spacing and more. Supercharge Gutenberg Editor!


📐 Spacing

🖼 Border

🏞 Background

❑  Box-Shadow

👀 Visibility

🌈 Shape Divider

💎 Icons

🔜 More features to be added.


⭐️ All these stylings can be adjusted for Responsive (Desktop, Tablet & mobile) and Hover!

## Custom CSS Editor for Power Users

You can write your custom CSS code as well. The custom CSS code box will be available for all Gutenberg core blocks as well as via the plugin admin area for writing global CSS that applies throughout your website (including Gutenberg editor screen).

## 🔥 Add Blocks from Block Library at GutenbergHub 🔥

Need a block? Search from the [Gutenberg Blocks Directory](https://gutenberghub.com/blocks?utm_medium=wp.org&utm_source=wordpressorg&utm_campaign=readme&utm_content=editorplus) at GutenbergHub and copy one to your website using the blocks page from the EditorPlus admin area.

## 💎 Add Icons anywhere (RichText) in Gutenberg Editor 💎

https://www.youtube.com/watch?v=yN-APh21obE

Need to insert an icon? EditorPlus brings icons integration that enables you to insert icons anywhere where you can insert text. You can add icons to paragraphs, headlines, buttons, or any other place where you can write text in the editor. Additionally, you can also apply a background color, add a border, and set border-radius to the icons. Currently, we've included Font Awesome and Icon Moon icon libraries, but, we may add more in the future.  

## 🌈 Add Nice SVG Shape Dividers in Gutenberg Editor 🌈

https://www.youtube.com/watch?v=BimiwZOens8

SVG shape dividers or separators add a nice visual effect to your section or content. Now you can easily add and customize it in Gutenberg WordPress Editor. Currently supported on the following blocks:

* Cover Block
* Columns Block & inner Column Block
* Group Block

As it is available for the Group block means you can simply group any block(s) content and add a shape divider to the group.

## 🦸‍♂️ Be a Pro - Copy & Paste Block Styles & Responsive Visual Editing 🦸‍♂️

https://www.youtube.com/watch?v=HpJdJCuC5kQ

Starting with Editor Plus v1.6.0, now you can copy a block style and paste it to another block with a click quickly. You do not need to re-style the same thing again and again. 

This release also improved responsive editing features by synchronizing the responsive previewer window.

## There is much more to come, stay tuned.

More exciting features and enhancements are planned for the WordPress Gutenberg Editor. Stay tuned for updates. 😇



== Installation ==
1. Upload the entire plugin folder to the `/wp-content/plugins/` directory.
1. Activate the plugin through the \'Plugins\' menu in WordPress.

Once Activated, you will see new styling options for all core Blocks in the Gutenberg editor.

Make sure to check out the Admin are of the plugin for configuration settings.

== Screenshots ==
1. Background (Solid, Gradient, Image)
2. Box-Shadow
3. Border & Border Radius
4. Spacing (Margin & Padding)

== Changelog ==

=1.6.0 (17 Aug 2020)
* New: Copy & Paste Blocks Styles with a click
* New: Live Visual Responsive Editing

=1.5.0 (15 Aug 2020)
* Fixed: Bugs

=1.4.0 (05 Aug 2020)
* Added: Shape Divider Feaure
* Bug Fixes & improvements

=1.3.0 (16 July 2020)
* Added: Icons Extension
* Bug Fixes & improvements

=1.2.0 (11 July 2020)
* Added: Blocks Extension
* Bug Fixes & improvements

=1.1.0
* Added: Custom CSS Editor
* Added: Overflow property under extra panel
* Added: z-index property under extra panel
* Bug Fixes

= 1.0.0 =
* Initial release